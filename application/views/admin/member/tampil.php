<div class="x_title">
  <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?> </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <table id="datatable" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th width="5%">No</th>
        <th>Email</th>
        <th>Nama Lengkap</th>
        <th width="12%">No. Telepon</th>
        <th width="35%">Alamat</th>
        <th style="width: 15%;text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($member as $key => $value): ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td><?php echo $value["email_member"] ?></td>
          <td><?php 
            if ($value["nama_member"]==null) {
              echo "-";
            }else{
              echo $value["nama_member"];
            }
           ?></td>
          <td><?php 
            if ($value['no_hp']==null) {
              echo "-";
            }else{
              echo $value['no_hp'];
            }
            ?></td>
          <td><?php 
            if ($value['alamat_member']==null) {
              echo "-";
            }else{
              echo $value['alamat_member'];
            }
          ?></td>
          <td style="text-align: right;">
            <a href="<?php echo base_url("admin/member/detail/$value[id_member]")?>" class="btn btn-info btn-sm ">Detail</a>
            <a href="<?php echo base_url("admin/member/hapus/$value[id_member]")?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Ingin Menghapus?')">Hapus</a>
          </td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>