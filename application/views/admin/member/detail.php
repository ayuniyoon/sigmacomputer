<div class="x_title">
  <h2><?php echo $judul; ?></h2>
  <div class="pull-right">
    <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-history"></i> Kembali</a>
  </div>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="clearfix"></div>
    <br />
    <!-- isi tab mulai bawah ini -->
    <div class="x_content">
      <div role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Profil</a>
          </li>
          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Riwayat Pembelian</a>
          </li>
        </ul>
        <div id="myTabContent" class="tab-content">
          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            <table class="table table-hover">
              <thead>
                <tr>
                  &nbsp;
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td width="20%"><i class="fa fa-user"></i>   Nama</td>
                  <td width="2px">:</td>
                  <td></i> <?php 
                    if ($detail['nama_member']==null) {
                      echo "-";
                    }else{
                      echo $detail['nama_member'];
                    }
                   ?></td>
                </tr>
                <tr>
                  <td><i class="fa fa-phone"></i>   No.Telepon</td>
                  <td>:</td>
                  <td><?php 
                    if ($detail['no_hp']==null) {
                      echo "-";
                    }else{
                      echo $detail['no_hp'];
                    }
                   ?></td>
                </tr>
                <tr>
                  <td><i class="fa fa-envelope"></i>   Email</td>
                  <td>:</td>
                  <td><?php echo $detail['email_member']; ?></td>
                </tr>
                <tr>
                  <td><i class="fa fa-home"></i>   Alamat</td>
                  <td>:</td>
                  <td><?php 
                    if ($detail['alamat_member']==null) {
                      echo "-";
                    }else{
                      echo $detail['alamat_member'];
                    }
                   ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
            <br>
            <table id="datatable" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <td width="5px">Transaksi</td>
                  <td>Nama Penerima</td>
                  <td>Total Bayar</td>
                  <td>Status Bayar</td>
                  <td>Status Kirim</td>
                  <td width="15px">Tgl.Pembelian</td>
                </tr>
              </thead>
              <tbody>
                <?php if ($riwayat==null){ ?> 
                <tr>
                  <td colspan="6"><center>Member belum pernah melakukan transaksi</center></td>
                </tr>
                <?php }else{ ?>
                <?php foreach ($riwayat as $key => $value): ?>
                  <tr>
                    <td><a href="<?php echo base_url("invoice/$value[id_transaksi]"); ?>" target="_blank"><?php echo "P".$value['id_transaksi']; ?></a></td>
                    <td><?php echo $value['nama_penerima']; ?></td>
                    <td><?php echo rupiah($value['total_pembelian']); ?></td>
                    <td><?php echo $value['status_pembelian']; ?></td>
                    <td><?php echo $value['status_pengiriman']; ?></td>
                    <td><?php echo date("d-M-Y", strtotime($value['tgl_transaksi'])); ?></td>
                  </tr>
                <?php endforeach ?>
                <?php } ?>
              </tbody>
            </table>

          </div>
          
        </div>
      </div>

    </div>
  </div>
</div>