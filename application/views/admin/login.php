<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $title ?> | Administrator</title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo base_url('assets/vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
	<!-- jQuery custom content scroller -->
	<link href="<?php echo base_url('assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css") ?> " rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?php echo base_url('assets/dist/css/custom.min.css') ?>" rel="stylesheet">
</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<img src="<?php echo base_url("assets/images/logo/sigma-kecil.png"); ?>" width="250">
					<form method="post">

						<h1>Administrator</h1>
						<div>
							<input type="text" class="form-control" placeholder="Username" required="" name="username" autofocus="autofocus" />
						</div>
						<div>
							<input type="password" name="password" class="form-control" placeholder="Password" required="required" autofocus="autofocus" />
						</div>
						<div>
							<button class="btn btn-success btn-block submit">Masuk</button>
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<div class="clearfix"></div>
							<br />

							<div>
								<p>© 2018 All Rights Reserved</p>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>
	<?php if ($hasil=='berhasil'): ?>
		<script>
			alert('Anda Sudah Berhasil Login');
			location='<?php echo base_url("admin/home"); ?>';
		</script>
	<?php endif ?>
	<?php if ($hasil=='gagal'): ?>
		<script>
			alert('Ups, Anda Gagal! Mungkin ada kesalahan di username atau password');
			location='<?php echo base_url("admin"); ?>';
		</script>
	<?php endif ?>
</body>
</html>
