<div class="x_title">
  <h2>Edit Rekening Bank</h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Edit Rekening</label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="nama_bank" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo $ubah["nama_bank"] ?>">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Rekening
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="no_rekening" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo $ubah["no_rekening"] ?>">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Atas Nama
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="nama_pemilik" class="form-control col-md-7 col-xs-12" value="<?php echo $ubah["nama_pemilik"] ?>" required>
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Logo Bank
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <img src="<?php echo base_url("assets/images/bank/$ubah[logo_bank]"); ?>" width="150">
        <br><br/>
        <input type="file" name="logo_bank" class="form-control col-md-7 col-xs-12">
        <small>*klik jika ingin mengganti logo</small>
      </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group pull-right">
      <a href="javascript:history.back()" class="btn btn-default">Batal</a>
      <button type="submit" class="btn btn-success">Simpan</button>
    </div>
  </form>
  <br />
</div>