  <div class="x_title">
    <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?> </h2>
    <div class="pull-right">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-bank"><i class="fa fa-plus"></i> Tambah Rekening</button>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table id="datatable" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th width="5%">No</th>
          <th>Nomor Rekening</th>
          <th width="15%">Bank Rekening</th>
          <th width="30%">Atas Nama</th>
          <th style="width: 15%; text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($bank as $key => $value): ?>
          <tr>
            <td><?php echo $key+1; ?></td>
            <td><?php echo $value["no_rekening"]; ?></td>
            <td><?php echo $value["nama_bank"]; ?></td>
            <td><?php echo $value["nama_pemilik"]; ?></td>
            <td style="text-align: right;">
              <a href="<?php echo base_url("admin/bank/ubah/$value[id_bank]"); ?>" class="btn btn-warning btn-sm ">Ubah</a>
              <a href="<?php echo base_url("admin/bank/hapus/$value[id_bank]") ?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Ingin Menghapus?')">Hapus</a>
            </td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
</div>
<br/> 

  <!-- modal untuk tambah data -->
  <div class="modal fade tambah-bank" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Tambah Bank</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
            <label>Bank </label>
            <input type="text" class="form-control" name="nama_bank" data-parsley-trigger="change" required="required" autofocus="autofocus" placeholder="BCA/BRI/BNI/Mandiri" maxlength="20" />

             <label>Nomor Rekening</label>
            <input type="text" class="form-control" name="no_rekening" data-parsley-trigger="change" required="required"/>

             <label>Atas Nama</label>
            <input type="text" class="form-control" name="nama_pemilik" data-parsley-trigger="change" required="required"/>

            <label>Logo Bank</label>
            <input type="file" class="form-control" name="logo_bank" required="required"/>
            <small>*ukuran gambar 250x85 pixel</small>
              <br/>
            <br/>
            <div class="pull-right">
             <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
             <button type="submit" class="btn btn-success">Simpan</button>
           </div>
         </form>
       </div>
       <div class="modal-footer">
       </div>

     </div>
   </div>
 </div>

 <!-- modal untuk edit data -->
 <div class="modal fade edit-data" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Edit Rekening Lama</h4>
      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left" method="post">
          <label>Rekening Lama</label>
          <input type="text" class="form-control" name="nama_bank" data-parsley-trigger="change" required="required" autofocus="autofocus" value="" />

          <label>Slug</label>
          <input type="text" class="form-control" data-parsley-trigger="change" disabled="disabled" />
          <br />

          <div class="pull-right">
           <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
           <button type="submit" class="btn btn-success">Simpan</button>
         </div>
       </form>

     </div>
     <div class="modal-footer">
     </div>

   </div>
 </div>
</div>

