  <div class="x_title">
    <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?> </h2>
    <div class="pull-right">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-subkategori"><i class="fa fa-plus"></i> Tambah Sub Kategori</button>
    </div>
    <div class="clearfix"></div>
  </div>
  <?php if (validation_errors()): ?>
    <div class="alert alert-info alert-dismissible" role="alert" style="color: #fff;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <?php echo validation_errors(); ?>
    </div>
  <?php endif ?>
  <div class="x_content">
    <table id="datatable" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th width="5%">No</th>
          <th width="30%">Kategori</th>
          <th width="30%">Sub Kategori Produk</th>
          <th>Slug</th>
          <th style="width: 15%; text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($subkategori as $key => $sub): ?>
          <tr>
            <td><?php echo $key+1; ?></td>
            <td>
              <?php if ($sub['id_kategori']==null){echo "<p style=color:red>Kategori tidak ada atau sudah dihapus</p>";}else{echo $sub['nama_kategori'];} ?>
            </td>
            <td><?php echo $sub['nama_subkategori'] ?></td>
            <td><?php echo $sub['url_subkategori'] ?></td>
            <td style="text-align: right;">
              <a href="<?php echo base_url("admin/subkategori/ubah/$sub[id_subkategori]"); ?>" class="btn btn-warning btn-sm">Ubah</a>
              <a href="<?php echo base_url("admin/subkategori/hapus/$sub[id_subkategori]") ?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Ingin Menghapus?')">Hapus</a>
            </td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
</div>
<br/> 

<!-- modal untuk tambah data -->
<div class="modal fade tambah-subkategori" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Tambah Sub Kategori Baru</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left" method="post">
          <label>Sub Kategori Baru</label>
          <input type="text" class="form-control" name="nama_subkategori" data-parsley-trigger="change" minlength="3" required autofocus/>

          <label>Select</label>
          <select class="form-control" name="id_kategori" required>
            <option value="">- Pilih Kategori - </option>
            <?php foreach ($kategori as $key => $value): ?>
              <option value="<?php echo $value['id_kategori'] ?>"><?php echo $value['nama_kategori']; ?></option>
            <?php endforeach ?>
          </select>
          <br/>
          <div class="pull-right">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<!-- modal untuk edit data -->
<div class="modal fade edit-data" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Edit Kategori Lama</h4>
      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left" method="post">
          <label>Kategori Lama</label>
          <input type="text" class="form-control" name="nama_kategori" data-parsley-trigger="change" required="required" autofocus="autofocus" value="" />

          <label>Slug</label>
          <input type="text" class="form-control" data-parsley-trigger="change" disabled="disabled" />
          <br />

          <div class="pull-right">
           <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
           <button type="submit" class="btn btn-success">Simpan</button>
         </div>
       </form>

     </div>
     <div class="modal-footer">
     </div>

   </div>
 </div>
</div>

<!-- tambah suhkategori -->

