<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="<?php echo base_url("admin/home"); ?>" class="site_title"><span style="margin-left: 10px;"> Sigma Computer</span></a>
    </div>
    <div class="clearfix"></div>
    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Menu Administrator</h3>
        <ul class="nav side-menu">
          <li class="<?php if($halaman=='home'){echo "active";} ?>"><a href="<?php echo base_url('admin/home'); ?>"><i class="fa fa-home"></i> Home</a></li>
          <li class="<?php if($halaman=='produk'){echo "active";} ?>"><a><i class="fa fa-suitcase"></i> Produk <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="<?php if($halaman=='produk'){echo "display: block;";} ?>">
              <li class="<?php if($halaman=='produk'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/produk'); ?>">Lihat Semua</a></li>
              <li class="<?php if($halaman=='produk'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/produk/tambah'); ?>">Tambah Produk</a></li>
            </ul>
          </li>
          <li class="<?php if($halaman=='brand'){echo "active";}elseif($halaman=='kategori'){echo "active";}elseif($halaman=='subkategori'){echo "active";} ?>"><a><i class="fa fa-sitemap"></i> Produk Master <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="<?php if($halaman=='brand'){echo "display: block;";}elseif($halaman=='kategori'){echo "display: block;";}elseif($halaman=='subkategori'){echo "display: block;";} ?>">
              <li class="<?php if($halaman=='brand'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/brand'); ?>">Data Brand</a></li>
              <li class="<?php if($halaman=='kategori'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/kategori'); ?>">Data Kategori</a></li>
              <li class="<?php if($halaman=='subkategori'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/subkategori'); ?>">Data Sub Kategori </a></li>
            </ul>
          </li>
          <li class="<?php if($halaman=='artikel'){echo "active";} ?>"><a><i class="fa fa-pencil"></i> Artikel <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="<?php if($halaman=='artikel'){echo "display: block;";} ?>">
              <li class="<?php if($halaman=='artikel'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/artikel'); ?>">Lihat Semua</a></li>
              <li class="<?php if($halaman=='artikel'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/artikel/tambah'); ?>">Tambah Post</a></li>
            </ul>
          </li>
          <li class="<?php if($halaman=='transaksi'){echo "active";}elseif($halaman=='pembayaran'){echo "active";} ?>"><a><i class="fa fa-shopping-cart"></i> Penjualan <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="<?php if($halaman=='transaksi'){echo "display: block;";}elseif($halaman=='pembayaran'){echo "display: block;";} ?>">
              <li class="<?php if($halaman=='transaksi'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/transaksi'); ?>">Lihat Transaksi</a></li>
            </ul>
          </li>
          <li class="<?php if($halaman=='member'){echo "active";} ?>"><a><i class="fa fa-users"></i> Pengguna <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="<?php if($halaman=='member'){echo "display: block;";} ?>">
              <li class="<?php if($halaman=='member'){echo "current-page";} ?>"><a href="<?php echo base_url('admin/member'); ?>">Data Member</a></li>
            </ul>
          </li>
          <li class="<?php if($halaman=='slider'){echo "active";}elseif($halaman=='informasi'){echo "active";}elseif($halaman=='pengaturan'){echo "active";}elseif($halaman=='bank'){echo "active";}elseif($halaman=='sms'){echo "active";} ?>"><a><i class="fa fa-cogs"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="<?php if($halaman=='slider'){echo "display: block;";}elseif($halaman=='informasi'){echo "display: block;";}elseif($halaman=='sms'){echo "display: block;";}elseif($halaman=='pengaturan'){echo "display: block;";}elseif($halaman=='bank'){echo "display: block;";} ?>">
              <li class="<?php if($halaman=='slider'){echo "current-page";} ?>"><a href="<?php echo base_url("admin/slider") ?>"></i> Slider</a></li>
              <li class="<?php if($halaman=='informasi'){echo "current-page";} ?>"><a href="<?php echo base_url("admin/informasi") ?>">Informasi Toko</a></li>
              <li class="<?php if($halaman=='pengaturan'){echo "current-page";} ?>"><a href="<?php echo base_url("admin/toko") ?>">Pengaturan Toko</a></li>
              <li class="<?php if($halaman=='sms'){echo "current-page";} ?>"><a href="<?php echo base_url("admin/sms") ?>">Template SMS</a></li>
              <li class="<?php if($halaman=='bank'){echo "current-page";} ?>"><a href="<?php echo base_url("admin/bank") ?>">Rekening</a></li>
            </ul>
          </li>
          <li><a href="<?php echo base_url('admin/admin/logout'); ?>" onclick="return confirm('Yakin Ingin Keluar?');"><i class="fa fa-sign-out"></i> Keluar / Logout</a></li>
          <br />
          <br />
          <br />
        </ul>
      </div>


    </div>
    <!-- /sidebar menu -->
  </div>
</div>