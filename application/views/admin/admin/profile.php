<div class="container" style="min-height: 480px;">
	<div class="row">
		<div class="x_title">
			<h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?> </h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<form class="form" method="post">
				<div class="form-group col-md-6">
					<input type="hidden" name="admin" value="<?php echo $admin['admin']; ?>">
					<label>Nama Lengkap</label>
					<input type="text" name="nama_admin" class="form-control" minlength="5" required value="<?php echo $admin['nama_admin']; ?>">
				</div>
				<div class="form-group col-md-6">
					<label>Email Admin</label>
					<input type="email" name="email_admin" class="form-control" value="<?php echo $admin['email_admin']; ?>" required minlength="5">
				</div>
				<div class="form-group col-md-6">
					<label>Username</label>
					<input type="text" name="username" class="form-control" value="<?php echo $admin['username']; ?>" required minlength="5">
				</div>
				<div class="form-group col-md-6">
					<label>Password</label>
					<input type="password" name="password" class="form-control" value="<?php echo $admin['password']; ?>" required>
				</div>
				<div class="form-group pull-right">
					<br>
					<button class="btn btn-success" type="submit">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>