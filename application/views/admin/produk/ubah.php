<div class="x_title">
  <h2>Edit Produk Lama </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  
  <?php if ($cek=="berhasil"): ?>
    <div class="alert alert-success">Data Berhasil dirubah</div>
    <meta charset="utf-8" http-equiv="refresh" content="2;url=<?php echo base_url("admin/produk/"); ?>">
  <?php endif ?>

  <div class="">
    <form class="form" method="post" enctype="multipart/form-data">
      <div class="form-group col-md-4 col-sm-4">
        <label>Kategori</label>
        <select class="form-control" name="id_kategori" onchange="submit()" required autofocus>
          <option value="">- Pilih Kategori -</option>
          <?php foreach ($kategori as $key => $value): ?>
            <option value="<?php echo $value['id_kategori'] ?>" <?php if ($id_kategori==$value['id_kategori']){echo "selected";} ?>><?php echo $value['nama_kategori']; ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Sub Kategori</label>
        <select class="form-control" name="id_subkategori" required="required" autofocus>
          <option>- Pilih Sub Kategori -</option>
          <?php foreach ($subkategori as $key => $value): ?>
            <option value="<?php echo $value['id_subkategori'] ?>" <?php if ($produk['id_subkategori']==$value['id_subkategori']) {echo "selected";} ?>><?php echo $value['nama_subkategori']; ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Brand</label>
        <select class="form-control" name="id_brand" required="required" autofocus>
          <option>- Pilih Brand -</option>
          <?php foreach ($brand as $key => $value): ?>
            <option value="<?php echo $value['id_brand'] ?>" <?php if ($produk['id_brand']==$value['id_brand']) {echo "selected";} ?>><?php echo $value['nama_brand']; ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group col-md-12">
        <label>Nama Produk</label>
        <input type="text" name="nama_produk" class="form-control" minlength="5" required="required" value="<?php echo $produk['nama_produk']; ?>">
      </div>

      <div class="form-group col-md-12">
        <label>Harga Produk</label><small> tanpa tanda titik / koma</small>
        <input type="number" name="harga_produk" class="form-control" required="required" min="0" pattern="[0-9]" value="<?php echo $produk['harga_produk']; ?>">
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Kode Produk</label>
        <input type="text" name="kode_produk" minlength="1" class="form-control" value="<?php echo $produk["kode_produk"]; ?>" required="required">
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Berat Produk</label><small> (Gram)</small>
        <input type="number" name="berat_produk" minlength="1" min="10" class="form-control" required="required" value="<?php echo $produk['berat_produk']; ?>">
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Stok Barang</label>
        <input type="number" name="stock_produk" class="form-control" min="0" required="required" value="<?php echo $produk['stock_produk']; ?>">
      </div>
      
      <div class="form-group col-md-12">
        <label>Spesifikasi Produk</label>
        <textarea class="form-control" name="spesifikasi" id="theeditor" required="required"><?php echo $produk['spesifikasi']; ?></textarea>
      </div>

      <div class="form-group col-md-12">
        <label>Deskripsi / Ulasan Singkat</label>
        <textarea class="form-control" name="deskripsi" id="theeditor1"><?php echo $produk['deskripsi']; ?></textarea>
      </div>

      <div class="form-group col-md-12">
        <label>Gambar</label><br/>
        <img src="<?php echo base_url("assets/images/produk/".$produk["foto_produk"]); ?>" width="350" required="required">
        <br/><br/>
        <input type="file" name="foto_produk" class="form-control">
        <small>*klik untuk mengganti foto produk</small>
      </div>

      <div class="form-group pull-right">
        <br>
        <br>
         <a href="javascript:history.back(-1)" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-success" name="simpan" value="simpan">Simpan</button>
      </div>
    </form>
  </div>
</div>
