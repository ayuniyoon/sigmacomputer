<div class="x_title">
  <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?></h2>
  <div class="pull-right">
    <a href="<?php echo base_url('admin/produk/tambah'); ?>" class="btn btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Produk</a>
  </div>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <table class="table table-striped table-hover table-bordered" id="datatable">
    <thead>
      <tr>
        <th width="5px" style="display: none;">No</th>
        <th width="8%">Kode</th>
        <th>Nama Produk</th>
        <th width="10%">Merk / Brand<SS/th>
        <th width="10%">Sub Kategori</th>
        <th style="width: 10%;">Harga</th>
        <th style="width: 5%;">Stock Tersedia</th>
        <th style="width: 5%;">Berat (gram)</th>
        <th style="width: 21%; text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($produk as $key => $value): ?>
        <tr>
          <td style="display: none;"><?php echo $key+1; ?></td>
          <td><?php echo $value['kode_produk']; ?></td>
          <td><?php echo $value['nama_produk']; ?></td>
          <td><?php echo $value['nama_brand']; ?></td>
          <td><?php echo $value['nama_subkategori']; ?></td>
          <td><?php echo rupiah($value['harga_produk']); ?></td>
          <td style="text-align: center;">
            <?php if ($value['stock_produk']==0) { echo "<p style=color:red;font-weight:600;>Kosong</p>";} else{echo $value['stock_produk']." unit";} ?>
          </td>
          <td style="text-align: center;"><?php echo $value['berat_produk']; ?></td>
          <td style="text-align: right;">
            <a href="<?php echo base_url("admin/produk/detail/$value[id_produk]") ?>" class="btn btn-sm btn-info">Detail</a>
            <a href="<?php echo base_url("admin/produk/ubah/$value[id_produk]") ?>" class="btn btn-sm btn-warning">Ubah</a>
            <a href="<?php echo base_url("admin/produk/hapus/$value[id_produk]"); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin Ingin Menghapus?')">Hapus</a>
          </td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>
