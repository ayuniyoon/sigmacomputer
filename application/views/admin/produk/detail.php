<div class="x_title">
  <h2><?php echo $ecom; ?></h2>
  <div class="pull-right">
   <a href="javascript:history.back()" class="btn btn-sm btn-default"><i class="fa fa-history"></i> Kembali</a>
   <a href="<?php echo base_url("produk/$detail[url_produk]"); ?>" target="_blank" class="btn btn-sm btn-default"><i class="fa fa-external-link"></i> Lihat</a>
   <a href="<?php echo base_url("admin/produk/ubah/$detail[id_produk]") ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
 </div>
 <div class="clearfix"></div>
</div>
<div class="x_content">

  <div class="col-md-5 col-sm-4 col-xs-12">
    <div class="product-image">
      <img src="<?php echo base_url("assets/images/produk/$detail[foto_produk]") ?>" alt="<?php echo $detail['nama_produk'] ?>" />
    </div>
  </div>
  
  <div class="col-md-7 col-sm-8 col-xs-12" style="border:0px solid #e5e5e5;">
    <h3 class="prod_title">Produk : <?php echo $detail['nama_produk']; ?></h3>
    <h4>Kode Produk : <b><?php echo $detail['kode_produk']; ?></b></h4>
    <div class="">
      <ul class="list-unstyled project_files">
        <li><i class="fa fa-tag"></i> Subkategori : <?php echo $detail['nama_subkategori']; ?></li>
        <li><i class="fa fa-archive"></i>  Stok : <?php if ($detail['stock_produk']==0){echo "<b style=color:red;>Stok Habis</b>";}else{echo $detail['stock_produk']." unit";} ?>
          <li><i class="fa fa-cube"></i> Berat : <?php echo $detail['berat_produk']; ?> gram</li>
        </ul>
      </div>
      <div class="" >
        <div class="product_price">
          <h2 class="price"><?php echo rupiah($detail['harga_produk']) ?></h2>
        </div>
      </div>
    </div>

    <div class="col-md-12">
    <br>
      <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Spesifikasi</a>
          </li>
          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Deskripsi</a>
          </li>
        </ul>
        <div id="myTabContent" class="tab-content">
          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            <p><?php echo $detail['spesifikasi']; ?></p>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
            <p style="text-align: justify;"><?php echo $detail['deskripsi']; ?></p>
          </div>
        </div>
      </div>

    </div>
  </div>