<div class="x_title">
  <h2>Tambah Produk Baru </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">

  <?php if ($cek=="berhasil"): ?>
    <div class="alert alert-success">Data Berhasil Ditambahkan</div>
    <meta charset="utf-8" http-equiv="refresh" content="2;url=<?php echo base_url("admin/produk/"); ?>">
  <?php endif ?>

  <div class="">
    <form class="form" method="post" enctype="multipart/form-data" style="border: none;">

      <div class="form-group col-md-4 col-sm-4">
        <label>Kategori</label>
        <select class="form-control" name="id_kategori" onchange="submit()" required>
          <option value="">- Pilih Kategori -</option>
          <?php foreach ($kategori as $key => $value): ?>
            <option value="<?php echo $value['id_kategori'] ?>" <?php if ($id_kategori==$value['id_kategori']){echo "selected";} ?>><?php echo $value['nama_kategori']; ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Sub Kategori</label>
        <select class="form-control" name="id_subkategori" required>
          <option value="">- Pilih Sub Kategori -</option>
          <?php foreach ($subkategori as $key => $value): ?>
            <option value="<?php echo $value['id_subkategori'] ?>"><?php echo $value['nama_subkategori']; ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Brand</label>
        <select class="form-control" name="id_brand" required>
          <option value="">- Pilih Brand -</option>
          <?php foreach ($brand as $key => $value): ?>
            <option value="<?php echo $value['id_brand'] ?>"><?php echo $value['nama_brand']; ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group col-md-12">
        <label>Nama Produk</label>
        <input type="text" name="nama_produk" class="form-control" required="required" minlength="5">
      </div>

      <div class="form-group col-md-12">
        <label>Harga Produk</label><small> tanpa tanda titik / koma</small>
        <input type="number" name="harga_produk" class="form-control" required="required" min="0" placeholder="80000" pattern="[0-9]">
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Kode Produk</label>
        <input type="text" name="kode_produk" class="form-control" required minlength="1" placeholder="SJ45">
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Berat Produk</label><small> (Gram)</small>
        <input type="number" name="berat_produk" class="form-control" required="required" minlength="1" min="10" placeholder="100">
      </div>

      <div class="form-group col-md-4 col-sm-4">
        <label>Stok Barang</label>
        <input type="number" name="stock_produk" class="form-control" required="required" min="0" placeholder="10">
      </div>

      <div class="form-group col-md-12">
        <label>Spesifikasi Produk</label>
        <textarea class="form-control" name="spesifikasi" id="theeditor" required="required"></textarea>
      </div>

      <div class="form-group col-md-12">
        <label>Deskripsi / Ulasan Singkat</label>
        <textarea class="form-control" name="deskripsi" id="theeditor1" required></textarea>
      </div>

      <div class="form-group col-md-12">
        <label>Gambar</label>
        <input type="file" name="foto_produk" class="form-control" required>
      </div>

      <div class="form-group pull-right">
        <br>
        <br>
        <a href="javascript:history.back(-1)" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-success" name="simpan" value="simpan">Simpan</button>
      </div>

    </form>
  </div>
</div>
