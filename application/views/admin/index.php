  <div class="row"> 
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="col-md-3 tile">
        <span><i class="fa fa-credit-card"></i> Penjualan</span>
        <h2><?php echo rupiah($sales['SUM(total_pembelian)']); ?></h2>
        <br>
        <span><small>Total Penjualan bulan ini</small></span>
      </div>
      <div class="col-md-3 tile">
        <span><i class="fa fa-shopping-cart"></i> Pesanan</span>
        <h2><?php echo $jml_penjualan; ?></h2>
        <!-- <span class="sparkline_two" style="height: 160px;">
          <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
        </span> -->
        <br>
        <span><small>Total pesanan masuk bulan ini </small></span>
      </div>
      <div class="col-md-3 tile">
        <span><i class="fa fa-money"></i> Konfirmasi Pembayaran</span>
        <h2><?php echo $konfirmasi_bln; ?></h2>
        <!-- <span class="sparkline_three" style="height: 160px;">
          <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
        </span> -->
        <br>
        <span><small>Total konfirmasi masuk bulan ini </small></span>
      </div>
      <div class="col-md-3 tile">
        <span><i class="fa fa-users"></i> Member</span>
        <h2><?php echo $member_bln; ?></h2>
        <!-- <span class="sparkline_two" style="height: 160px;">
          <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
        </span> -->
        <br>
        <span><small>Total member mendaftar bulan ini</small></span>
      </div>
    </div>   

  </div>
</div>

<div class="x_panel" style="min-height: 500px;">
  <div class="x_title">
    <h2><i class="fa fa-bars"></i> Aktifitas Bulan Ini</h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">

    <div class="" role="tabpanel" data-example-id="togglable-tabs">
      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-line-chart"></i> Grafik Penjualan</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-shopping-cart"></i> Pesanan</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-money"></i> Konfirmasi Pembayaran</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-users"></i> Member</a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
          <!-- chart -->
          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
        <!-- tab 2 -->
        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
          <table id="datatable" class="table table-striped table-bordered" width="100%">
            <thead>
              <tr>
                <th style="display: none;">&nbsp;</th>
                <th>Nota</th>
                <th>Member</th>
                <th>Total Bayar</th>
                <th>Tgl.Transaksi</th>
                <th>Status Bayar</th>
                <th>Status Kirim</th>
                <th>Kurir</th>
                <th style="text-align: center;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($penjualan_array as $key => $value): ?>
                <tr>
                  <td><?php echo $value['id_transaksi']; ?></td>
                  <td><?php echo $value['email_member']; ?></td>
                  <td><?php echo rupiah($value['total_pembelian']); ?></td>
                  <td><?php echo date("d M Y", strtotime($value['tgl_transaksi'])); ?></td>
                  <td><?php 
                    if($value['status_pembelian']==0){ ?>
                    <span class="label label-danger">Belum Diterima</span>
                    <?php }else{ ?>
                    <span class="label label-success">Lunas</span>
                    <?php } ?>
                  </td>
                  <td>
                    <?php if($value['status_pengiriman']==0){ ?>
                    <span class="label label-danger">Belum Dikirim</span>
                    <?php }else{ ?>
                    <span class="label label-success">Dikirim</span>
                    <?php } ?>
                  </td>
                  <td><?php echo $value['ekspedisi']; ?></td>
                  <td align="center"><a href="<?php echo base_url("admin/transaksi/detail/".$value['id_transaksi']); ?>" class="btn btn-default btn-xs">Detail</a></td>
              </tr>
            </tbody>
            <?php endforeach ?>
          </table>
        </div>
        <!-- tab 3 -->
        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
          <table id="datatable" class="table table-striped table-bordered" width="100%">
            <thead>
              <tr>
                <th style="display: none;">&nbsp;</th>
                <th>Nota</th>
                <th>Bank Tujuan</th>
                <th>Nama Pengirim</th>
                <th>Jml.Transfer</th>
                <th>Status Bayar</th>
                <th>Tgl.Transfer</th>
                <th>Tgl.Konfirmasi</th>
                <th style="text-align: center;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($konfirmasi as $key => $value): ?>
                <tr>
                  <td style="display: none;"><?php echo $key+1; ?></td>
                  <td><?php echo $value['id_transaksi']; ?></td>
                  <td><?php echo $value['nama_bank']; ?></td>
                  <td><?php echo $value['nama_pengirim']; ?></td>
                  <td><?php echo rupiah($value['jumlah_pembayaran']); ?></td>
                  <td>
                    <?php if ($value['status_pembelian']==0){ ?>
                    <span class="label label-danger">Belum Diterima</span>
                    <?php }else{ ?>
                    <span class="label label-success">Lunas</span>
                    <?php } ?>
                  </td>
                  <td><?php echo date("d M Y", strtotime($value['tgl_transfer'])); ?></td>
                  <td><?php echo date("d M Y", strtotime($value['tgl_konfirmasi'])); ?></td>
                  <td align="center"><a href="<?php echo base_url("admin/transaksi/pembayaran/".$value['id_transaksi']); ?>" class="btn btn-default btn-xs">Lihat</a></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <!-- tab 4 -->
        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
         <table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Email</th>
              <th>Nama Lengkap</th>
              <th>No. Telp</th>
              <th>Alamat</th>
            </tr>
          </thead>
          <tbody>
           <?php foreach ($member as $key => $value): ?>
            <tr>
              <td><?php echo $key+1; ?></td>
              <td><?php echo $value['email_member']; ?></td>
              <td><?php echo $value['nama_member']; ?></td> 
              <td><?php echo $value['no_hp']; ?></td>
              <td><?php echo $value['alamat_member']; ?></td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table>
      </div>

 </div>
</div>

</div>
