</div>
</div>
</div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Toko Online Sigma Computer Prambanan &copy; 2018
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<!-- modal pengiriman -->
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pengiriman Barang</h4>
    </div>
    <form method="post">
        <div class="modal-body">
            <div class="form-group">
                <input type="hidden" name="id_transaksi" id="transaksi" class="form-control">
            </div>
            <div class="form-group">
                <label>No. Resi</label>
                <input type="text" name="no_resi" class="form-control" id="resi">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button id="save" type="button" class="btn btn-primary">Kirim</button>
        </div>
    </form>
</div>
</div>
</div>

<!-- CKeditor -->
<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<script>
    CKEDITOR.replace("theeditor");
    CKEDITOR.replace("theeditor1");
</script>

<!-- jQuery -->
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/vendors/fastclick/lib/fastclick.js') ?>"></script>
<!-- NProgress -->
<script src="<?php echo base_url('assets/vendors/nprogress/nprogress.js') ?>"></script>
<!-- jQuery custom content scroller -->
<script src="<?php echo base_url('assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') ?>"></script>

<!-- Datatables -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>

<!-- Pie & chart -->
<script src="<?php echo base_url("assets/vendors/Chart.js/dist/Chart.min.js") ?>"></script>
<!-- jQuery Sparklines -->
<script src="<?php echo base_url("assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js") ?>"></script>
<!-- easy-pie-chart -->
<script src="<?php echo base_url("assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js") ?>"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo base_url("vendors/bootstrap-progressbar/bootstrap-progressbar.min.js") ?>"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/dist/js/custom.min.js') ?>"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
    // grafik penjualan
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Grafik Penjualan Perbulan'
        },
        xAxis: {
            categories: [
            <?php foreach ($penjualan as $key => $value): ?>
            '<?php echo date("M",strtotime($value[0]['tgl_transaksi'])) ?>',
        <?php endforeach ?>
        ]
    },
    yAxis: {
        title: {
            text: 'Penjualan'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Total Penjualan Per Bulan',
        data: 
        [
        <?php foreach ($penjualan as $key => $value): ?>
        <?php echo $value[0]['SUM(total_pembelian)']; ?>,
    <?php endforeach ?>
    ]
}]
});
</script>
<script>
    $(document).on('click','.kirim',function(){
        var id_transaksi = $(this).data('id');
        if(id_transaksi!==0)
        {
            $("#transaksi").val(id_transaksi);
            $("#modal").modal("show");
        }
    });
    $("#save").on('click',function()
    {
        var resi = $("#resi").val();
        var id_transaksi = $("#transaksi").val();
        
        $.ajax({
            url:'<?php echo base_url("admin/transaksi/kirim") ?>',
            method:'POST',
            data:"no_resi="+resi+"&id_transaksi="+id_transaksi,
            success:function(hasil)
            {
                if(hasil=="berhasil")
                {
                    alert("No. Resi berhasil di inputkan");
                    location="transaksi";
                }
                else
                {
                    alert("No. Resi gagal di inputkan");
                    location="transaksi";
                }
            }
        })
    })
</script>
</body>
</html>