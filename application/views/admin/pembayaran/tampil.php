<div class="x_title">
  <h2><i class="fa fa-list-ul"></i><?php echo " ".$judul; ?> </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <table id="datatable" class="table table-striped table-hover">
    <thead>
      <tr>
        <th style="display: none;">No</th>
        <th>ID Transaksi</th>
        <th>Bank Tujuan</th>
        <th>Nama Pengirim</th>
        <th>Jml Pembayaran</th>
        <th>Tgl. Konfirmasi</th>
        <th>Status</th>
        <th style="width: 20%;text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($bayar as $key => $b): ?>
        <tr>
          <td style="display: none;"><?php echo $key+1; ?></td>
          <td><?php echo $b['id_transaksi']; ?></td>
          <td><?php echo $b['nama_bank']; ?></td>
          <td><?php echo $b['nama_pengirim']; ?></td>
          <td><?php echo rupiah($b['jumlah_pembayaran']); ?></td>
          <td><?php echo date("d M Y", strtotime($b['tgl_konfirmasi'])); ?></td>
          <td>
            <?php if ($b['status_pembelian']==0){ ?>
              <span class="label label-danger">Belum Diterima</span>
            <?php }else{ ?>
              <span class="label label-success">Lunas</span>
            <?php } ?>
          </td>
          <td style="text-align: right;">
            <div>
              <a href="<?php echo base_url("admin/pembayaran/bayar/$b[id_transaksi]"); ?>" class="btn btn-success btn-sm" onclick="return confirm('Pembayaran Yakin sudah diterima? Bagian ini tidak bisa diganti');" title="Terima Pembayaran" data-toggle="tooltip" data-placement="top">Terima</a>
              <a href="<?php echo base_url("admin/pembayaran/detail/$b[id_pembayaran]"); ?>" class="btn btn-info btn-sm" title="Detail Pembayaran" data-toggle="tooltip" data-placement="top">Detail</a>
            </div>
          </td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>
<div class="modal fade" id="bukti-gambar" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Bukti Pembayaran</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <img src="<?php echo base_url("assets/images/konfirmasi/$b[id_pembayaran]") ?>">
        </div>
      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<script>
 $(document).on( "click", '.edit_button',function(e) {
  var id = $(this).data('id_pembayaran');
  $(".business_skill_id").val(id);
});
</script>