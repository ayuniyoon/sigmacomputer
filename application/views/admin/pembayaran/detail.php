<div class="x_title">
	<h2><?php echo $judul; ?></h2>
	<div class="pull-right">
		<a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-history"></i> Kembali</a>
	</div>
	<div class="clearfix"></div>
</div>
<div class="x_content" style="min-height: 500px;">
	<?php if (empty($pembayaran)): ?>
		<div class="alert alert-danger">Belum ada pembayaran yang masuk</div>
	<?php else: ?>
		<div class="col-md-4 col-sm-4 col-xs-12 profile_left">
			<div class="profile_img">
				<div id="crop-avatar">
					<br>
					<img class="img-responsive avatar-view" src="<?php echo base_url("assets/images/konfirmasi/$pembayaran[bukti_pembayaran]") ?>">
				</div>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="clearfix"></div>
			<div class="x_content">
				<table class="table table-hover table-stripped">
					<thead>
						<tr>
							<th colspan="3">Transaksi Nomor : P <?php echo $pembayaran['id_transaksi']; ?></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="20%">Status Bayar</td>
							<td width="2px">:</td>
							<td>
								<?php if ($pembayaran['status_pembelian']==0){ ?>
								<span class="label label-danger">Belum Diterima</span>
								<?php }else{ ?>
								<span class="label label-success">Lunas</span>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td>Bank Tujuan</td>
							<td>:</td>
							<td><?php echo $pembayaran['nama_bank']; ?></td>
						</tr>
						<tr>
							<td>Nama Pengirim</td>
							<td>:</td>
							<td><?php echo $pembayaran['nama_pengirim']; ?></td>
						</tr>
						<tr>
							<td>Jumlah Transfer</td>
							<td>:</td>
							<td><?php echo rupiah($pembayaran['jumlah_pembayaran']); ?></td>
						</tr>
						<tr>
							<td>Tgl.Transfer</td>
							<td>:</td>
							<td><?php echo date('d M Y', strtotime($pembayaran['tgl_transfer'])); ?></td>
						</tr>
						<tr>
							<td>Tgl.Konfirmasi</td>
							<td>:</td>
							<td><?php echo date('d M Y', strtotime($pembayaran['tgl_konfirmasi'])); ?></td>
						</tr>
					</tbody>
				</table>
				<?php if ($pembayaran['status_pembelian']==1): ?>
					<div class="alert alert-success" style="color: #fff;">Transaksi Ini Telah Lunas</div>
				<?php else: ?>
					<form method="post">
						<input type="hidden" name="id_transaksi" value="<?php echo $pembayaran['id_transaksi']; ?>">
						<button class="btn btn-info" type="submit" style="color: #fff;" onclick="return confirm('Apakah Pembayaran sudah yakin diterima?');" >Terima Pembayaran</button>
					</form>
				<?php endif ?>
			</div>
		</div>

	<?php endif ?>
</div>
