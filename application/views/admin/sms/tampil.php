  <div class="x_title">
    <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?></h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content" style="min-height: 500px;">
    <div class="alert alert-success">Satu SMS hanya memuat maksimal 160 karakter</div>
  	<form class="form" method="post">
      <input type="hidden" name="id" value="<?php echo $temp['id'] ?>">
  		<div class="form-group col-md-4">
  			<label>SMS Transaksi</label>
  			<textarea name="temp_1" class="form-control" rows="5" minlength="10"><?php echo $temp['temp_1']; ?></textarea>
        <small>*dikirim kepada pembeli</small>
  		</div>
  		<div class="form-group col-md-4">
  			<label>SMS Notifikasi Administrator</label>
  			<textarea name="temp_2" class="form-control" rows="5" minlength="10"><?php echo $temp['temp_2']; ?></textarea>
        <small>*dikirim ke administrator</small>
  		</div>
      <div class="form-group col-md-4">
        <label>SMS Notifikasi Konfirmasi</label>
        <textarea name="temp_3" class="form-control" rows="5" minlength="10"><?php echo $temp['temp_3']; ?></textarea>
        <small>*dikirim ke administrator</small>
      </div>
  		<div class="pull-right">
        <br>
  			<button class="btn btn-success" type="submit">Update</button>
  		</div>
  	</form>
  	<br>
  	<br>
  	<br>
  </div>
</div>
<br/> 
