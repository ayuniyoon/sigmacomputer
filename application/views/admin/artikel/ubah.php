<div class="x_title">
  <h2><?php echo $judul; ?> </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  <div class="col-md-12 center-margin">
    <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label>Judul Artikel</label>
        <input type="text" class="form-control" name="judul_artikel" required="required" value="<?php echo $ubah["judul_artikel"] ?>">
      </div>
      <div class="form-group">
        <label>Slug</label>
        <input type="text" class="form-control" name="url_artikel" disabled="disabled" value="<?php echo $ubah["url_artikel"] ?>">
      </div>
      <div class="form-group">
        <label>Isi Artikel</label>
        <textarea class="form-control" id="theeditor" name="isi_artikel"><?php echo $ubah["isi_artikel"] ?></textarea>
      </div>
      <div class="form-group">
        <label>Gambar</label>
        <br/><br/>
        <img src="<?php echo base_url("assets/images/artikel/$ubah[gambar_artikel]")?>" width="300">
        <br />
        <br/><br/>
        <input type="file" name="gambar_artikel"><small>* klik jika ingin gambar ganti</small>
      </div>
      <br/>
      <div class="form-group">
        <label>Status Publikasi</label>
        <select class="form-control" name="status_artikel" required="required">
          <option value="Draft" <?php if ($ubah['status_artikel']=='Draft') {
            echo "selected";
          } ?>>Draft</option>
          <option value="Publish" <?php if ($ubah['status_artikel']=='Publish') {
            echo "selected";
          } ?>>Publish</option>
        </select>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group pull-right">
        <a href="javascript:history.back()" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-success">Simpan</button>
      </div>
    </form>
  </div>
</div>
