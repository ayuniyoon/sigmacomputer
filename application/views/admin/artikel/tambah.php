<div class="x_title">
  <h2>Tambah Artikel Baru </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  <div class="col-md-12 center-margin">
    <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label>Judul Artikel</label>
        <input type="text" class="form-control" name="judul_artikel" autofocus="autofocus" required="required">
      </div>
      <div class="form-group">
        <label>Isi Artikel</label>
        <textarea class="form-control" id="theeditor" name="isi_artikel"></textarea>
      </div>
      <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar_artikel" class="form-control" required="required">
      </div>
      <div class="form-group">
        <label>Status Publikasi</label>
        <select class="form-control" name="status_artikel">
          <option value="Draft">Draft</option>
          <option value="Publish">Publish</option>
        </select>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group pull-right">
        <button class="btn btn-default" onclick="javascript:history.back(-1)">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
      </div>
    </form>
  </div>
</div>
