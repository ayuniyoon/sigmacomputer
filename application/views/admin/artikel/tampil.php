<div class="x_title">
  <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?> </h2>
  <div class="pull-right">
    <a href="<?php echo base_url('admin/artikel/tambah'); ?>" class="btn btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah</a>
  </div>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <table id="datatable" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th width="5%">No</th>
        <th>Judul Artikel</th>
        <th width="10%">Status</th>
        <th width="13%">Tgl. Posting</th>
        <th style="width: 21%; text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($artikel as $key => $value): ?>
        <tr>
          <td><?php echo $key+1;?></td>
          <td><?php echo $value['judul_artikel'] ?></td>
          <td><?php echo $value['status_artikel'] ?></td>
          <td><?php echo date("d/m/Y", strtotime($value['tgl_posting'])) ?></td>
          <td style="text-align: right;">
            <a href="<?php echo base_url("admin/artikel/detail/$value[id_artikel]"); ?>" class="btn btn-sm btn-info">Detail</a>
            <a href="<?php echo base_url("admin/artikel/ubah/$value[id_artikel]"); ?>" class="btn btn-sm btn-warning">Ubah</a>
            <a href="<?php echo base_url("admin/artikel/hapus/$value[id_artikel]") ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin Ingin Menghapus?')">Hapus</a>
          </td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>
