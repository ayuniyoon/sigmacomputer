<div class="x_title">
  <h2>Detail Artikel / Post</h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
    <div class="profile_img">
      <!-- Current avatar -->
      <br>
      <img src="<?php echo base_url("assets/images/artikel/$detail[gambar_artikel]") ?>" width="100%" alt="<?php echo $detail['judul_artikel'] ?>" title="<?php echo $detail['judul_artikel'] ?>">  
    </div>
    <br />
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="detail-judul">
      <h2><?php echo $detail['judul_artikel'] ?></h2>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12 status-konten">
      <i class="fa fa-calendar"></i>  Tanggal Posting : <?php echo date("d/m/Y", strtotime($detail['tgl_posting'])) ?>
      &nbsp;  
      <i class="fa fa-briefcase"></i>  Status : 
      <?php if ($detail['status_artikel']=='Draft'){ ?>
      <span class="label label-warning">Draft</span>
      <?php }else{ ?>
      <span class="label label-success">Published</span>
      <?php } ?>
    </div>
    <!-- isi tab mulai bawah ini -->
    <div class="detail-isi">
      <?php echo $detail['isi_artikel'] ?>
    </div>
    <br/>
    <div class="pull-right">
      <a href="javascript:history.back(-1)" class="btn btn-default"><i class="fa fa-history m-right-xs"></i> Kembali</a>
      <a href="<?php echo base_url("admin/artikel/ubah/$detail[id_artikel]") ?>" class="btn btn-warning"><i class="fa fa-edit m-right-xs"></i> Edit</a>
    </div>
  </div>
</div>