<div class="x_title">
  <h2><?php echo $judul; ?></h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  <form class="form-horizontal form-label-left" method="post">
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Kolom Pengaturan</label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="kolom_pengaturan" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo $ubah["kolom_pengaturan"] ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Isi Pengaturan
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea class="form-control" name="isi_pengaturan"><?php echo $ubah["isi_pengaturan"] ?></textarea>
      </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group pull-right">
      <a href="javascript:history.back()" class="btn btn-default">Batal</a>
      <button type="submit" class="btn btn-success">Simpan</button>
    </div>
  </form>
  <br />
</div>