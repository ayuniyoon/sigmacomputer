<div class="x_title">
	<h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?></h2>
	<div class="pull-right">
		<button class="btn btn-primary" data-toggle="modal" data-target=".tambah-data"><i class="fa fa-plus"></i> Tambah</button>
	</div>
	<div class="clearfix"></div>
</div>
<div class="x_content">
	<table id="datatable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th width="5px" style="display: none;">No</th>
				<th width="11%">Kolom ID</th>
				<th>Kolom Pengaturan</th>
				<th>Isi Pengaturan</th>
				<th style="text-align: center;width: 20%;">Aksi</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach ($pengaturan as $key => $va): ?>
				<tr>
					<td style="display: none;"><?php echo $key+1; ?></td>
					<td style="text-align: center;"><?php echo $va['id_pengaturan']; ?></td>
					<td><?php echo $va['kolom_pengaturan']; ?></td>
					<td><?php echo $va['isi_pengaturan']; ?></td>
					<td style="text-align: right;">
						<a href="<?php echo base_url("admin/toko/ubah/$va[id_pengaturan]"); ?>" class="btn btn-warning btn-sm">Edit</a>
						<a href="<?php echo base_url("admin/toko/hapus/$va[id_pengaturan]"); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Ingin Mengapus?')">Hapus</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<!-- MODAL -->
<div class="modal fade tambah-data" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Tambah Pengaturan</h4>
        </div>
        <div class="modal-body">

          <form class="form-horizontal form-label-left" method="post">

            <label>Kolom Pengaturan</label>
            <input type="text" class="form-control" name="kolom_pengaturan" data-parsley-trigger="change" required="required" autofocus="autofocus" />
            <br/>

            <label>Isi Pengaturan</label>
            <textarea name="isi_pengaturan" required="required" autofocus="autofocus" class="form-control" placeholder="Isi Pengaturan"></textarea>
            <br/>

            <div class="pull-right">
             <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
             <button type="submit" class="btn btn-success">Simpan</button>
           </div>
         </form>
       </div>
       <div class="modal-footer">
       </div>

     </div>
   </div>
 </div>