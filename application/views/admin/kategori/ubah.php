<div class="x_title">
  <h2>Edit Kategori</h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  <form class="form-horizontal form-label-left" method="post">
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Edit Kategori</label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="nama_kategori" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo $ubah["nama_kategori"] ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Slug
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="url_brand" class="form-control col-md-7 col-xs-12" disabled="disabled" value="<?php echo $ubah["url_kategori"] ?>">
      </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group pull-right">
      <a href="javascript:history.back()" class="btn btn-default">Batal</a>
      <button type="submit" class="btn btn-success">Simpan</button>
    </div>
  </form>
  <br />
</div>