<div class="x_title">
  <h2><i class="fa fa-list-ul"></i><?php echo " ".$judul; ?> </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  

  <table id="datatable" class="table table-striped table-bordered" >
    <thead>
      <tr>
        <th style="display: none;">No</th>
        <th style="width: 5%;">Kode Transaksi</th>
        <th width="10px">Member</th>
        <th width="10px">Total Bayar</th>
        <th width="8px">Tgl. Transaksi</th>
        <th width="8px">Status Bayar</th>
        <th width="10px">Status Kirim</th>
        <th width="7px">Kurir</th>
        <th width="15%" style="text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($penjualan as $key => $jual): ?>
        <tr>
          <td style="display: none;"><?php echo $key+1; ?></td>
          <td><?php echo $jual['id_transaksi']; ?></td>
          <td><?php echo $jual['email_member']; ?></td>
          <td><?php echo rupiah($jual['total_pembelian']); ?></td>
          <td><?php echo date('d M Y', strtotime($jual['tgl_transaksi'])); ?></td>
          <td>
             <?php if ($jual['status_pembelian']==0){ ?>
              <span class="label label-danger">Belum Diterima</span>
            <?php }else{ ?>
              <span class="label label-success">Lunas</span>
            <?php } ?>
          </td>
          <td>
            <?php if ($jual['status_pengiriman']==0) { ?>
              <span class="label label-danger">Belum Dikirim</span>
            <?php }else{ ?>
              <span class="label label-success">Dikirim</span>
            <?php } ?>
          </td>
          <td><?php echo $jual['ekspedisi']; ?></td>
          <td align="right">
            <a href="<?php echo base_url("admin/transaksi/pembayaran/".$jual['id_transaksi']) ?>" class="btn btn-warning btn-sm")" title="Detail Pembayaran" data-toggle="tooltip" data-placement="top"><i class="fa fa-dollar"></i></a>
            <a data-id="<?php echo $jual['id_transaksi']; ?>" class="btn btn-success btn-sm kirim" title="Kirim Barang" data-toggle="tooltip" data-placement="top" data-target="#modal"><i class="fa fa-truck"></i></a>
            <a href="<?php echo base_url("admin/transaksi/detail/".$jual['id_transaksi']) ?>" class="btn btn-info btn-sm" title="Detail Transaksi" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>