<div class="x_title">
  <h2><?php echo $judul; ?></h2>
  <div class="pull-right">
    <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-history"></i> Kembali</a>
  </div>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <section class="content invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12 invoice-header">
        <h1>
          <i class="fa fa-globe"></i> Invoice.
          <small class="pull-right" style="font-size: 16px;">Pembelian Tgl : <?php echo date('d M Y', strtotime($ambil['tgl_transaksi'])); ?></small>
        </h1>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 col-md-4 invoice-col">
        Dikirim Dari:
        <address>
          <strong><?php echo $nama_toko['isi_pengaturan']; ?></strong>
          <br><?php echo $alamat['isi_pengaturan']; ?>
          <br>Telepon : <?php echo $telepon['isi_pengaturan']; ?>
          <br>Email: <?php echo $email['isi_pengaturan']; ?>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 col-md-4 invoice-col">
        <b>Pembeli:</b>
        <address>
          <strong><?php echo $ambil['nama_member']; ?></strong>
          <br>Email: <?php echo $ambil['email_member']; ?>
          <br>Telepon: <?php echo $ambil['no_hp']; ?>
          <br><?php echo $ambil['alamat_member']; ?>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 col-md-4 invoice-col">
        <b>Kepada Yth. :</b>
        <address>
          <strong><?php echo $ambil['nama_penerima']; ?></strong>
          <br><?php echo $ambil['alamat_penerima']; ?>
          <br><?php echo $ambil['nama_kecamatan'].", ".$ambil['kota_tujuan'].", ".$ambil['tipe_kota'].", ".$ambil['kode_pos']; ?>
          <br>Telepon: <?php echo $ambil['telepon_penerima']; ?>
          <br>Kurir: <?php echo $ambil['ekspedisi']; ?>,
          Paket Kurir: <?php echo $ambil['paket_ekspedisi']; ?>,
          Estimasi: <?php echo $ambil['estimasi_hari']; ?>
        </address>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div>
      <h4><strong>Transaksi Nomor : P <?php echo $ambil['id_transaksi']; ?></strong></h4>
      Status Bayar : 
       <?php if ($ambil['status_pembelian']==0){ ?>
       <span class="label label-danger">Belum Lunas</span> 
       <?php }else{ ?>
       <span class="label label-success">Lunas</span> 
       <?php } ?>
      Status Kirim :
      <?php if ($ambil['status_pengiriman']==0){ ?>
       <span class="label label-danger">Belum Dikirim</span>
       <?php }else{ ?>
       <span class="label label-success">Dikirim</span> tanggal : <?php echo date("d M Y", strtotime($ambil['tgl_pengiriman'])); ?><br/>
        No.Resi : <?php echo $ambil['no_resi']; ?>
       <?php } ?>
    </div>
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table">
         <br>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Nama Produk</th>
              <th>Harga Produk</th>
              <th>Jml.Beli</th>
              <th>Sub Harga</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $total_harga = 0;
            foreach ($detail as $key => $d): ?>
            <tr>
              <td><?php echo $d['nama_produk']; ?></td>
              <td><?php echo rupiah($d['harga_produk']); ?></td>
              <td><?php echo $d['jumlah_beli']; ?></td>
              <td><?php echo rupiah($d['sub_harga']); ?></td>
              <?php $total_harga+=$d['sub_harga']; ?>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-6">
      <table class="table table-bordered">
        <tbody>
          <?php foreach ($bank as $key => $b): ?>
            <tr>
              <td><img src="<?php echo base_url("assets/images/bank/$b[logo_bank]"); ?>" alt="<?php echo $b['nama_bank']; ?>" width="65"></td>
              <td style=""><?php echo $b['no_rekening']; ?></td>
              <td style="width: 40%;"><?php echo " a/n ".$b['nama_pemilik']; ?></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      <div class="table-responsive">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <th style="width:50%">Total Harga:</th>
              <td><?php echo rupiah($total_harga); ?></td>
            </tr>
            <tr>
              <th>Ongkos Kirim:</th>
              <td><?php echo rupiah($ambil['ongkos_kirim']); ?></td>
            </tr>
            <tr>
              <th>Total Bayar:</th>
              <td style="font-size: 20px;"><?php echo rupiah($ambil['total_pembelian']); ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <button class="btn btn-default pull-right" onclick="window.print();"><i class="fa fa-print"></i> Cetak</button>
    </div>
  </div>
</section>
</div>