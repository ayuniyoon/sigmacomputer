<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">  
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $admin['nama_admin']; ?><i class="fa fa-angle-down" style="margin-left: 10px;"></i></a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="<?php echo base_url("admin/home/profile") ?>"> Profile</a></li>
            <li><a href="<?php echo base_url("admin/admin/logout"); ?>" onclick="return confirm('Yakin Ingin Logout?');"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
        
      </ul>
    </nav>
  </div>
</div>
<!-- end topnav -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">