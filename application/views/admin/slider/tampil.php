<div class="x_title">
	<h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?></h2>
	<div class="clearfix"></div>
</div>
<div class="x_content" style="min-height: 520px;">
	<div class="row">
		<?php foreach ($slider as $key => $value): ?>
			<div class="col-md-4">
				<div class="thumbnail responsive" style="height: 180px;">
					<img src="<?php echo base_url("assets/images/slider/$value[gambar]"); ?>" alt="<?php echo $value['alt_gambar']; ?>">
				</div>
					<p class="caption" style="margin-top: -10px;"><a href="<?php echo base_url("admin/slider/hapus/$value[id_slider]"); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Yakin Ingin Menghapus?')"><i class="fa fa-trash"></i></a> <?php echo $value['alt_gambar']; ?></p> 	
			</div>
		<?php endforeach ?>	
	</div>
	<div>
		<br>
		<br>
		<button class="btn btn-primary" data-toggle="modal" data-target=".tambah-slider"><i class="fa fa-plus"></i> Tambah Slider</button>
	</div>
</div>

<!-- modal -->
<div class="modal fade tambah-slider" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Tambah Slider Baru </h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
					<label>Gambar</label>
					<small>minimal ukuran gambar 500x500px</small>
					<input type="file" class="form-control" name="gambar" data-parsley-trigger="change" required="required" autofocus="autofocus" />
					<br/>

					<label>Nama Slider</label>
					<input type="text" class="form-control" name="alt_gambar" data-parsley-trigger="change" required="required" autofocus="autofocus" />
					<br/>


					<div class="pull-right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-success">Simpan</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>