<div class="x_title">
  <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?> </h2>
  <div class="pull-right">
    <a href="<?php echo base_url('admin/informasi/tambah'); ?>" class="btn btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah</a>
  </div>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <table id="datatable" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th width="5%">No</th>
        <th>Judul Informasi</th>
        <th width="10%">Status</th>
        <th width="13%">Tgl. Posting</th>
        <th style="width: 21%; text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
     <?php foreach ($info as $key => $value): ?>
      <tr>
        <td><?php echo $key+1; ?></td>
        <td><?php echo $value["judul_informasi"] ?></td>
        <td><?php echo $value["status_informasi"] ?></td>
        <td><?php echo date("d/m/Y", strtotime($value["tgl_posting"])) ?></td>
        <td style="text-align: right;">
          <a href="<?php echo base_url("admin/informasi/detail/$value[id_informasi]") ?>" class="btn btn-info btn-sm">Detail</a>
          <a href="<?php echo base_url("admin/informasi/ubah/$value[id_informasi]") ?>" class="btn btn-warning btn-sm">Ubah</a>
          <a href="<?php echo base_url("admin/informasi/hapus/$value[id_informasi]") ?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Ingin Menghapus?')">Hapus</a>
        </td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
</div>
