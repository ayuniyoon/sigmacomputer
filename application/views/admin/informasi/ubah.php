<div class="x_title">
  <h2><?php echo $judul; ?> </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  <div class="col-md-12 center-margin">
    <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label>Judul Informasi</label>
        <input type="text" class="form-control" name="judul_informasi" required="required" value="<?php echo $ubah["judul_informasi"] ?>">
      </div>
      <div class="form-group">
        <label>Slug</label>
        <input type="text" class="form-control" name="url_informasi" disabled="disabled" value="<?php echo $ubah["url_informasi"] ?>">
      </div>
      <div class="form-group">
        <label>Isi Informasi</label>
        <textarea class="form-control" id="theeditor" name="isi_informasi"><?php echo $ubah["isi_informasi"] ?></textarea>
      </div>
      <div class="form-group">
        <label>Status Publikasi</label>
        <select class="form-control" name="status_informasi" required="required">
          <option value="Draft" <?php if ($ubah['status_informasi']=='Draft') {echo "selected";} ?>>Draft</option>
          <option value="Publish" <?php if ($ubah['status_informasi']=='Publish') {echo "selected"; } ?> >Publish</option>
        </select>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group pull-right">
        <a href="javascript:history.back(-1)" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-success">Simpan</button>
      </div>
    </form>
  </div>
</div>
