<div class="x_title">
  <h2><?php echo $judul ?></small></h2>
  <div class="pull-right">
    <a class="btn btn-default" href="javascript:history.back()"><i class="fa fa-history"></i> Kembali</a>
    <a href="<?php echo base_url("$detail[url_informasi]") ?>" class="btn btn-default">Lihat <i class="fa fa-external-link"></i></a>
    <a href="<?php echo base_url("admin/informasi/ubah/$detail[id_informasi]") ?>" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</a>
  </div>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <div class="col-md-12 col-lg-12 col-sm-12">
    <div class="detail-post">
      <div class="detail-judul">
        <h2><?php echo $detail['judul_informasi']; ?></h2>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 status-konten">
        <i class="fa fa-calendar"></i>  Tanggal Posting : <?php echo date("d/m/Y", strtotime($detail['tgl_posting'])) ?>
        &nbsp; 
        <i class="fa fa-briefcase"></i>  Status : 
        <?php if ($detail['status_informasi']=='Draft'){ ?>
        <span class="label label-warning">Draft</span>
        <?php }else{ ?>
        <span class="label label-success">Published</span>
        <?php } ?>
      </div>
      <div class="detail-isi">
        <?php echo $detail['isi_informasi'] ?>
      </div>
      <br/>
      <br />
      <br />
    </div>
  </div>
  
    <div class="clearfix"></div>
  </div>