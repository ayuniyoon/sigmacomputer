<div class="x_title">
  <h2><i class="fa fa-list-ul"></i> <?php echo $judul; ?> </h2>
  <div class="pull-right">
    <a href="<?php echo base_url("admin/brand/tambah"); ?>" class="btn btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Brand</a>
  </div>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <table id="datatable" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th width="5%">No</th>
        <th>Brands Produk</th>
        <th width="20%">Slug</th>
        <th style="width: 15%; text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($brand as $key => $value): ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td><?php echo $value["nama_brand"] ?></td>
          <td><?php echo $value["url_brand"] ?></td>
          <td style="text-align: right;">
            <a href="<?php echo base_url("admin/brand/ubah/$value[id_brand]")?>" class="btn btn-warning btn-sm ">Ubah</a>
            <a href="<?php echo base_url("admin/brand/hapus/$value[id_brand]")?>" class="btn btn-danger btn-sm " onclick="return confirm('Yakin Ingin Menghapus?')">Hapus</a>
          </td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>