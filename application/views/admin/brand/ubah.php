<div class="x_title">
  <h2><?php echo $judul; ?> </h2>
  <div class="clearfix"></div>
</div>
<div class="x_content">
  <br />
  <?php if (validation_errors()): ?>
    <div class="alert alert-info alert-dismissible" role="alert" style="color: #fff;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <?php echo validation_errors(); ?>
    </div>
  <?php endif ?>
  <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Brand
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="nama_brand" class="form-control col-md-7 col-xs-12" required minlength="2" value="<?php echo $ubah["nama_brand"] ?>">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Slug
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="url_brand" class="form-control col-md-7 col-xs-12" disabled="disabled" value="<?php echo $ubah["url_brand"] ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Logo Brand
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <img src="<?php echo base_url("assets/images/brand/$ubah[gambar_brand]") ?>" width="200">
      </div>    
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Ganti Logo
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="file" name="gambar_brand">
      </div>
    </div> 
    <div class="ln_solid"></div>
    <div class="form-group pull-right">
      <a href="javascript:history.back()" class="btn btn-default">Batal</a>
      <button type="submit" class="btn btn-success">Simpan</button>
    </div>
  </form>
</div>