<section>
	<div class="container" style="min-height: 580px;">
		<div class="row">
			<div class="col-md-9 col-md-push-3" style="margin-bottom: 15px;">
				<!-- mulai konten-->
				<div class="panel detail-konten">
					<h1><?php echo $mpembayaran['judul_informasi']; ?></h1>
					<i class="fa fa-calendar"></i>  Tanggal Posting : <?php echo date("d M Y", strtotime($mpembayaran['tgl_posting'])) ?>
					<br/>
					<div class="detailblog">
						<p><?php echo $mpembayaran['isi_informasi']; ?></p>
					</div>
					<div>
						<br>
						<table class="table table-striped table-hover">
							<tbody>
								<?php foreach ($bank as $key => $value): ?>
									<tr>
										<td width="60"><img src="<?php echo base_url("assets/images/bank/$value[logo_bank]"); ?>" width="60"></td>
										<td><?php echo $value['nama_bank']; ?></td>
										<td><?php echo $value['no_rekening']; ?></td>
										<td>a/n</td>
										<td><?php echo $value['nama_pemilik']; ?></td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						<br>
					</div>
				</div>
				<a href="javascript:history.back(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
				<!-- end konten-->
			</div>
			<div class="col-md-3 col-md-pull-9">
				<div class="panel panel-default">
					<div class="panel-heading panel-kategori">
						<h3 class="panel-title">KATEGORI</h3>
					</div>
					<ul class="list-group kategori">
						<?php foreach ($kategori as $key => $k): ?>
							<a data-toggle="collapse" data-target="#<?php echo $key; ?>" class="list-group-item"><?php echo $k['nama_kategori']; ?> <i class="fa fa-angle-down pull-right"></i></a>
							<ul class="list-group collapse" id="<?php echo $key ?>">
								<?php foreach ($subkategori as $key => $sk): ?>
									<?php if ($sk['id_kategori']==$k['id_kategori']): ?>
										<li style="list-style: none;"><a href="<?php echo base_url("kategori/$k[url_kategori]/$sk[url_subkategori]"); ?>" class="list-group-item" style="padding-left: 20px;"><i class="fa fa-angle-right" style="padding-right: 10px;"></i><?php echo $sk['nama_subkategori']; ?></a></li>
									<?php endif ?>
								<?php endforeach ?>
							</ul>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<!-- Untuk Produk col-md-12 -->