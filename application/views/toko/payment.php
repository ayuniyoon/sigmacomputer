<div class="container" style="min-height: 580px;">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Terima Kasih, Anda akan menerima SMS Pemberitahuan sesaat lagi.</strong> Silahkan untuk mencatat nomor rekening dibawah ini untuk proses pembayaran.
          </div>
          <h3>
            <i class="fa fa-globe"></i> Invoice.
            <small class="pull-right" style="font-size: 14px;">Pembelian Tgl: <?php echo date("d M Y", strtotime($invoice['tgl_transaksi'])) ?></small>
          </h3>
        </div>
      </div>
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
         Pengirim :
         <address>
          <strong><?php echo $nama_toko['isi_pengaturan']; ?></strong>
          <br><?php echo $alamat['isi_pengaturan']; ?>
          <br>Telepon : <?php echo $telepon['isi_pengaturan']; ?>
          <br>Whatsapp : <?php echo $wa['isi_pengaturan']; ?>
          <br>Email: <?php echo $email['isi_pengaturan']; ?>
        </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Pembeli:</b>
          <address>
            <strong><?php echo $invoice['nama_member']; ?></strong>
            <br>Email: <?php echo $invoice['email_member']; ?>
            <br>Telp:
            <?php if ($invoice['no_hp']==null){ ?>
            <?php echo "<a href=".base_url("member").">tidak lengkap</a>"; ?>
            <?php }else{ ?>
            <?php echo $invoice['no_hp']; ?>
            <?php } ?>
            <br>Alamat: <?php if ($invoice['alamat_member']==null){ ?>
            <?php echo "<a href=".base_url("member").">tidak lengkap</a>"; ?>
            <?php }else{ ?>
            <?php echo $invoice['alamat_member']; ?>
            <?php } ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Kepada Yth. :</b>
          <address>
            <strong><?php echo $invoice['nama_penerima']; ?></strong>
            <br>Alamat :
            <br><?php echo $invoice['alamat_penerima']; ?>
            <br><?php echo $invoice['nama_kecamatan'].", ".$invoice['kota_tujuan'].", ".$invoice['tipe_kota'].", ".$invoice['kode_pos']; ?>
            <br>Telp: <?php echo $invoice['telepon_penerima']; ?>
            <br>Kurir: <?php echo $invoice['ekspedisi']; ?>,
            <?php echo $invoice['paket_ekspedisi']; ?>,
            Estimasi: <?php echo $invoice['estimasi_hari']; ?> hari
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div>
        <h4>Transaksi No :</b> P.<?php echo $invoice['id_transaksi']; ?></h4>
        Status Bayar: 
        <?php 
        if ($invoice['status_pembelian']==0) { ?>
          <span class="label label-danger">Belum Lunas</span>
        <?php }else{ ?>
          <span class="label label-success">Lunas</span>
        <?php } ?>
        Status Kirim: 
        <?php 
        if ($invoice['status_pengiriman']==0) { ?>
          <span class="label label-danger">Belum Dikirim</span>
        <?php }else{ ?>
          <span class="label label-success">Dikirim</span>, tanggal : <?php echo $invoice['tgl_pengiriman']; ?>
        <?php } ?>
      </div>
      <br>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Nama Produk</th>
                <th>Harga Produk</th>
                <th>Jml.Beli</th>
                <th>Sub Harga</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($detail as $key => $det): ?>
                <tr>
                  <td><?php echo $det['nama_produk']; ?></td>
                  <td><?php echo rupiah($det['harga_produk']); ?></td>
                  <td><?php echo $det['jumlah_beli']; ?></td>
                  <td><?php echo rupiah($det['jumlah_beli']*$det['harga_produk']); ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-6">
          <!-- <p class="lead">Metode Pembayaran:</p> -->
          <table class="table table-bordered">
            <tbody>
              <?php foreach ($bank as $key => $b): ?>
                <tr>
                  <td style="width: 100px;"><img src="<?php echo base_url("assets/images/bank/$b[logo_bank]"); ?>" alt="<?php echo $b['nama_bank']; ?>" width="65"></td>
                  <td style=""><?php echo $b['no_rekening']; ?></td>
                  <td style="width: 30%;"><?php echo " a/n ".$b['nama_pemilik']; ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table table-bordered">
              <tbody style="text-align: right;">
                <tr>
                  <td style="width:50%">Total Harga : </td>
                  <td width="35%"><?php echo rupiah($invoice['total_harga']); ?></td>
                </tr>
                <tr>
                  <td>Ongkos Kirim : </td>
                  <td><?php echo rupiah($invoice['ongkos_kirim']); ?></td>
                </tr>
                <tr>
                  <td>Total Bayar : </td>
                  <td style="font-size: 20px;"><?php echo rupiah($invoice['total_pembelian']); ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row no-print">
        <div class="col-xs-12">
          <button class="btn btn-default pull-right" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
        </div>
      </div>
      <!-- </section> -->

    </div>
  </div>
</div>
<br>
<br>
<br>
<br>
<br>