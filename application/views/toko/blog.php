<section>
	<div class="container" style="min-height: 560px;">
		<div class="row">
			<br/>
			<div class="col-md-9 col-md-push-3">
				<!-- mulai konten-->
				<div class="panel">
					<div class="detail-konten" style="border-bottom: 1px dashed #999; padding-bottom: 5px;">
						<h3 class="panel-title" style="font-size: 20px;">Artikel Terbaru</h3>
					</div>
					<?php foreach ($blog as $key => $b): ?>
						<div class="previewblog">
							<div class="media">
								<div class="media-left media-middle" style="padding-bottom: 10px;">
									<a href="<?php echo base_url("blog/$b[url_artikel]"); ?>">
										<img class="media-object" src="<?php echo base_url("assets/images/artikel/$b[gambar_artikel]"); ?>" alt="<?php echo $b['judul_artikel']; ?>" title="<?php echo $b['judul_artikel']; ?>" width="100">
									</a>
								</div>
								<div class="media-body">
									<h4 class="media-heading"><a href="<?php echo base_url("blog/$b[url_artikel]"); ?>" style="color:#202020;"><?php echo substr($b['judul_artikel'], 0,150); ?></a></h4>
									<p><?php echo substr(strip_tags($b['isi_artikel']), 0,180); ?></p>
								</div>
							</div>
						</div>
					<?php endforeach ?>
					<div class="text-center" style="margin-bottom: 5%; margin-top: 15px;">
						<ul class="pagination">
							<?php echo $mpaging; ?>
						</ul>
					</div>
				</div>
				<!-- end konten-->
			</div>
			<div class="col-md-3 col-md-pull-9">
				<div class="panel panel-default">
					<div class="panel-heading panel-kategori">
						<h3 class="panel-title"><i class="fa fa-navicon"></i> KATEGORI</h3>
					</div>
					<ul class="list-group kategori">
						<?php foreach ($kategori as $key => $k): ?>
							<a data-toggle="collapse" data-target="#<?php echo $key; ?>" class="list-group-item"><?php echo $k['nama_kategori']; ?> <i class="fa fa-angle-down pull-right"></i></a>
							<ul class="list-group collapse" id="<?php echo $key ?>">
								<?php foreach ($subkategori as $key => $sk): ?>
									<?php if ($sk['id_kategori']==$k['id_kategori']): ?>
										<li style="list-style: none;"><a href="<?php echo base_url("kategori/$k[url_kategori]/$sk[url_subkategori]"); ?>" class="list-group-item" style="padding-left: 20px;"><i class="fa fa-angle-right" style="padding-right: 10px;"></i><?php echo $sk['nama_subkategori']; ?></a></li>
									<?php endif ?>
								<?php endforeach ?>
							</ul>
						<?php endforeach ?>
					</ul>
				</div>
				<br/>
				<br/>
			</div>