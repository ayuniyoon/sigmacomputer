<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Toko Online Sigma Computer</title>
	<link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/font-awesome/css/font-awesome.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/owlcarousel/css/owl.carousel.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/owlcarousel/css/owl.theme.default.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/dist/css/main.css"); ?>">
</head>
<body>

	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<a href="" class="text-center">
						<img src="<?php echo base_url("assets/images/logo/sigma-putih.png"); ?>" width="250">
					</a>
				</div>
				<div class="col-md-6">
					<div class="pencarian">
						<form class="form-search">
							<div class="input-group">
								<select class="form-control">
									<option value="">Pilih Kategori</option>
									<option value="">Laptop</option>
									<option value="">Gadget</option>
									<option value="">Personal Computer</option>
									<option value="">Software</option>
									<option value="">Komponen</option>
								</select>
								<input type="text" class="form-control" placeholder="Cari Produk...">
								<span class="input-group-btn">
									<button class="btn btn-default"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-3">
					<div class="btn-group pull-right">
						<a href="" class="btn btn-default"><i class="fa fa-shopping-bag"></i></a>
						<a href="" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Keranjang <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="">Laptop Gaming Tahan Banting</a></li>
							<li><a href="">HP Vivo</a></li>
							<li class="divider"></li>
							<li><a href="">Checkout</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#naff" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="naff">
				<ul class="nav navbar-nav">
					<li><a href="">Home</a></li>
					<li><a href="">Keranjang</a></li>
					<li><a href="">Checkout</a></li>
					<li><a href="">Login</a></li>
					<li><a href="">Daftar</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-md-push-3">
					<!-- mulai slider -->
					<div class="owl-carousel" id="homeslider">
						<div>
							<img src="<?php echo base_url("assets/images/slider/slider-1.jpg"); ?>" width="100%" height="400">
						</div>
						<div>
							<img src="<?php echo base_url("assets/images/slider/slider-2.jpg"); ?>" width="100%" height="400">
						</div>
					</div>
					<!-- end slider -->
					<br />
					
				</div>
				<div class="col-md-3 col-md-pull-9">
					<div class="panel panel-default">
						<div class="panel-heading panel-kategori">
							<h3 class="panel-title">KATEGORI</h3>
						</div>
						<div class="list-group kategori">
							<a href="" class="list-group-item">Laptop</a>
							<a href="" class="list-group-item">Gadget</a>
							<a href="" class="list-group-item">Personal Komputer</a>
							<a href="" class="list-group-item">Komponen Komputer</a>
							<a href="" class="list-group-item">Komponen Laptop</a>
							<a href="" class="list-group-item">Komponen Jaringan</a>
							<a href="" class="list-group-item">Aksesoris Komputer</a>
							<a href="" class="list-group-item">Aksesoris Gadget</a>
							<a href="" class="list-group-item">Software</a>
						</div>
					</div>
				</div>
				<!-- Untuk Produk col-md-12 -->
				<div class="col-md-12">
					<div class="panel panel-newproduk">
						<div class="panel-heading">
							<h3 class="panel-title">LAPTOP</h3>
							<div id="navsemualaptop" class="pull-right"></div>
						</div>
						<div class="panel-body">
							<div class="owl-carousel owl-theme" id="semualaptop">
								<div class="text-center produk">
									<img src="assets/image/produk/p1.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 1 Ada Produknya</a></h3>
									<div class="ket-produk">
										<b>Rp 12,000,000,00</b>
										<div class="btn btn-group">
											<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
											<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
										</div>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p2.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 2 New Product 2 New Product 2</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p3.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 3</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p4.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 4</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p5.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 5</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p6.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 6</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p7.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 7</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
							</div>
							<div class="button-lihat text-center">
								<a href="" class="btn btn-default">Lihat Semua Laptop</a>
							</div>
						</div>
					</div>
				</div>
				<!-- end produk col-md-12 -->

				<!-- mulai gadget -->
				<div class="col-md-12">
					<div class="panel panel-newproduk">
						<div class="panel-heading">
							<h3 class="panel-title">Gadget</h3>
							<div id="navsemuagadget" class="pull-right"></div>
						</div>
						<div class="panel-body">
							<div class="owl-carousel owl-theme" id="semuagadget">
								<div class="text-center produk">
									<img src="assets/image/produk/p1.jpg" alt="">
									<h3 class="produk-title"><a href="">Travel Charger SUMO SC-H88 Smart Cha</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p2.jpg" alt="">
									<h3 class="produk-title"><a href="">Advance Mini Karaoke Player MS-52</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p3.jpg" alt="">
									<h3 class="produk-title"><a href="">Laptop Asus X441NA Ram 2GB</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p4.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 4</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p5.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 5</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p6.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 6</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
								<div class="text-center produk">
									<img src="assets/image/produk/p7.jpg" alt="">
									<h3 class="produk-title"><a href="">New Product 7</a></h3>
									<b>Rp 12,000,000,00</b>
									<div class="btn btn-group">
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-random"></i></a>
										<a href="" class="btn btn-default btn-sm"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
									</div>
								</div>
							</div>
							<div class="button-lihat text-center">
								<a href="" class="btn btn-default">Lihat Semua Gadget</a>
							</div>
						</div>
					</div>
				</div>
				<!-- end gadget -->

				<!-- mulai brand -->
				<div class="col-md-12">
					<div class="panel panel-brand">
						<div class="panel-heading">
							<h3 class="panel-title">Brands</h3>
						</div>
						<div class="panel-body">
							<div class="owl-carousel" id="brand">
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b1.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b2.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b3.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b4.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b6.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b7.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b8.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b9.jpg" alt=""></a>
								</div>
								<div class="text-center">
									<a href=""><img src="assets/image/brand/b4.jpg" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end brand -->
			</div>
		</div>
	</section>

	<footer>
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12">
						<h2>Contact Us</h2>
						<ul>
							<li><i class="fa fa-map-marker"></i> Jl. Prambanan, Sleman, Yogyakarta</li>
							<li><i class="fa fa-phone"></i> 0857-1234-5678</li>
							<li><i class="fa fa-envelope"></i> cs@sigmacomputer.com</li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<h2>Customer Service</h2>
						<ul>
							<li><a href="">My Account</a></li>
							<li><a href="">FAQ</a></li>
							<li><a href="">Help Center</a></li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<h2>Shop</h2>
						<ul>
							<li><a href="">Keranjang</a></li>
							<li><a href="">Daftar</a></li>
							<li><a href="">Login</a></li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<h2>About</h2>
						<ul>
							<li><a href="">Blog</a></li>
							<li><a href="">Cara Belanja</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				Copyright &copy; <strong>Sigma Computer - Trainit All Right Reserved</strong>
			</div>
		</div>
	</footer>


	<!-- panggil Jquery -->
	<script src="<?php echo base_url("assets/dist/js/jquery.min.js"); ?>"></script>
	<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
	<script src="<?php echo base_url("assets/owlcarousel/owl.carousel.min.js"); ?>"></script>
	<script src="<?php echo base_url("assets/dist/js/app.js"); ?>"></script>
</body>
</html>