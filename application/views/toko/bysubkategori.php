<section>
	<div class="container" style="min-height: 600px; margin-bottom: 50px;">
		<div class="row">
			<div class="panel-heading kategori-heading">
				<h3><?php echo $sub['nama_subkategori']; ?></h3>
			</div>
			<!-- mulai gadget -->
			<div class="col-md-12 col-sm-12 col-xs-12" id="watermark">
				<?php if (!$subkategori){ ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					Maaf, produk untuk kategori ini masih kosong.
				</div>
				<?php }else{ ?>
				<?php foreach ($subkategori as $key => $sub): ?>
					<div class="produkbykategori">		
						<div class="col-sm-3 col-md-2">
							<div class="thumbnail produk">
								<a href="<?php echo base_url("produk/$sub[url_produk]"); ?>"><img src="<?php echo base_url("assets/images/produk/$sub[foto_produk]"); ?>" alt="<?php echo "$sub[nama_produk]"; ?>" title="<?php echo "$sub[nama_produk]"; ?>" style="padding: 3px;"></a>
								<div class="caption text-center">
									<?php if ($sub['stock_produk']==0){ ?>
									<button class="btn btn-danger">Habis</button>
									<?php } ?>
									<p><a href="<?php echo base_url("produk/$sub[url_produk]"); ?>" style="color: #000;"><?php echo $sub['nama_produk']; ?></a></p>
									<h4><?php echo rupiah($sub['harga_produk']); ?></h4>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				<?php } ?>
			</div>
			<div class="text-center" style="margin-bottom: 5%; margin-top: 15px;">
				<ul class="pagination">
					<?php echo $mpaging; ?>
				</ul>
			</div>
		</div>
	</div>
