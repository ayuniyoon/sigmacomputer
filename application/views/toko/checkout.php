<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title"><b>RINCIAN PEMBELIAN ANDA</b></h3>
				</div>
				<div class="panel-body">
					<table class="table table-stripped table-hover table-bordered">
						<thead>
							<tr>
								<th width="5px">No</th>
								<th>Produk Image</th>
								<th width="35%">Nama Produk</th>
								<th>Harga Satuan</th>
								<th width="8%">Qty (pcs)</th>
								<th width="8%">Berat (gr)</th>
								<th width="15%">Sub Total</th>
							</tr>
						</thead>
						<tbody>
							<?php $total=0; ?>
							<?php $pcs=0; ?>
							<?php $total_berat=0; ?>
							<?php if ($keranjang==null){ ?>	
								<tr>
									<td colspan="8" align="center" style="padding-top: 30px; padding-bottom: 30px;">Keranjang Anda masih kosong, Silahkan belanja terlebih dahulu</td>
								</tr>
							<?php }else{ ?>	
							<?php foreach ($keranjang as $key => $value): ?>
								<tr>
									<td><?php echo $key+1; ?></td>
									<td>
										<img src="<?php echo base_url("assets/images/produk/$value[foto_produk]"); ?>" width="90">
									</td>
									<td><?php echo $value['nama_produk']; ?></td>
									<td><?php echo rupiah($value['harga_produk']); ?></td>
									<td align="center"><?php echo $value['jumlah_beli']; ?></td>
									<td align="center"><?php echo $value['berat_produk']*$value['jumlah_beli']; ?></td>
									<td><?php echo rupiah($value['subharga']); ?></td>
								</tr>
								<?php $total+=$value['subharga']; ?>
								<?php $total_berat+=$value['berat_produk']*$value['jumlah_beli']; ?>
								<?php $pcs+=$value['jumlah_beli']; ?>
							<?php endforeach ?>								
							<?php } ?>
							<tr style="text-align: right;font-weight: 600; background-color: #e8e8e8;">
								<td colspan="5">TOTAL BELANJA</td>
								<td><?php echo $pcs; ?> pcs</td>
								<td><?php echo rupiah($total); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-heading" style=""">
					<h3 class="panel-title" style="margin-bottom: 25px;border-bottom: 1px dashed #999; padding-bottom: 10px; font-weight: 600;">ALAMAT TUJUAN :</h3>
				</div>
				<form method="post">
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>Nama Penerima</label>
						<input type="text" class="form-control col-md-3 col-sm-3 col-xs-12" name="nama_penerima" required="required"/>
					</div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>No. Telepon</label>
						<input type="number" class="form-control col-md-3 col-sm-3 col-xs-12" name="telepon_penerima" required="required" maxlength="12" />
					</div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>Provinsi</label>
						<select class="form-control col-md-3 col-sm-3 col-xs-12" name="provinsi">
							<option value="">- Pilih Provinsi -</option>
						</select>
					</div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>Kota</label>
						<select class="form-control col-md-3 col-sm-3 col-xs-12" name="kota">
							<option value="">- Pilih Kota -</option>
						</select>
					</div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>Kecamatan</label>
						<input type="text" class="form-control col-md-3 col-sm-3 col-xs-12" name="nama_kecamatan" required="required"/>
					</div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>Kode Pos</label>
						<input type="text" class="form-control col-md-3 col-sm-3 col-xs-12" name="kode_pos" required maxlength="5" />
					</div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>Ekspedisi</label>
						<select class="form-control col-md-3 col-sm-3 col-xs-12" name="kurir">
							<option value="">- Pilih Ekspedisi -</option>
							<option value="jne">JNE</option>
							<option value="tiki">TIKI</option>
							<option value="pos">POS</option>
						</select>
					</div>
					<div class="form-group col-md-3 col-sm-3 col-xs-12">
						<label>Paket Ekspedisi</label>
						<select class="form-control col-md-3 col-sm-3 col-xs-12" name="paket_kurir" required="">
							<option value="">- Pilih Paket Ekspedisi -</option>
						</select>
					</div>
					<div class="form-group col-md-12 col-sm-12 col-xs-12">
						<label for="message">Alamat Lengkap</label><small> cantumkan nama jalan, rt/rw, kelurahan, dusun</small>
						<textarea name="alamat_penerima" required="required" class="form-control col-md-6 col-sm-6 col-xs-12" minlength="10"></textarea>
					</div>
					<input type="hidden" name="kota_tujuan">
					<input type="hidden" name="tipe_kota">
					<input type="hidden" id="kota">
					<input type="hidden" name="total_jmlbeli" value="<?php echo $pcs; ?>">
					<input type="hidden" name="total_harga" value="<?php echo $total; ?>">
					<input type="hidden" name="total_berat" value="<?php echo $total_berat; ?>">
					<input name="paket_ekspedisi" value="" type="hidden">
					<input name="ongkos_kirim"  value="" type="hidden">
					<input name="estimasi_hari" value="" type="hidden">
				<div class="pull-right" >
					<br/>
					<br/>
					<button class="btn btn-success btn-default btn-lg">Lanjut Pembayaran</button>
				</div>			
				</form>
			</div>
		</div>
	</div>
	<br />				
	<br />	
	<br />	
	<br />	
	<br />	
</div>