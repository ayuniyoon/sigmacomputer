		</div>
	</div>
</section>

<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h2>Sigma Computer</h2>
					<ul>
						<li><?php echo $tagline['isi_pengaturan']; ?></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h2>Contact Us</h2>
					<ul>
						<li><i class="fa fa-map-marker"></i> <?php echo $alamat['isi_pengaturan']; ?></li>
						<li><i class="fa fa-phone"></i> <?php echo $telepon['isi_pengaturan']; ?></li>
						<li><i class="fa fa-whatsapp"></i> <?php echo $wa['isi_pengaturan']; ?></li>
						<li><i class="fa fa-envelope"></i> <?php echo $email['isi_pengaturan']; ?></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h2>SOCIAL MEDIA</h2>
					<ul>
						<li><i class="fa fa-facebook-square"></i> <?php echo $facebook['isi_pengaturan']; ?></li>
						<li><i class="fa fa-instagram"></i> <?php echo $instagram['isi_pengaturan']; ?></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h2>INFORMATION</h2>
					<ul>
						<li><a href="<?php echo base_url("blog"); ?>">Blog</a></li>
						<li><a href="<?php echo base_url("profil-perusahaan") ?>">Profil Perusahaan</a></li>
						<li><a href="<?php echo base_url("cara-belanja"); ?>">Cara Belanja</a></li>
						<li><a href="<?php echo base_url("metode-pembayaran"); ?>">Metode Pembayaran</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			Copyright &copy; <strong>Sigma Computer - Ayuni Yoon All Right Reserved</strong>
		</div>
	</div>
</footer>


<!-- panggil Jquery -->
<script src="<?php echo base_url("assets/dist/js/jquery.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/owlcarousel/owl.carousel.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/dist/js/app.js"); ?>"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
    $('#datatable').DataTable();
} );
</script>

<script>
	$(document).ready(function(){
		var hasil = $("input[name=jumlah_beli]").val();
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			url:'<?php echo base_url("Api/provinsi"); ?>',
			success:function(data)
			{
				$("select[name=provinsi]").html(data);
			}
		});
	});
</script>
<script type="text/javascript">
	// kota
	$("select[name=provinsi]").on('change',function(){
			var idprovinsi = $(this).val();
			$.ajax({
				url:'<?php echo base_url("Api/kota"); ?>',
				method:'POST',
				data:'idprovinsi='+idprovinsi,
				success:function(hasil)
				{
					$("select[name=kota]").html(hasil);
				}
			});
		});
	$("select[name=kota]").on('change',function(){
		var namakota = $(this).val();
		var nama_kota = $("option:selected",this).attr('kota');
		var type = $("option:selected",this).attr('tipe-kota');
		console.log(nama_kota);
		$('input[name=kota_tujuan]').val(nama_kota);
		$('input[name=tipe_kota]').val(type);
		$('#kota').val(namakota);
	});
</script>
<script type="text/javascript">
	// kurir
	$("select[name=kurir]").on('change',function(){
		var kurir = $(this).val();
		var kota_tujuan = $("#kota").val();
		var berat = $("input[name=total_berat]").val();

		$.ajax({
			url:'<?php echo base_url("Api/paketkurir"); ?>',
			method:'POST',
			data:'kurir='+kurir+'&berat='+berat+'&kota_tujuan='+kota_tujuan,
			success:function(hasil)
			{
				$("select[name=paket_kurir]").html(hasil);
			}
		})
	})
	$("select[name=paket_kurir]").on('change',function(){
		var paket = $(this).val();
		var harga = $("option:selected",this).attr('harga');
		var hari = $("option:selected",this).attr('hari');

		$("input[name=paket_ekspedisi]").val(paket);
		$("input[name=ongkos_kirim]").val(harga);
		$("input[name=estimasi_hari]").val(hari);
	});
</script>
<script>
	// cek invoice
	function cekinvoice()
	{
		var invoice = $("#invoice").val();
		var pesan = document.getElementById('pesan');
		// membuat ajax
		$.ajax({
			url:'<?php echo base_url("home/cekinvoice"); ?>',
			method:'POST',
			data:'id_transaksi='+invoice,
			success:function(hasil)
			{
				if(hasil=="gagal")
				{
					pesan.style.color='red';
					pesan.innerHTML="Tidak ada nomer Invoice tersebut";
				}
				else
				{
					pesan.style.color='green';
					pesan.innerHTML="Nomer Invoice Sesuai";
				}
			}
		});
	}
</script>
<script>
	// cek email member login
	function cekemail()
	{
		var member = $("#email").val();
		var pesan = document.getElementById('pesan');
		// membuat ajax
		$.ajax({
			url:'<?php echo base_url("home/cekemail"); ?>',
			method:'POST',
			data:'email_member='+member,
			success:function(hasil)
			{
				if(hasil=="gagal")
				{
					pesan.style.color='red';
					pesan.innerHTML="email tidak dikenal";
				}
				else
				{
					pesan.style.color='green';
					pesan.innerHTML="email terdaftar";
				}
			}
		});
	}
</script>

</body>
</html>