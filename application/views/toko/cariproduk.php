<section>
	<div class="container" style="min-height: 580px;">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="produkbykategori">
					<?php if (empty($produk)): ?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
						Maaf, pencarian untuk produk ini masih kosong.</div>
					<?php endif ?>
					<?php foreach ($produk as $produkey => $produk): ?>
						<div class="col-sm-3 col-md-2">
							<div class="thumbnail produk" id="watermark">
								<a href="<?php echo base_url("produk/$produk[url_produk]"); ?>">
									<img src="<?php echo base_url("assets/images/produk/$produk[foto_produk]"); ?>" alt="<?php echo $produk['nama_produk']; ?>" title="<?php echo $produk['nama_produk']; ?>" style="padding: 3px;"></a>
									<div class="caption text-center">
										<?php if ($produk['stock_produk']==0){ ?>
										<button class="btn btn-danger">Habis</button>
										<?php } ?>
										<p><a href="<?php echo base_url("produk/$produk[url_produk]"); ?>" style="color: #000;"><?php echo $produk['nama_produk']; ?></a></p>
										<h4><?php echo rupiah($produk['harga_produk']); ?></h4>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>