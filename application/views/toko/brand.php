<div class="col-md-12">
	<div class="panel panel-brand">
		<div class="panel-heading">
			<h3 class="panel-title">Brand Terbaik</h3>
		</div>
		<div class="panel-body">
			<div class="owl-carousel" id="brand">
				<?php foreach ($brand as $key => $b): ?>
				<div class="text-center" style="height: 80%;">
					<a href=""><img src="
						<?php if(isset($b['gambar_brand'])){echo base_url("assets/images/brand/$b[gambar_brand]");} ?>" alt="<?php echo $b['nama_brand']; ?>">
					</a>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</div>