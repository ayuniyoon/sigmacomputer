<section>
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-push-3">
				<!-- mulai slider -->
				<div class="owl-carousel slider" id="homeslider">
					<?php foreach ($slider as $key => $s): ?>
						<div>
							<img src="<?php echo base_url("assets/images/slider/$s[gambar]"); ?>" alt="<?php echo $s['alt_gambar']; ?>">
						</div>
					<?php endforeach ?>
				</div>
				<!-- end slider -->
				<br />
			</div>
			<div class="col-md-3 col-md-pull-9">
				<div class="panel panel-default">
					<div class="panel-heading panel-kategori">
						<h3 class="panel-title"><i class="fa fa-navicon"></i> KATEGORI</h3>
					</div>
					<ul class="list-group kategori">
						<?php foreach ($kategori as $key => $k): ?>
							<a data-toggle="collapse" data-target="#<?php echo $key; ?>" class="list-group-item"><?php echo $k['nama_kategori']; ?> <i class="fa fa-angle-down pull-right"></i></a>
							<ul class="list-group collapse" id="<?php echo $key ?>">
								<?php foreach ($subkategori as $key => $sk): ?>
									<?php if ($sk['id_kategori']==$k['id_kategori']): ?>
										<li style="list-style: none;"><a href="<?php echo base_url("kategori/$k[url_kategori]/$sk[url_subkategori]"); ?>" class="list-group-item" style="padding-left: 20px;"><i class="fa fa-angle-right" style="padding-right: 10px;"></i><?php echo $sk['nama_subkategori']; ?></a></li>
									<?php endif ?>
								<?php endforeach ?>
							</ul>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<!-- Untuk Produk col-md-12 -->
			<div class="col-md-12">
				<div class="panel panel-newproduk">
					<div class="panel-heading">
						<h3 class="panel-title">PRODUK TERBARU</h3>
						<div id="produkterbaru" class="pull-right"></div>
					</div>
					<div class="panel-body" id="watermark">
						<div class="owl-carousel owl-theme" id="terbaru">
							<?php foreach ($produk_terbaru as $key => $t): ?>	
								<div class="text-center produk" style="padding: 5px;">
									<a href="<?php echo base_url("produk/$t[url_produk]"); ?>">
										<img src="<?php echo base_url("assets/images/produk/$t[foto_produk]"); ?>" alt="<?php echo $t['nama_produk']; ?>" title="<?php echo $t['nama_produk']; ?>">
									</a>
									<?php if ($t['stock_produk']==0){ ?>
									<button class="btn btn-danger">Habis</button>
									<?php } ?>
									<p><a href="<?php echo base_url("produk/$t[url_produk]"); ?>" style="color: #000;"><?php echo substr($t['nama_produk'], 0,60); ?></a></p>
									<h4><?php echo rupiah("$t[harga_produk]"); ?></h4>
								</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</div>
			<!-- end produk col-md-12 -->

			<!-- mulai gadget -->
			<div class="col-md-12">
				<div class="panel panel-newproduk">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo $gadget['nama_kategori']; ?></h3>
						<div id="bykategori" class="pull-right"></div>
					</div>
					<div class="panel-body">
						<div class="owl-carousel owl-theme" id="produkbykategori">
							<?php foreach ($produk_kategori as $key => $pk): ?>
								<div class="text-center produk" id="watermark">
									<a href="<?php echo base_url("produk/$pk[url_produk]"); ?>">
										<img src="<?php echo base_url("assets/images/produk/$pk[foto_produk]"); ?>" alt="<?php echo $pk['nama_produk']; ?>" title="<?php echo $pk['nama_produk']; ?>" style="padding: 5px;">
									</a>
									<?php if ($pk['stock_produk']==0){ ?>
									<button class="btn btn-danger">Habis</button>
									<?php } ?>
									<p><a href="<?php echo base_url("produk/$pk[url_produk]"); ?>" style="color: #000;"><?php echo $pk['nama_produk']; ?></a></p>
									<h4><?php echo rupiah($pk['harga_produk']); ?></h4>
								</div>
							<?php endforeach ?>
						</div>
						<div class="button-lihat text-center">
							<a href="<?php echo base_url("kategori/$gadget[url_kategori]") ?>" class="btn btn-default">Lihat Kategori <?php echo $gadget['nama_kategori']; ?></a>
						</div>
					</div>
				</div>
			</div>
			<!-- end gadget -->