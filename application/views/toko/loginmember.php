<div class="container" style="min-height: 500px;">
	<div class="row">
		<div class="panel panel-member">
			<div class="panel-body" style="margin-top: -30px;margin-bottom: 90px;">
				<div class="col-md-12">
					<br/>
					<div class="col-md-3">
						<h3>BELUM MENJADI MEMBER?</h3>
						<p>Silahkan mendaftar pada halaman <a href="<?php echo base_url("register"); ?>">Register</a>. Anda harus terdaftar menjadi anggota agar dapat melakukan pembelian secara online.</p>
					</div>
					<div class="col-md-9" style="border-left: 1px solid #e7e7e7;">
						<h3>LOGIN MEMBER</h3>
						<p>Bila anda sudah terdaftar menjadi anggota, silakan Login pada form dibawah ini.</p>
						<?php if (validation_errors()): ?>
							<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
						<?php endif ?>
						<div class="col-md-8 center-margin">
							<form class="form-horizontal form-label-left" method="post">
								<div class="form-group">
									<label>Email address</label>
									<input type="email" name="email_member" class="form-control" id="email" onkeyup="cekemail();return false;" placeholder="Enter email" required autofocus>
									<span id="pesan"></span>
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="password" class="form-control" placeholder="Password" required>
								</div>
								<button type="submit" class="btn btn-success" style="margin-left:-15px;">Login</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>