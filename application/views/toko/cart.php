<div class="container" style="min-height: 500px;">
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">PERINCIAN BELANJA ANDA</h3>
				</div>
				<div class="panel-body">
					<form method="post">
						<table class="table table-stripped table-hover table-bordered">
							<thead>
								<tr>
									<th width="5px">No</th>
									<th>Produk Image</th>
									<th width="35%">Nama Produk</th>
									<th>Harga Satuan</th>
									<th width="8%">Qty (pcs)</th>
									<th width="8%">Berat (kg)</th>
									<th width="15%">Total</th>
									<th width="8%;">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $total=0; ?>
								<?php $pcs=0; ?>
								<?php if ($keranjang==null){ ?>
								<tr>
									<td colspan="8" align="center" style="padding-top: 30px; padding-bottom: 30px;">Keranjang Anda masih kosong, Silahkan belanja terlebih dahulu</td>
								</tr>
								<?php } else { ?>
								<?php foreach ($keranjang as $key => $value): ?>
									<tr>
										<td><?php echo $key+1; ?></td>
										<td>
											<img src="<?php echo base_url("assets/images/produk/$value[foto_produk]"); ?>" width="90">
										</td>
										<td><?php echo $value['nama_produk']; ?></td>
										<td><?php echo rupiah($value['harga_produk']); ?></td>
										<td>
											<input type="number" name="jumlah_beli[<?php echo $value['id_produk'] ?>]" value="<?php echo $value['jumlah_beli']; ?>" max="<?php echo $value['stock_produk']; ?>" min="1" class="form-control">
										</td>
										<td><?php echo $value['berat_produk']*$value['jumlah_beli']; ?></td>
										<td><?php echo rupiah($value['subharga']); ?></td>
										<td>
											<button class="btn btn-default btn-sm" title="refresh"><i class="fa fa-refresh"></i></button>
											<a href="<?php echo base_url("delete/$value[id_produk]"); ?>" class="btn btn-default btn-sm" title="delete"><i class="fa fa-close"></i></a>
										</td>
									</tr>
									<?php $total+=$value['subharga']; ?>
									<?php $pcs+=$value['jumlah_beli']; ?>
								<?php endforeach ?>
								<?php } ?>
								<tr style="text-align: right;font-weight: 600; background-color: #e8e8e8;">
									<td colspan="5">TOTAL BELANJA</td>
									<td><?php echo $pcs; ?> pcs</td>
									<td><?php echo rupiah($total); ?></td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<!-- <td colspan="8">
										<button class="btn btn-warning pull-right">Update</button>
									</td> -->
								</tr>
							</tbody>
						</table>
					</form>
					<div class="pull-right">
						<!-- <h3>TOTAL RP. 5,000,000</h3><br/> -->
						<div class="pull-right" style="padding-top: 20px;">
							<a href="<?php echo base_url(); ?>" class="btn btn-danger btn-default btn-lg">Belanja Lagi</a>
							<a href="<?php echo base_url("home/checkout"); ?>" class="btn btn-success btn-default btn-lg">Checkout</a>
						</div>
					</div>			
				</div>
			</div>
		</div>
	</div>
	<br />				
	<br />	
	<br />	
</div>