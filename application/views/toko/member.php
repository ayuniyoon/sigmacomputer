<section>
	<div class="container" style="min-height: 600px;">
		<div class="row">
			<div class="col-md-9 col-md-push-3">
				<div class="" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
						<li class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Profil Saya</a>
						</li>
						<li><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Riwayat Belanja</a>
						</li>
						<?php if (isset($keranjang)): ?>
							<li><a href="<?php echo base_url("shoppingcart"); ?>" role="tab">Keranjang Anda</a>
							<?php endif ?>
						</li>
						<?php if (isset($invoice)): ?>
							<li><a href="<?php echo base_url("invoice"); ?>" role="tab">Invoice Anda</a>
							<?php endif ?>
						</li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
							<br>
							<br>
							<form method="post" class="form" style="margin: 0px;">
								<input type="hidden" name="id_member" value="<?php echo $member['id_member']; ?>">
								<div class="form-group col-md-6 col-sm-6 col-xs-12">
									<label>Nama Lengkap</label>
									<input type="text" class="form-control col-md-6 col-sm-6 col-xs-12" name="nama_member" value="<?php echo $member['nama_member']; ?>" required/>
								</div>
								<div class="form-group col-md-6 col-sm-6 col-xs-12">
									<label>Email</label>
									<input type="email" class="form-control col-md-6 col-sm-6 col-xs-12" name="email_member" value="<?php echo $member['email_member']; ?>" required/>
								</div>
								<div class="form-group col-md-6 col-sm-6 col-xs-12">
									<label>Password</label>
									<input type="password" class="form-control col-md-6 col-sm-6 col-xs-12" name="password" value="<?php echo $member['password']; ?>"/>
								</div>
								<div class="form-group col-md-6 col-sm-6 col-xs-12">
									<label>Nomor Telepon</label>
									<input type="number" required class="form-control col-md-6 col-sm-6 col-xs-12" name="no_hp" value="<?php echo $member['no_hp']; ?>" >
								</div>
								<div class="form-group col-md-12 col-sm-12 col-xs-12">
									<label>Alamat Lengkap</label>
									<textarea rows="5" class="form-control" name="alamat_member"><?php echo $member['alamat_member']; ?></textarea>
								</div>
								<div class="pull-right" style="margin-right: 15px;">
									<button class="btn btn-success">Update Profil</button>
								</div>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
							<br>
							<table id="datatable" class="table table-striped table-bordered table-hover" width="100%">
								<thead>
									<tr>
										<td width="5px">Transaksi</td>
										<td>Nama Penerima</td>
										<td>Total Bayar</td>
										<td>Status Bayar</td>
										<td>Status Kirim</td>
										<td width="15px">Tgl.Pembelian</td>
									</tr>
								</thead>
								<tbody>
									<?php if ($riwayat==null){ ?> 
									<tr>
										<td colspan="6"><center>Ups, anda belum pernah melakukan transaksi</center></td>
									</tr>
									<?php }else{ ?>
									<?php foreach ($riwayat as $key => $value): ?>
										<tr>
											<td><?php echo "P".$value['id_transaksi']; ?></td>
											<td><?php echo $value['nama_penerima']; ?></td>
											<td><?php echo rupiah($value['total_pembelian']); ?></td>
											<td>
												<?php if ($value['status_pembelian']==0) { ?>
													<span class="label label-danger">Belum Lunas</span>
												<?php }else{ ?>
													<span class="label label-success">Lunas</span>
												<?php } ?>
											</td>
											<td>
												<?php if ($value['status_pengiriman']==0) { ?>
													<span class="label label-danger">Belum Dikirim</span>
												<?php }else{ ?>
													<span class="label label-success">Dikirim</span>
												<?php } ?>	
											</td>
											<td><?php echo date("d M Y", strtotime($value['tgl_transaksi'])); ?></td>
										</tr>
									<?php endforeach ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-md-pull-9">
				<div class="panel panel-default">
					<div class="panel-heading panel-kategori">
						<h3 class="panel-title">KATEGORI</h3>
					</div>
					<ul class="list-group kategori">
						<?php foreach ($kategori as $key => $k): ?>
							<a data-toggle="collapse" data-target="#<?php echo $key; ?>" class="list-group-item"><?php echo $k['nama_kategori']; ?> <i class="fa fa-angle-down pull-right"></i></a>
							<ul class="list-group collapse" id="<?php echo $key ?>">
								<?php foreach ($subkategori as $key => $sk): ?>
									<?php if ($sk['id_kategori']==$k['id_kategori']): ?>
										<li style="list-style: none;"><a href="<?php echo base_url("kategori/$k[url_kategori]/$sk[url_subkategori]"); ?>" class="list-group-item" style="padding-left: 20px;"><i class="fa fa-angle-right" style="padding-right: 10px;"></i><?php echo $sk['nama_subkategori']; ?></a></li>
									<?php endif ?>
								<?php endforeach ?>
							</ul>
						<?php endforeach ?>
					</ul>
				</div>
			</div>