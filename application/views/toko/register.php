<div class="container" style="min-height: 500px;">
	<div class="row">
		<div class="panel panel-member">
			<div class="panel-body" style="margin-top: -30px;margin-bottom: 90px;">
				<div class="col-md-12">
					<br>
					<div class="col-md-3">
						<h3>SUDAH MENJADI MEMBER ?</h3>
						<p>Bila anda sudah memiliki akun, silakan Login melalui halaman <a href="<?php echo base_url("login"); ?>">login member</a>.</p>
					</div>
					<div class="col-md-9" style="border-left: 1px solid #e7e7e7;">
						<h3>PENDAFTARAN MEMBER</h3>
						<p>Silakan isi formulir dibawah ini untuk membuat akun Sigma Computer Web Store</p>
						<?php if (validation_errors()): ?>
							<div class="alert alert-info" role="alert"><?php echo validation_errors(); ?></div>
						<?php endif ?>
						<div class="col-md-8 center-margin">
							<form method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
								<div class="form-group">
									<label>Nomor Handphone </label>
									<input type="number" name="no_hp" class="form-control" placeholder="Masukkan Nomor Handphone Anda" maxlength="13" autofocus>
								</div>
								<div class="form-group">
									<label>Email address</label>
									<input type="email" name="email_member" class="form-control" placeholder="Masukkan Email" required autofocus>
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="password" class="form-control" placeholder="Password" required>
								</div>
								<div class="form-group">
									<label>Retype Password</label>
									<input type="password" name="passconf" class="form-control" placeholder="Retype Password" required>
								</div>
								<button class="btn btn-success" type="submit" style="margin-left:-15px;">Submit Pendaftaran</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>