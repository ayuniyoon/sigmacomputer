<section>
	<div class="container">
		<div class="row" style="padding-top: 5px; padding-bottom: 30px;">
			<div class="produkbykategori">
				<div class="col-md-4 col-sm-12">
					<div class="thumbnail" id="watermark">
						<img src="<?php echo base_url("assets/images/produk/$dp[foto_produk]"); ?>" alt="<?php echo $dp['nama_produk']; ?>" title="<?php echo $dp['nama_produk']; ?>" style="padding: 10px;">
						<?php if ($dp['stock_produk']==0){ ?>
						<button class="btn btn-danger">Habis</button>
						<?php } ?>
					</div>
				</div>
				<div class="col-md-8 col-sm-12">
					<div class="nama-produk">
						<h1><?php echo $dp['nama_produk']; ?></h1>
					</div>
					<div class="detailproduk">
						<table class="table">
							<thead></thead>
							<tbody>
								<tr>
									<td width="15%;">Produk No</td>
									<td width="3px;">:</td>
									<td><?php echo $dp['kode_produk']; ?></td>
								</tr>
								<tr>
									<td>Berat</td>
									<td>:</td>
									<td><?php echo $dp['berat_produk']; ?> gram</td>
								</tr>
								<tr>
									<td>Stok Tersisa</td>
									<td>:</td>
									<td><?php if ($dp['stock_produk']==0){echo "Sold Out";} else{ echo "<b>In stock</b>";} ?></td>
								</tr>
							</tbody>
						</table>
					</div> <!--detail -->
					<div class="produk-single">
						<small>Harga (Rp)</small>
						<h3><?php echo rupiah($dp['harga_produk']); ?></h3>
						<div class="produk-tombol">
							<a href="<?php echo base_url("perbandingan"); ?>" class="btn btn-default btn-lg" style="margin-top: 10px;"><i class="fa fa-exchange"></i></a>

							<?php if ($dp['stock_produk']==0){ ?>
							<button type="button" data-toggle="tooltip" data-placement="top" title="Stok habis" class="btn btn-success btn-lg" style="margin-top: 10px; background-color: #0d7e46;" disabled><i class="fa fa-shopping-cart"></i> Beli Sekarang</button>
							<?php }else{ ?>
							<a href="<?php echo base_url("cart/$dp[id_produk]"); ?>" class="btn btn-success btn-lg" style="margin-top: 10px; background-color: #0d7e46;"><i class="fa fa-shopping-cart"></i> Beli Sekarang</a>
							<?php } ?>	
						</div>
					</div>
				</div>
			</div>
			<!-- deskripsi -->
			<div class="col-md-12">
				<div class="deskripsi-spesifikasi">
					<h3 style="font-size:16px; padding-top: 10px; font-weight: 600;">Deskripsi <?php echo $dp['nama_produk']; ?></h3>
					<p><?php if ($dp['deskripsi']==null){echo "Tidak Ada";}else{echo $dp['deskripsi'];} ?>
						<h2 style="font-size:16px; padding-top: 5px; font-weight: 600;">Spesifikasi <?php echo $dp['nama_produk']; ?></h2>
						<p><?php if ($dp['spesifikasi']==null){echo "Tidak Ada";}else{echo $dp['spesifikasi'];} ?></p>
					</div>
					<div style="margin-bottom: 20px;margin-top: 20px;">
						<a href="javascript:history.back(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
					</div>
				</div>

				<!-- produk sejenis -->
				<div class="col-md-12 col-sm-12">
					<div class="panel panel-newproduk">
						<div class="panel-heading">
							<h3 class="panel-title">Produk Sejenis</h3>
							<div id="bykategori" class="pull-right"></div>
						</div>
						<div class="panel-body" id="watermark">
							<div class="owl-carousel owl-theme" id="detailterbaru">
								<?php foreach ($produk_terbaru as $key => $pt): ?>
									<div class="text-center produk">
										<a href="<?php echo base_url("produk/$pt[url_produk]"); ?>">
											<img src="<?php echo base_url("assets/images/produk/$pt[foto_produk]"); ?>" alt="<?php echo $pt['nama_produk']; ?>" title="<?php echo $pt['nama_produk']; ?>" style="padding: 5px;">
										</a>
										<?php if ($pt['stock_produk']==0){ ?>
										<button class="btn btn-danger">Habis</button>
										<?php } ?>
										<p><a href="<?php echo base_url("produk/$pt[url_produk]"); ?>" style="color: #000;"><?php echo $pt['nama_produk']; ?></a></p>
										<h4><?php echo rupiah($pt['harga_produk']); ?></h4>
									</div>
								<?php endforeach ?>

							</div>
						</div>
					</div>
				</div>
			</div> <!-- row-->
