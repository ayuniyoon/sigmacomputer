<section>
	<div class="container">
		<div class="row">
			<br/>
			<div class="col-md-9 col-md-push-3">
				<!-- mulai konten-->
				<div class="panel detail-konten">
					<h1><?php echo $dtblog['judul_artikel']; ?></h1>
					<i class="fa fa-calendar"></i>  Tanggal Posting : <?php echo date("d M Y", strtotime($dtblog['tgl_posting'])) ?>
					<br/>
					<center><img src="<?php echo base_url("assets/images/artikel/$dtblog[gambar_artikel]") ?>" style="padding-top: 30px;" width="80%"></center>
					<div class="detailblog">
						<p><?php echo $dtblog['isi_artikel']; ?></p>
					</div>
				</div>
				<div>
					<a href="javascript:history.back(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
				</div>
				<!-- end konten-->
				<br />
				<!-- mulai related post -->
				<div class="panel">
					<div class="panel-heading" style="border-top: 1px dashed #999; padding-bottom: 10px; margin-top: 10px;">
						<h3 class="panel-title" style="font-size: 20px; margin-left: -11px;">Artikel Terkait</h3>
					</div>
					<?php foreach ($artikel_terkait as $key => $ak): ?>
					<div class="media">
						<div class="media-left media-middle">
							<a href="<?php echo base_url("blog/$ak[url_artikel]"); ?>">
								<img class="media-object" src="<?php echo base_url("assets/images/artikel/$ak[gambar_artikel]"); ?>" alt="<?php echo $ak['judul_artikel'] ?>" width="80">
							</a>
						</div>
						<div class="media-body">
							<h4 class="media-heading" style="font-size: 18px;"><a href="<?php echo base_url("blog/$ak[url_artikel]"); ?>" style="color: #000;"><?php echo substr($ak['judul_artikel'], 0,150); ?></a></h4>
							<p style="font-size: 11px;"><i class="fa fa-calendar"></i> <?php echo "  ".date("d M Y", strtotime($ak['tgl_posting'])) ?></p>
						</div>
					</div>
					<?php endforeach ?>
				</div>
				<!-- end related post -->
				<br />
				<br />
				<br /> 
			</div>
			<div class="col-md-3 col-md-pull-9">
				<div class="panel panel-default">
					<div class="panel-heading panel-kategori">
						<h3 class="panel-title"><i class="fa fa-navicon"></i> KATEGORI</h3>
					</div>
					<ul class="list-group kategori">
						<?php foreach ($kategori as $key => $k): ?>
							<a data-toggle="collapse" data-target="#<?php echo $key; ?>" class="list-group-item"><?php echo $k['nama_kategori']; ?> <i class="fa fa-angle-down pull-right"></i></a>
							<ul class="list-group collapse" id="<?php echo $key ?>">
								<?php foreach ($subkategori as $key => $sk): ?>
									<?php if ($sk['id_kategori']==$k['id_kategori']): ?>
										<li style="list-style: none;"><a href="<?php echo base_url("kategori/$k[url_kategori]/$sk[url_subkategori]"); ?>" class="list-group-item" style="padding-left: 20px;"><i class="fa fa-angle-right" style="padding-right: 10px;"></i><?php echo $sk['nama_subkategori']; ?></a></li>
									<?php endif ?>
								<?php endforeach ?>
							</ul>
						<?php endforeach ?>
					</ul>
				</div>
				<br/>
				<br/>
			</div>
			<!-- Untuk Produk col-md-12 -->