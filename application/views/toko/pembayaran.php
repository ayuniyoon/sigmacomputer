<div class="container">
	<div class="row">
		<div class="panel panel-member">
			<div class="panel-body" style="margin-top: -30px;margin-bottom: 90px;">
				<div class="col-md-12">
					<br/>
					<div class="col-md-3">
						<p style="padding-top: 15px;">Lakukan konfirmasi pembayaran untuk memberitahu sales kami bahwa Anda telah melakukan pembayaran melalui nomor rekening yang tersedia. Cek kembali sms pemberitahuan kami, dan pastikan jumlah transfer sama dengan nominal pembayaran di sms pemberitahuan.</p>
					</div>
					<div class="col-md-9" style="border-left: 1px solid #e7e7e7;">
						<h3>KONFIRMASI PEMBAYARAN</h3>
						<p>Untuk memudahkan proses konfirmasi pembayaran, pastikan <strong>Nomor Transaksi</strong> yang Anda masukkan sesuai dengan Nomor Transaksi dalam SMS pemberitahuan.
						</p>
						<?php if ($cek=='berhasil'): ?>
							<div class="alert alert-success">Terima Kasih, Konfirmasi Anda berhasil dikirim. Pembayaran Anda sedang kami proses.<br/>Silahkan Login untuk melihat status pembayaran Anda.</div>
						<?php endif ?>
						<div class="col-md-9 center-margin">
							<form method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label>Nomor Transaksi</label>
									<input type="number" name="id_transaksi" class="form-control" id="invoice" onkeyup="cekinvoice();return false;" placeholder="Input Nomor Transaksi" required autofocus>
									<span id="pesan"></span>
								</div>
								<div class="form-group">
									<label>Via Bank</label>
									<select class="form-control" name="id_bank" required>
										<option value="">-Pilih Bank-</option>
										<?php foreach ($bank as $key => $value): ?>
											<option value="<?php echo $value['id_bank']; ?>"><?php echo $value['nama_bank']; ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="form-group">
									<label>Nama Pengirim</label>
									<input type="text" name="nama_pengirim" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Jumlah Transfer</label>
									<input type="number" name="jumlah_pembayaran" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal Transfer</label>
									<input type="date" name="tgl_transfer" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Bukti Pembayaran</label>
									<input type="file" name="bukti_pembayaran" class="form-control" required>
									<small>masukkan foto struk / screenshot pembayaran anda</small>
								</div>
								<br>
								<button type="submit" class="btn btn-success" style="margin-left:-15px;">Submit Konfirmasi Pembayaran</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>