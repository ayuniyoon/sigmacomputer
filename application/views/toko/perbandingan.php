<section>
	<div class="container" style="min-height: 600px;">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div style="display: inline;">
					<h3>Perbandingan</h3>
				</div>
				<?php if (empty($perbandingan)){ ?>
					<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						Klik produk yang akan dibandingkan. Close produk untuk mengganti produk yang baru. Fitur ini hanya bisa menampilkan maksimal 3 produk saja.
					</div>
				<?php }else{?>
					<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						Close produk untuk compare produk baru.
					</div>
				<?php }?>
			</div>
			<?php foreach ($perbandingan as $key => $value): ?>
				<div class="col-xs-12 col-md-4 col-sm-12">
					<div class="perbandingan">
						<a href="<?php echo base_url("home/perbandingan/$value[id_produk]"); ?>" type="button" class="close" data-dismiss="alert" style="padding: 5px 10px;" aria-label="Close"><span aria-hidden="true">&times;</span></a>
						<div class="text-center compare" style="padding: 5px;">
							<a href="<?php echo base_url("produk/$value[url_produk]"); ?>">
								<img src="<?php echo base_url("assets/images/produk/$value[foto_produk]"); ?>" alt="" title="<?php echo $value['nama_produk']; ?>">
							</a>
							<h4><a href="<?php echo base_url("produk/$value[url_produk]"); ?>" style="color: #000;"><?php echo $value['nama_produk']; ?></a></h4>
							<h3><?php echo rupiah($value['harga_produk']); ?></h3>
							<div style="text-align: left; padding-left: 5px;padding-right: 5px;">
								<p><b>Spesifikasi :	</b></p>
								<p style="width: 100%";>
									<?php if (!$value['spesifikasi']) {
										echo "Tidak ada";
									}else{
										echo $value['spesifikasi'];
									}?>
								</p>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
