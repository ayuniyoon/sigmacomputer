<section>
	<div class="container" style="min-height: 580px;">
		<div class="row">
			<div class="panel-heading kategori-heading">
				<h3>Kategori : <?php echo $judul['nama_kategori']; ?></h3>
			</div>
			<!-- mulai gadget -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="produkbykategori" id="watermark">
					<?php foreach ($kategori as $key => $k): ?>
						<div class="col-sm-3 col-md-2">
							<div class="thumbnail produk">
								<a href="<?php echo base_url("produk/$k[url_produk]"); ?>">
									<img src="<?php echo base_url("assets/images/produk/$k[foto_produk]"); ?>" alt="<?php echo $k['nama_produk']; ?>" title="<?php echo $k['nama_produk']; ?>" style="padding: 3px;"></a>
									<?php if ($k['stock_produk']==0){ ?>
									<button class="btn btn-danger">Habis</button>
									<?php } ?>
									<div class="caption text-center">
										<p><a href="<?php echo base_url("produk/$k[url_produk]"); ?>" style="color: #000;"><?php echo $k['nama_produk']; ?></a></p>
										<h4><?php echo rupiah($k['harga_produk']); ?></h4>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
					<div class="text-center" style="margin-bottom: 5%; margin-top: 15px;">
						<ul class="pagination">
							<?php echo $mpaging; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>