<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sigma Computer</title>
	<link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/font-awesome/css/font-awesome.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/owlcarousel/css/owl.carousel.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/owlcarousel/css/owl.theme.default.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/dist/css/main.css"); ?>">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
</head>
<body>
	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12 col-xs-12" align="center">
					<a href="<?php echo base_url(); ?>" class="text-center">
						<img src="<?php echo base_url("assets/images/logo/sigma-putih.png"); ?>" width="250">
					</a>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="kontak-head">
						<p>Hubungi Kami : <b><i class="fa fa-phone"></i> <?php echo $telepon['isi_pengaturan']." "; ?>&nbsp;&nbsp; <i class="fa fa-whatsapp"></i> <?php echo " ".$wa['isi_pengaturan']; ?></b>
						</p>
					</div>
					<div class="pencarian text-center">
						<form class="form-search" method="post" action="<?php echo base_url("cari"); ?>">
							<div class="input-group">
								<select class="form-control" name="kategori" onchange="submit()">
									<option value="">Pilih Kategori</option>
									<?php foreach ($kategori_header as $key => $k): ?>	
										<option value="<?php echo $k['id_kategori']; ?>"><?php echo $k['nama_kategori']; ?></option>
									<?php endforeach ?>
								</select>
								<input type="text" class="form-control" placeholder="Cari Produk..." name="nama_produk">
								<span class="input-group-btn">
									<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search" value="cari" name="cari"></i></button>
								</span>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="keranjang">
						<div class="btn-group">
							<a href="" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a>
							<a href="" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Keranjang  <span class="caret"></span></a>
							<ul class="dropdown-menu" style="width: 400;">
								<?php if (!$keranjang){ ?>
									<li><a>Keranjang kosong</a></li>
								<?php }else{ ?>
								<?php foreach ($keranjang as $id_produk => $value): ?>
									<li style="display: inline;"><a href="<?php echo base_url("produk/$value[url_produk]"); ?>"><?php echo $value['jumlah_beli']."X ".substr($value['nama_produk'], 0,35); ?></a></li>
								<?php endforeach ?>
								
								<?php } ?>
								<li class="divider"></li>
								<li><a href="<?php echo base_url("shoppingcart"); ?>">Checkout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#naff" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="naff">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo base_url(); ?>">Home</a></li>
					<li><a href="<?php echo base_url("perbandingan"); ?>">Perbandingan</a></li>
					<li><a href="<?php echo base_url("konfirmasi"); ?>">Konfirmasi</a></li>
					<li><a href="<?php echo base_url("blog"); ?>">Blog</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php if (isset($member)){ ?>
					<li class="dropdown">
						<a href="<?php echo base_url("login"); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hai, <?php echo $member['nama_member']; ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url("member"); ?>">Profil Saya</a></li>
							<li><a href="<?php echo base_url("logout"); ?>" onclick="return confirm('Yakin ingin keluar?');">Logout / Keluar</a></li>
						</ul>
					</li> <!-- li dropdown -->
					<?php }else{ ?>
					<li class="dropdown">
						<a href="<?php echo base_url("login"); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Login <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url("login"); ?>">Login</a></li>
							<li><a href="<?php echo base_url("register"); ?>">Register</a></li>
						</ul>
					</li> <!-- li dropdown -->
					<?php } ?>
				</ul>
				</div>
			</div>
		</nav>