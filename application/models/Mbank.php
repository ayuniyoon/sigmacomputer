<?php
/**
* 
*/
class Mbank extends CI_Model
{
	
	function tampil()
	{
		$this->db->order_by('id_bank', 'desc');
		$ambildata = $this->db->get("bank");
		$data = $ambildata->result_array();
		return $data;
	}
	function tambah($input)
	{
		$config['upload_path']		= './assets/images/bank';
		$config['allowed_types']	= 'gif|jpeg|jpg|png|JPG|JPEG|PNG|GIF';

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('logo_bank'))
		{
			$input['logo_bank'] = $this->upload->data('file_name');
		}

		$this->db->insert('bank',$input);
	}
	function ubah_data($input, $id_bank)
	{
		$config['upload_path']		= './assets/images/bank';
		$config['allowed_types']	= 'gif|jpeg|jpg|png|JPG|JPEG|PNG|GIF';

		$this->load->library("upload", $config);
		$ubahgambar = $this->upload->do_upload("logo_bank");

		if($ubahgambar)
		{
			$ubah = $this->ambil_data($id_bank);
			$hapus = $ubah['logo_bank'];

			if(!empty($hapus)){
				unlink("./assets/images/bank/".$hapus);
			}
			$input['logo_bank'] = $this->upload->data("file_name");

			$this->db->where('id_bank', $id_bank);
			$this->db->update('bank', $input);
		}
		else
		{	
			$this->db->where('id_bank', $id_bank);
			$this->db->update('bank', $input);
		}

	}
	function ambil_data($id_bank)
	{
		$this->db->where('id_bank', $id_bank);
		$data = $this->db->get("bank");
		$ambil = $data->row_array();
		return $ambil;
	}
	function hapus_data($id_bank)
	{
		$ambil = $this->ambil_data($id_bank);
		$hapus = $ambil['logo_bank'];

		if(!empty($hapus))
		{
			unlink("./assets/images/bank/".$hapus);
		}

		$this->db->where('id_bank', $id_bank);
		$this->db->delete('bank');
	}
}
?>