<?php

class Mkategori extends CI_Model
{
	
	function tampil()
	{
		$this->db->order_by('id_kategori', 'desc');
		$ambildata = $this->db->get('kategori');
		$data = $ambildata->result_array();
		return $data;
	}
	function tambah($input)
	{
		$data = $input['nama_kategori'];
		$url = strtolower($data);

		$url = str_replace(" ", "-", $url);
		$url = str_replace(",", "-", $url);
		$url = str_replace(".", "-", $url);
		$url = str_replace("?", "-", $url);
		$url = str_replace("(", "-", $url);
		$url = str_replace(")", "-", $url);
		$url = str_replace("!", "-", $url);
		$url = str_replace("{", "-", $url);
		$url = str_replace("}", "-", $url);
		$url = str_replace("[", "-", $url);
		$url = str_replace("]", "-", $url);
		$url = str_replace("+", "-", $url);
		$url = str_replace("/", "-", $url);
		$url = str_replace("'", "-", $url);
		$url = str_replace('"', "-", $url);
		$url = str_replace("&", "-", $url);
		$url = str_replace("%", "-", $url);
		$url = str_replace("*", "-", $url);
		$url = str_replace("#", "-", $url);
		$url = str_replace("@", "-", $url);
		$url = str_replace("$", "-", $url);
		$url = str_replace("=", "-", $url);
		$url = str_replace("`", "-", $url);
		$url = str_replace("^", "-", $url);
		$url = str_replace(":", "-", $url);
		$url = str_replace(";", "-", $url);
		$url = str_replace(">", "-", $url);
		$url = str_replace("<", "-", $url);
		$url = str_replace("~", "-", $url);
		$url = str_replace("_", "-", $url);

		$input['url_kategori'] = $url;

		$this->db->insert('kategori',$input);
	}
	function ambil_data($id_kategori)
	{
		// query panggil database
		$this->db->where('id_kategori', $id_kategori);
		$data = $this->db->get('kategori');
		$ambil = $data->row_array();
		return $ambil;
	}
	function ambil_judul($url_kategori)
	{
		$this->db->where('url_kategori', $url_kategori);
		$data = $this->db->get('kategori');
		$ambil = $data->row_array();
		return $ambil;
	}
	function hapus_data($id_kategori)
	{
		$this->db->where('id_kategori', $id_kategori);
		$this->db->delete('kategori');
	}
	function ubah_data($input, $id_kategori)
	{
		$this->db->where('id_kategori', $id_kategori);
		$this->db->update('kategori', $input);
	}
	function tampil2()
	{
		$ambildata = $this->db->get('kategori');
		$data = $ambildata->row_array();
		return $data;
	}
	function kategori_sidenav()
	{
		$this->db->limit(9);
		$ambil = $this->db->get('kategori');
		$data = $ambil->result_array();
		return $data;
	}
	function kategori_nav()
	{
		$ambil = $this->db->get('kategori');
		$data = $ambil->result_array();
		return $data;
	}
}
?>