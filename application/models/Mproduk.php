<?php

class Mproduk extends CI_Model
{
	function tampil()
	{
		$this->db->order_by('id_produk', 'desc');
		$this->db->join('subkategori', 'produk.id_subkategori=subkategori.id_subkategori','left');
		$this->db->join('brand', 'produk.id_brand=brand.id_brand', 'left');
		$ambil = $this->db->get("produk");
		$data = $ambil->result_array();
		return $data;
	}
	function ambil_subkategori($id_kategori)
	{
		$this->db->where('id_kategori', $id_kategori);
		$ambil = $this->db->get("subkategori");
		$data = $ambil->result_array();
		return $data;
	}
	function ambil_data($id_produk)
	{	
		$this->db->join('subkategori', 'produk.id_subkategori=subkategori.id_subkategori','left');
		$this->db->join('brand', 'produk.id_brand=brand.id_brand', 'left');
		$this->db->where('id_produk', $id_produk);
		$ambil = $this->db->get('produk');
		$data = $ambil->row_array();
		return $data;
	}
	function keranjang($id_produk)
	{
		$semuadata=array();
		$this->db->where('id_produk', $id_produk);
		$ambil = $this->db->get('produk');
		$semuadata[] = $ambil->row_array();
		return $semuadata;

	}
	function produk_related($id_subkategori)
	{
		$this->db->limit(15);
		$this->db->where('id_subkategori', $id_subkategori);
		$ambil = $this->db->get('produk');
		$data = $ambil->result_array();
		return $data;
	}
	function ambil_produk_perbandingan($perbandingan)
	{
		$semua_produk=array();

		if (isset($perbandingan)) {
			foreach ($perbandingan as $key => $id_produk) {
				$ambil_data = $this->ambil_data($id_produk);
				$semua_produk[] = $ambil_data;
			}
		}
		else{
			echo "";
		}
		return $semua_produk;
	}
	function tambah($input)
	{
		$data['id_subkategori'] = $input['id_subkategori'];
		$data['id_brand'] = $input['id_brand'];
		$data['kode_produk'] = $input['kode_produk'];
		$data['nama_produk'] = $input['nama_produk'];
		$data['berat_produk'] = $input['berat_produk'];
		$data['stock_produk'] = $input['stock_produk'];
		$data['harga_produk'] = $input['harga_produk'];
		$data['spesifikasi'] = $input['spesifikasi'];
		$data['deskripsi'] = $input['deskripsi'];

		$url = strtolower($data['nama_produk']);
		$url = str_replace(" ", "-", $url);
		$url = str_replace(",", "-", $url);
		$url = str_replace(".", "-", $url);
		$url = str_replace("?", "-", $url);
		$url = str_replace("(", "-", $url);
		$url = str_replace(")", "-", $url);
		$url = str_replace("!", "-", $url);
		$url = str_replace("{", "-", $url);
		$url = str_replace("}", "-", $url);
		$url = str_replace("[", "-", $url);
		$url = str_replace("]", "-", $url);
		$url = str_replace("+", "-", $url);
		$url = str_replace("/", "-", $url);
		$url = str_replace("'", "-", $url);
		$url = str_replace('"', "-", $url);
		$url = str_replace("&", "-", $url);
		$url = str_replace("%", "-", $url);
		$url = str_replace("*", "-", $url);
		$url = str_replace("#", "-", $url);
		$url = str_replace("@", "-", $url);
		$url = str_replace("$", "-", $url);
		$url = str_replace("=", "-", $url);
		$url = str_replace("`", "-", $url);
		$url = str_replace("^", "-", $url);
		$url = str_replace(":", "-", $url);
		$url = str_replace(";", "-", $url);
		$url = str_replace(">", "-", $url);
		$url = str_replace("<", "-", $url);
		$url = str_replace("~", "-", $url);
		$url = str_replace("_", "-", $url);

		$data['url_produk'] = $url;

		$config['upload_path']		= './assets/images/produk';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';

        // panggil library upload
		$this->load->library('upload', $config);
        // jika benar upload gambar
		if ($this->upload->do_upload('foto_produk'))
		{
			$data['foto_produk'] = $this->upload->data('file_name');
		}
        // query input data ke database
		$this->db->insert('produk', $data);
	}
	function ubah($input, $id_produk)
	{

		$data['id_subkategori'] = $input['id_subkategori'];
		$data['id_brand'] = $input['id_brand'];
		$data['kode_produk'] = $input['kode_produk'];
		$data['nama_produk'] = $input['nama_produk'];
		$data['berat_produk'] = $input['berat_produk'];
		$data['stock_produk'] = $input['stock_produk'];
		$data['harga_produk'] = $input['harga_produk'];
		$data['spesifikasi'] = $input['spesifikasi'];
		$data['deskripsi'] = $input['deskripsi'];

		$url = strtolower($data['nama_produk']);
		$url = str_replace(" ", "-", $url);
		$url = str_replace(",", "-", $url);
		$url = str_replace(".", "-", $url);
		$url = str_replace("?", "-", $url);
		$url = str_replace("(", "-", $url);
		$url = str_replace(")", "-", $url);
		$url = str_replace("!", "-", $url);
		$url = str_replace("{", "-", $url);
		$url = str_replace("}", "-", $url);
		$url = str_replace("[", "-", $url);
		$url = str_replace("]", "-", $url);
		$url = str_replace("+", "-", $url);
		$url = str_replace("/", "-", $url);
		$url = str_replace("'", "-", $url);
		$url = str_replace('"', "-", $url);
		$url = str_replace("&", "-", $url);
		$url = str_replace("%", "-", $url);
		$url = str_replace("*", "-", $url);
		$url = str_replace("#", "-", $url);
		$url = str_replace("@", "-", $url);
		$url = str_replace("$", "-", $url);
		$url = str_replace("=", "-", $url);
		$url = str_replace("`", "-", $url);
		$url = str_replace("^", "-", $url);
		$url = str_replace(":", "-", $url);
		$url = str_replace(";", "-", $url);
		$url = str_replace(">", "-", $url);
		$url = str_replace("<", "-", $url);
		$url = str_replace("~", "-", $url);
		$url = str_replace("_", "-", $url);

		$data['url_produk'] = $url;

		$config['upload_path']		= './assets/images/produk';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';

		// panggil library upload
		$this->load->library('upload', $config);

		//jika ada proses upload foto/file
		if ($this->upload->do_upload('foto_produk')) {
			$data['foto_produk'] = $this->upload->data('file_name');

			// menghapus foto lama
			$ambil = $this->ambil_data($id_produk);
			$foto_lama = $ambil['foto_produk'];

			if (!empty($foto_lama)) {
				unlink("./assets/images/produk/".$foto_lama);
			}

			$this->db->where('id_produk', $id_produk);
			$this->db->update('produk', $data);
		}
		else
		{
			// mengubah data dari table produk
			$this->db->where('id_produk', $id_produk);
			$this->db->update('produk', $data);
		}
	}
	function hapus($id_produk)
	{
		$this->db->where('id_produk', $id_produk);
		$data = $this->db->get('produk');
		$ambil = $data->row_array();

		// $ambil = $this->ambil_data($id_produk);
		$gambar = $ambil['foto_produk'];

		if (!empty($gambar)) {
			unlink("./assets/images/produk/".$gambar);
		}

		$this->db->where('id_produk', $id_produk);
		$this->db->delete('produk');
	}
// untuk halaman depan / homepage
	function produk_kategori($id_kategori)
	{
		$this->db->limit(15);
		$this->db->where('id_kategori', $id_kategori);
		$this->db->join('subkategori sk', 'produk.id_subkategori=sk.id_subkategori');
		$ambil = $this->db->get('produk');
		$data = $ambil->result_array();
		return $data;
	}
	function produk_url($url_produk)
	{
		$this->db->join('subkategori', 'produk.id_subkategori=subkategori.id_subkategori','left');
		$this->db->join('brand', 'produk.id_brand=brand.id_brand', 'left');
		$this->db->where('url_produk', $url_produk);
		$ambil = $this->db->get('produk');
		$data = $ambil->row_array();
		return $data;
	}
	function terbaru()
	{
		$this->db->order_by('id_produk', 'desc');
		$this->db->limit(20);
		$ambil = $this->db->get('produk');
		$data = $ambil->result_array();
		return $data;
	}
	function bykategori($url_kategori)
	{
		$this->db->order_by('id_produk', 'desc');
		$this->db->join('subkategori sk', 'produk.id_subkategori=sk.id_subkategori', 'left');
		$this->db->join('kategori k', 'sk.id_kategori=k.id_kategori', 'left');
		$this->db->where('url_kategori', $url_kategori);
		$ambil = $this->db->get('produk');
		$data = $ambil->result_array();
		return $data;
	}
	function bysubkategori($url_subkategori)
	{
		$this->db->order_by('id_produk', 'desc');
		$this->db->join('subkategori sk', 'produk.id_subkategori=sk.id_subkategori', 'left');
		$this->db->where('url_subkategori', $url_subkategori);
		$ambil = $this->db->get('produk');
		$data = $ambil->result_array();
		return $data;
	}
	function tampil_keranjang($keranjang)
	{
		$semua_produk=array();
		if (isset($keranjang)) {
			foreach ($keranjang as $id_produk => $qty) {
				$produk = $this->ambil_data($id_produk);
				$produk['jumlah_beli']=$qty;
				$produk['subharga']=$produk['harga_produk']*$qty;
				$semua_produk[]=$produk;
			}
			return $semua_produk;
		}
		else
		{
			echo "";
		}
	}
	function cari_produk($nama_produk)
	{
		$ambil = $this->db->query("SELECT * from produk where nama_produk like '%$nama_produk%' ");
		$data = $ambil->result_array();
		return $data;
	}
	function cari_produk_by_kategori($id_kategori)
	{
		$ambil = $this->db->query("SELECT * from produk LEFT JOIN subkategori ON produk.id_subkategori=subkategori.id_subkategori where id_kategori like '%$id_kategori%' ");
		$data = $ambil->result_array();
		return $data;
	}
	function kategori_produk_pagination($url_kategori, $batas, $page)
	{
		$this->db->order_by('id_produk', 'desc');
		$this->db->join('subkategori sk', 'produk.id_subkategori=sk.id_subkategori', 'left');
		$this->db->join('kategori k', 'sk.id_kategori=k.id_kategori', 'left');
		$this->db->where('url_kategori', $url_kategori);
		$ambil = $this->db->get('produk', $batas, $page);
		$semua_data = $ambil->result_array();
		return $semua_data;
	}
	function subkategori_produk_pagination($url_subkategori, $batas, $page)
	{
		$this->db->order_by('id_produk', 'desc');
		$this->db->join('subkategori sk', 'produk.id_subkategori=sk.id_subkategori', 'left');
		$this->db->where('url_subkategori', $url_subkategori);
		$ambil = $this->db->get('produk', $batas, $page);
		$semua_data = $ambil->result_array();
		return $semua_data;
	}



}
?>