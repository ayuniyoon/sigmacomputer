<?php

class Mbrand extends CI_Model
{
	
	function tampil()
	{
		$this->db->order_by('nama_brand','asc');
		$ambildata = $this->db->get('brand');
		$data = $ambildata->result_array();
		return $data;

	}
	function tambah($input)
	{
		$nama_brand = $input['nama_brand'];
		$url = strtolower($nama_brand);

		$url = str_replace(" ", "-", $url);
		$url = str_replace(",", "-", $url);
		$url = str_replace(".", "-", $url);
		$url = str_replace("?", "-", $url);
		$url = str_replace("(", "-", $url);
		$url = str_replace(")", "-", $url);
		$url = str_replace("!", "-", $url);
		$url = str_replace("{", "-", $url);
		$url = str_replace("}", "-", $url);
		$url = str_replace("[", "-", $url);
		$url = str_replace("]", "-", $url);
		$url = str_replace("+", "-", $url);
		$url = str_replace("/", "-", $url);
		$url = str_replace("'", "-", $url);
		$url = str_replace('"', "-", $url);
		$url = str_replace("&", "-", $url);
		$url = str_replace("%", "-", $url);
		$url = str_replace("*", "-", $url);
		$url = str_replace("#", "-", $url);
		$url = str_replace("@", "-", $url);
		$url = str_replace("$", "-", $url);
		$url = str_replace("=", "-", $url);
		$url = str_replace("`", "-", $url);
		$url = str_replace("^", "-", $url);
		$url = str_replace(":", "-", $url);
		$url = str_replace(";", "-", $url);
		$url = str_replace(">", "-", $url);
		$url = str_replace("<", "-", $url);
		$url = str_replace("~", "-", $url);
		$url = str_replace("_", "-", $url);

		$input['url_brand'] = $url;

		$config['upload_path']		= './assets/images/brand';
		$config['allowed_types']	= 'gif|jpeg|jpg|png|JPG|JPEG|PNG|GIF';

        // panggil library upload
		$this->load->library('upload', $config);

        // jika benar upload gambar
		if ($this->upload->do_upload('gambar_brand'))
		{
			$input['gambar_brand'] = $this->upload->data('file_name');
		}
        // query input data ke database
		$this->db->insert('brand', $input);
	}
	function ambil_data($id_brand)
	{
		// query SELECT * FROM brand WHERE id_brand = '$id_brand';
		$this->db->where('id_brand', $id_brand);
		$data = $this->db->get('brand');
		$ambil = $data->row_array();
		return $ambil;
	}
	function hapus_data($id_brand)
	{
		// query delete * from brand where id_brand = $id_brand
		$ambil = $this->ambil_data($id_brand);
		$hapusgambar = $ambil['gambar_brand'];

		if (!empty($hapusgambar))
		{
			unlink("./assets/images/brand/".$hapusgambar);
		}

		$this->db->where('id_brand', $id_brand);
		$this->db->delete('brand');
	}
	function ubah_data($input, $id_brand)
	{
		$config['upload_path']		= './assets/images/brand';
		$config['allowed_types']	= 'gif|jpeg|jpg|png|JPG|JPEG|PNG|GIF';

		$this->load->library("upload", $config);
		$ubahgambar = $this->upload->do_upload("gambar_brand");

        // jika gambar diubah
		if ($ubahgambar) {

			$ubah = $this->ambil_data($id_brand);
			$hapusgambar = $ubah['gambar_brand'];

			if (!empty($hapusgambar)) {
				unlink("./assets/images/brand/".$hapusgambar);
			}

        	// ambil nama gambar dari formulir ubah
			$input["gambar_brand"] = $this->upload->data("file_name");

			$this->db->where("id_brand", $id_brand);
			$this->db->update('brand', $input);
		}
		else
		{
			$this->db->where("id_brand", $id_brand);
			$this->db->update('brand', $input);
		}
	}
	// untuk halaman homepage
	function brand()
	{
		$this->db->limit(20);
		$ambil = $this->db->get('brand');
		$brand = $ambil->result_array();
		return $brand;
	}

}
?>
