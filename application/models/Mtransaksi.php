<?php

class Mtransaksi extends CI_Model
{
	function total_penjualan()
	{
		$ambil = $this->db->query("SELECT * FROM transaksi WHERE status_pembelian='1' GROUP BY month(tgl_transaksi)");
		$data = $ambil->result_array();

		$semuadata=array();
		foreach ($data as $key => $value)
		{
			$bulan = date("m",strtotime($value['tgl_transaksi']));
			// $bulan = date("m");
			$ambildata = $this->db->query("SELECT SUM(total_pembelian), tgl_transaksi FROM transaksi WHERE MONTH(tgl_transaksi)='$bulan' AND status_pembelian='1' ");

			$semuadata[] = $ambildata->result_array();
		}
		return $semuadata;
	}
	function total_sales()
	{
		$ambil = $this->db->query("SELECT * FROM transaksi GROUP BY MONTH(tgl_transaksi)");
		$data = $ambil->result_array();

		foreach ($data as $key => $value) {
			$bulanini = date("m");
			$ambildata = $this->db->query("SELECT SUM(total_pembelian) FROM transaksi WHERE status_pembelian='1' AND status_pengiriman='1' AND MONTH(tgl_transaksi)='$bulanini' ");
			$total = $ambildata->row_array();

		}
		return $total;
	}
	function jumlah_penjualan()
	{
		$ambil = $this->db->get('transaksi');
		$data = $ambil->result_array();

		foreach ($data as $key => $value) {
			$bulanini = date("m");
			$ambildata = $this->db->query("SELECT * FROM transaksi LEFT JOIN member ON member.id_member=transaksi.id_member WHERE MONTH(tgl_transaksi)='$bulanini' order by id_transaksi DESC ");
			$dataarray = $ambildata->result_array();
		}
		return $dataarray;
	}	
	function tampil()
	{
		$this->db->order_by('id_transaksi', 'desc');
		$this->db->join('member', 'member.id_member=transaksi.id_member', 'left');
		$ambil = $this->db->get('transaksi');
		$data = $ambil->result_array();
		return $data;
	}
	function detail($id_transaksi)
	{
		$this->db->where('id_transaksi', $id_transaksi);
		$ambil = $this->db->get('detail_transaksi');
		$data = $ambil->result_array();
		return $data;

	}
	function ambil($id_transaksi)
	{
		$this->db->where('id_transaksi', $id_transaksi);
		$this->db->join('member m', 'transaksi.id_member=m.id_member', 'left');
		$ambil = $this->db->get('transaksi');
		$data = $ambil->row_array();
		return $data;
	}
	function bayar($data, $id_transaksi)
	{
		$this->db->where('id_transaksi', $id_transaksi);
		$this->db->update('transaksi', $data);
	}
	function kirim_barang($data, $id_transaksi)
	{
		$this->db->where('id_transaksi', $id_transaksi);
		$ambildata = $this->db->get('transaksi');
		$semua = $ambildata->row_array();

		if ($semua['status_pembelian']==0) {
			return 'gagal';
		}
		else
		{
			$this->db->where('id_transaksi',$id_transaksi);
			$this->db->update('transaksi', $data);
			$semuadata = $this->detail($id_transaksi);
			foreach ($semuadata as $key => $value) 
			{
				$this->db->where('id_produk',$value['id_produk']);	
				$ambil = $this->db->get('produk');
				$dapat = $ambil->row_array();

				$hasil['stock_produk'] = $dapat['stock_produk']-$value['jumlah_beli'];

				$this->db->where('id_produk',$value['id_produk']);
				$this->db->update('produk',$hasil);
			}
			return 'berhasil';
		}

	}
	function simpan_transaksi($input,$keranjang)
	{
		$login = $this->session->userdata('member');
		$pembelian['id_member']=$login['id_member'];
		$pembelian['nama_penerima'] = $input['nama_penerima'];
		$pembelian['telepon_penerima'] = $input['telepon_penerima'];
		$pembelian['alamat_penerima'] = $input['alamat_penerima'];
		$pembelian['kota_tujuan'] = $input['kota_tujuan'];
		$pembelian['tipe_kota'] = $input['tipe_kota'];
		$pembelian['nama_kecamatan'] = $input['nama_kecamatan'];
		$pembelian['kode_pos'] = $input['kode_pos'];
		$pembelian['ekspedisi'] = $input['kurir'];
		$pembelian['paket_ekspedisi'] = $input['paket_ekspedisi'];
		$pembelian['estimasi_hari'] = $input['estimasi_hari'];
		$pembelian['total_berat'] = $input['total_berat'];
		$pembelian['total_jmlbeli'] = $input['total_jmlbeli'];
		$pembelian['total_harga'] = $input['total_harga'];
		$pembelian['ongkos_kirim'] = $input['ongkos_kirim'];
		$pembelian['total_pembelian'] = $input['ongkos_kirim']+$input['total_harga'];
		$pembelian['status_pembelian'] = "0";
		$pembelian['status_pengiriman'] = "0";
		$pembelian['tgl_transaksi'] = date("Y-m-d");

		$this->db->insert('transaksi',$pembelian);

		$id_transaksi = $this->db->insert_id('transaksi');

		foreach ($keranjang as $id_produk => $jml)
		{
			$this->db->where('id_produk',$id_produk);
			$ambil = $this->db->get('produk');
			$produk = $ambil->row_array();

			$detail['id_produk'] = $produk['id_produk'];
			$detail['nama_produk'] = $produk['nama_produk'];
			$detail['harga_produk'] = $produk['harga_produk'];
			$detail['berat_produk'] = $produk['berat_produk'];
			$detail['jumlah_beli']=$jml;
			$detail['sub_berat'] = $jml*$produk['berat_produk'];
			$detail['sub_harga'] = $jml*$produk['harga_produk'];
			$detail['id_transaksi'] = $id_transaksi;

			$this->db->insert('detail_transaksi',$detail);
		}


		$sms['total_pembayaran']="Rp. ".number_format($pembelian['total_pembelian']);
		$sms['id_transaksi']=$id_transaksi;

		$this->db->where('id', '1');
		$ambildata = $this->db->get('temp_sms');
		$datasms = $ambildata->row_array();

		$isi_sms_1 = $datasms['temp_1'];
		$isi_sms_2 = $datasms['temp_2'];

		foreach ($sms as $key => $nilai)
		{
			$isi_sms_1 = str_replace("{".$key."}", $nilai, $isi_sms_1);
		}


		if(empty($login['no_hp']))
		{

		}
		else
		{
			// sms ke pembeli
			$telepon=$login['no_hp'];
			$userkey = "otbqzn"; //userkey lihat di zenziva
			$passkey = "sigmacomputer"; // set passkey di zenziva
			$url = "https://reguler.zenziva.net/apps/smsapi.php";
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($isi_sms_1));
			curl_setopt($curlHandle, CURLOPT_HEADER, 0);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
			curl_setopt($curlHandle, CURLOPT_POST, 1);
			$results = curl_exec($curlHandle);
			curl_close($curlHandle);

			$XMLdata = new SimpleXMLElement($results);
			$status = $XMLdata->message[0]->text;
			
		}

		// sms ke administrator
		$telepon="085728735223";
		$userkey = "otbqzn"; //userkey lihat di zenziva
		$passkey = "sigmacomputer"; // set passkey di zenziva
		$url = "https://reguler.zenziva.net/apps/smsapi.php";
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($isi_sms_2));
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		$results = curl_exec($curlHandle);
		curl_close($curlHandle);

		$XMLdata = new SimpleXMLElement($results);
		$status = $XMLdata->message[0]->text;

		return $id_transaksi;
	}

	function ambil_riwayat($id_member)
	{
		$this->db->join('member', 'transaksi.id_member=member.id_member', 'left');
		$this->db->where('transaksi.id_member', $id_member);
		$this->db->order_by('id_transaksi', 'desc');
		$ambil = $this->db->get('transaksi');
		$data = $ambil->result_array();

		return $data;
	}
	function ambil_invoice($id_transaksi)
	{
		$this->db->join('member', 'transaksi.id_member=member.id_member', 'left');
		$this->db->where('transaksi.id_transaksi', $id_transaksi);
		$ambil = $this->db->get('transaksi');
		$data = $ambil->row_array();

		return $data;
	}

	function ubah_status($id,$status)
	{
		$this->db->where('id_transaksi',$id);
		$this->db->update('transaksi',$status);
	}


	
}
?>