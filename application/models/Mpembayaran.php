<?php

class Mpembayaran extends CI_Model
{
	function jumlah_konfirmasi()
	{
		$ambil = $this->db->get('pembayaran'); 
		$array = $ambil->result_array();

		foreach ($array as $key => $value) {
			$bulanini = date("m");
			$ambildata = $this->db->query("SELECT * FROM pembayaran LEFT JOIN transaksi ON pembayaran.id_transaksi=transaksi.id_transaksi LEFT JOIN bank ON pembayaran.id_bank=bank.id_bank WHERE MONTH(tgl_konfirmasi)='$bulanini' order by tgl_konfirmasi desc");
			$data = $ambildata->result_array();
		}
		
		return $data;
	}
	function tampil()
	{
		$this->db->order_by('tgl_konfirmasi', 'desc');
		$this->db->join('bank b', 'pembayaran.id_bank=b.id_bank', 'left');
		$this->db->join('transaksi', 'pembayaran.id_transaksi=transaksi.id_transaksi', 'left');
		$ambil = $this->db->get('pembayaran');
		$data = $ambil->result_array();

		return $data;
	}
	function ambil_data($id_transaksi)
	{
		$this->db->where('pembayaran.id_transaksi', $id_transaksi);
		$this->db->join('transaksi', 'pembayaran.id_transaksi=transaksi.id_transaksi', 'left');
		$this->db->join('bank b', 'pembayaran.id_bank=b.id_bank', 'left');
		$ambil = $this->db->get('pembayaran');
		$data = $ambil->row_array();
		
		return $data;
	}
	function konfirmasi($input)
	{
		$input['tgl_konfirmasi'] = date("Y-m-d");

		$config['upload_path']		= './assets/images/konfirmasi/';
		$config['allowed_types']	= 'gif|jpeg|jpg|png';
		
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('bukti_pembayaran'))
		{
			$input['bukti_pembayaran'] = $this->upload->data('file_name');
		}
		$this->db->insert('pembayaran', $input);

		// lakukan sms notifikasi
		$this->db->where('id', '1');
		$ambilsms = $this->db->get('temp_sms');
		$datasms = $ambilsms->row_array();

		$pesan = $datasms['temp_3'];

		$telepon="085728735223";
		$userkey = "otbqzn"; //userkey lihat di zenziva
		$passkey = "sigmacomputer"; // set passkey di zenziva
		$url = "https://reguler.zenziva.net/apps/smsapi.php";
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($pesan));
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		$results = curl_exec($curlHandle);
		curl_close($curlHandle);

		$XMLdata = new SimpleXMLElement($results);
		$status = $XMLdata->message[0]->text;		
	

	}
	// function bayar($id_transaksi)
	// {
	// 	$this->db->where('id_transaksi', $id_transaksi);
	// 	$ambil = $this->db->get('pembayaran');
	// 	$data = $ambil->row_array();

	// 	return $data;
	// }





}
?>