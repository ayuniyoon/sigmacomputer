<?php

class Mmember extends CI_Model
{
	function jumlah_mendaftar()
	{
		$ambil = $this->db->get('member');
		$data = $ambil->result_array();

		foreach ($data as $key => $value) {
			$bulanini = date("m");
			$ambildata = $this->db->query("SELECT * FROM member WHERE MONTH(tgl_join)='$bulanini' ORDER BY tgl_join desc");
			$total = $ambildata->result_array();
		}
		return $total;
	}
	function tampil()
	{
		$this->db->order_by('id_member','desc');
		$ambildata = $this->db->get('member');
		$data = $ambildata->result_array();
		return $data;
	}
	function ambil_data($id_member)
	{
		$this->db->where('id_member', $id_member);
		$data = $this->db->get('member');
		$ambil = $data->row_array();
		return $ambil;
	}
	// untuk form login member
	function ambil_email($email)
	{
		$this->db->where('email_member', $email);
		$ambil = $this->db->get('member');
		$data = $ambil->row_array();
		return $data;
	}
	function hapus_data($id_member)
	{
		$this->db->where('id_member', $id_member);
		$this->db->delete('member');
	}
	function simpan($input){
		$member['no_hp'] = $input['no_hp'];
		$member['email_member'] = $input['email_member'];
		$member['password'] = sha1($input['password']);
		$member['tgl_join'] = date("Y-m-d");

		$this->db->insert('member', $member);
	}
	function ubah($id_member,$input)
	{
		$member['id_member'] = $id_member;
		$member['nama_member'] = $input['nama_member'];
		$member['email_member'] = $input['email_member'];
		$member['no_hp'] = $input['no_hp'];
		$member['alamat_member'] = $input['alamat_member'];
		$member['password'] = sha1($input['password']);

		$this->db->where('id_member', $id_member);
		$this->db->update('member', $member);
	}
	function login($input)
	{
		$email = $input['email_member'];
		$password = sha1($input['password']);

		$this->db->where('email_member', $email);
		$this->db->where('password', $password);
		$member = $this->db->get('member');

		$hasil = $member->num_rows();

		if ($hasil>0) {
		$akun = $member->row_array();
			$this->session->set_userdata('member', $akun);

			return 'berhasil';
		}
		else
		{
			return 'gagal';
		}
	}
}
?>