<?php

class Martikel extends CI_Model
{
	
	function tampil()
	{
		$this->db->order_by('id_artikel', 'desc');
		$ambildata = $this->db->get('artikel');
		$data = $ambildata->result_array();
		return $data;
	}
	function tambah($input)
	{
		$judul = $input['judul_artikel'];
		$url = strtolower($judul);

		$url = str_replace(" ", "-", $url);
		$url = str_replace(",", "", $url);
		$url = str_replace(".", "", $url);
		$url = str_replace("?", "-", $url);
		$url = str_replace("(", "-", $url);
		$url = str_replace(")", "-", $url);
		$url = str_replace("!", "-", $url);
		$url = str_replace("{", "-", $url);
		$url = str_replace("}", "-", $url);
		$url = str_replace("[", "-", $url);
		$url = str_replace("]", "-", $url);
		$url = str_replace("+", "-", $url);
		$url = str_replace("/", "-", $url);
		$url = str_replace("'", "-", $url);
		$url = str_replace('"', "-", $url);
		$url = str_replace("&", "-", $url);
		$url = str_replace("%", "-", $url);
		$url = str_replace("*", "-", $url);
		$url = str_replace("#", "-", $url);
		$url = str_replace("@", "-", $url);
		$url = str_replace("$", "-", $url);
		$url = str_replace("=", "-", $url);
		$url = str_replace("`", "-", $url);
		$url = str_replace("^", "-", $url);
		$url = str_replace(":", "-", $url);
		$url = str_replace(";", "-", $url);
		$url = str_replace(">", "-", $url);
		$url = str_replace("<", "-", $url);
		$url = str_replace("~", "-", $url);
		$url = str_replace("_", "-", $url);
		$url = str_replace("”", "-", $url);

		$input['url_artikel'] = $url;

		$config['upload_path']		= './assets/images/artikel/';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';

		$this->load->library("upload", $config);
		$gambar = $this->upload->do_upload("gambar_artikel");

		if ($gambar) {
			$input['gambar_artikel'] = $this->upload->data('file_name');
		}

		$this->db->insert('artikel', $input);
		redirect('admin/artikel', 'refresh');

	}
	function ambil_url($url_artikel)
	{
		$this->db->where('url_artikel', $url_artikel);
		$data = $this->db->get('artikel');
		$ambil = $data->row_array();
		return $ambil;
	}
	function ambil_data($id_artikel)
	{
		// select * from artikel where id_artikel = '$id_artikel'
		$this->db->where('id_artikel', $id_artikel);
		$data = $this->db->get('artikel');
		$ambil = $data->row_array();
		return $ambil;
	}
	function hapus_data($id_artikel)
	{
		$ambil = $this->ambil_data($id_artikel);
		$hapusgambar = $ambil['gambar_artikel'];

		if (!empty($hapusgambar)) {
			unlink("./assets/images/artikel/".$hapusgambar);
		}

		$this->db->where('id_artikel', $id_artikel);
		$this->db->delete('artikel');
	}
	function ubah_data($input, $id_artikel)
	{
		$config['upload_path']		= './assets/images/artikel/';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';

		$this->load->library("upload", $config);
		$ubahgambar = $this->upload->do_upload("gambar_artikel");

        // jika gambar diubah
		if ($ubahgambar) {

			$ubah = $this->ambil_data($id_artikel);
			$hapusgambar = $ubah['gambar_artikel'];

			if (!empty($hapusgambar)) {
				unlink("./assets/images/artikel/".$hapusgambar);
			}

        	// ambil nama gambar dari formulir
			$input['gambar_artikel'] = $this->upload->data("file_name");
			$this->db->where("id_artikel", $id_artikel);
			$this->db->update('artikel', $input);
		}
		else
		{
        	 // gambar tidak dirubah langsung jalankan query
			$this->db->where("id_artikel", $id_artikel);
			$this->db->update('artikel', $input);
		}
	}
	// function depan
	function artikel_terkait()
	{
		$this->db->order_by('id_artikel', 'desc');
		$this->db->limit(5);
		$ambil = $this->db->get('artikel');
		$data = $ambil->result_array();
		return $data;
	}
	function tampil_blog_pagination($batas, $page)
	{
		$this->db->order_by('id_artikel', 'desc');
		$ambil = $this->db->get('artikel', $batas, $page);
		$semua_data = $ambil->result_array();
		return $semua_data;
	}

	
}
?>