<?php

class Msubkategori extends CI_Model
{
	
	function tampil()
	{
		$this->db->order_by('id_subkategori','desc');
		$this->db->join('kategori', 'subkategori.id_kategori=kategori.id_kategori', 'left');
		$ambil = $this->db->get("subkategori");
		$data = $ambil->result_array();
		return $data;
	}
	function tambah($input)
	{
		$data = $input['nama_subkategori']; 
		$url = strtolower($data);

		$url = str_replace(" ", "-", $url);
		$url = str_replace(",", "-", $url);
		$url = str_replace(".", "-", $url);
		$url = str_replace("?", "-", $url);
		$url = str_replace("(", "-", $url);
		$url = str_replace(")", "-", $url);
		$url = str_replace("!", "-", $url);
		$url = str_replace("{", "-", $url);
		$url = str_replace("}", "-", $url);
		$url = str_replace("[", "-", $url);
		$url = str_replace("]", "-", $url);
		$url = str_replace("+", "-", $url);
		$url = str_replace("/", "-", $url);
		$url = str_replace("'", "-", $url);
		$url = str_replace('"', "-", $url);
		$url = str_replace("&", "-", $url);
		$url = str_replace("%", "-", $url);
		$url = str_replace("*", "-", $url);
		$url = str_replace("#", "-", $url);
		$url = str_replace("@", "-", $url);
		$url = str_replace("$", "-", $url);
		$url = str_replace("=", "-", $url);
		$url = str_replace("`", "-", $url);
		$url = str_replace("^", "-", $url);
		$url = str_replace(":", "-", $url);
		$url = str_replace(";", "-", $url);
		$url = str_replace(">", "-", $url);
		$url = str_replace("<", "-", $url);
		$url = str_replace("~", "-", $url);
		$url = str_replace("_", "-", $url);

		$input['url_subkategori'] = $url;

		$this->db->insert('subkategori',$input);
	}
	function ambil_data($id_subkategori)
	{
		// query panggil database
		$this->db->where('id_subkategori', $id_subkategori);
		$data = $this->db->get('subkategori');
		$ambil = $data->row_array();
		return $ambil;
	}
	function ambil_judul($url_subkategori)
	{
		$this->db->where('url_subkategori', $url_subkategori);
		$data = $this->db->get('subkategori');
		$ambil = $data->row_array();
		return $ambil;
	}
	function ambil_kategori($id_kategori){
		$this->db->where('id_kategori', $id_kategori);
		$ambil = $this->db->get("kategori");
		$data = $ambil->result_array();
		return $data;
	}
	function ubah_data($input, $id_subkategori)
	{
		$this->db->where('id_subkategori', $id_subkategori);
		$this->db->update('subkategori', $input);
	}
	function hapus_data($id_subkategori)
	{
		$this->db->where('id_subkategori', $id_subkategori);
		$this->db->delete('subkategori');
	}
}
?>