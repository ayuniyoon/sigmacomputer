<?php
/**
* 
*/
class Mslider extends CI_Model
{
	
	function tampil()
	{
		$this->db->order_by('id_slider', 'desc');
		$ambil = $this->db->get('slider');
		$data = $ambil->result_array();
		return $data;
	}
	function tambah($input)
	{
		$config['upload_path']		= './assets/images/slider/';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';

        // panggil library upload
		$this->load->library('upload', $config);
        // jika benar upload gambar
		if ($this->upload->do_upload('gambar'))
		{
			$input['gambar'] = $this->upload->data('file_name');
		}
        // query input data ke database
		$this->db->insert('slider', $input);
	}
	function hapus($id_slider)
	{
		$this->db->where('id_slider', $id_slider);
		$this->db->delete('slider');	
	}
}
?>