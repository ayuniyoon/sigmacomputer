<?php

class Minformasi extends CI_Model
{
	function tampil(){
		$this->db->order_by('id_informasi', 'desc');
		$ambildata = $this->db->get('informasi');
		$data = $ambildata->result_array();
		return $data;
	}
	function tambah($input)
	{
		$informasi = $input['judul_informasi'];
		$url = strtolower($informasi);

		$url = str_replace(" ", "-", $url);
		$url = str_replace(",", "-", $url);
		$url = str_replace(".", "-", $url);
		$url = str_replace("?", "-", $url);
		$url = str_replace("(", "-", $url);
		$url = str_replace(")", "-", $url);
		$url = str_replace("!", "-", $url);
		$url = str_replace("{", "-", $url);
		$url = str_replace("}", "-", $url);
		$url = str_replace("[", "-", $url);
		$url = str_replace("]", "-", $url);
		$url = str_replace("+", "-", $url);
		$url = str_replace("/", "-", $url);
		$url = str_replace("'", "-", $url);
		$url = str_replace('"', "-", $url);
		$url = str_replace("&", "-", $url);
		$url = str_replace("%", "-", $url);
		$url = str_replace("*", "-", $url);
		$url = str_replace("#", "-", $url);
		$url = str_replace("@", "-", $url);
		$url = str_replace("$", "-", $url);
		$url = str_replace("=", "-", $url);
		$url = str_replace("`", "-", $url);
		$url = str_replace("^", "-", $url);
		$url = str_replace(":", "-", $url);
		$url = str_replace(";", "-", $url);
		$url = str_replace(">", "-", $url);
		$url = str_replace("<", "-", $url);
		$url = str_replace("~", "-", $url);
		$url = str_replace("_", "-", $url);

		$input['url_informasi'] = $url;

		// masukkan ke database
		$this->db->insert('informasi', $input);
	}
	function ambil_data($id_informasi)
	{
		$this->db->where("id_informasi", $id_informasi);
		$ambil = $this->db->get("informasi");
		$data = $ambil->row_array();
		return $data;
	}
	function ubah($input, $id_informasi)
	{
		$this->db->where("id_informasi", $id_informasi);
		$this->db->update("informasi", $input);
		redirect("admin/informasi", "refresh");
	}
	function hapus_data($id_informasi)
	{
		$this->db->where('id_informasi', $id_informasi);
		$this->db->delete('informasi');
	}
	function ambil_info($query)
	{
		$ambil = $this->db->query("SELECT * FROM informasi WHERE url_informasi='$query' AND status_informasi='Publish' ");
		$data = $ambil->row_array();
		return $data;
	}
}
?>