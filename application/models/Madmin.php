<?php

class Madmin extends CI_Model
{
	
	function login($input)
	{
		$username = $input['username'];
		$password = $input['password'];

		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$ambil = $this->db->get('admin');

		$hasil = $ambil->num_rows();

		if($hasil>0) {
			$akun = $ambil->row_array();
			$this->session->set_userdata('ambil', $akun);

			return 'berhasil';
		}
		else
		{
			return 'gagal';
		}
	}
	function ubah($id, $input)
	{
		$this->db->where('admin', $id);
		$this->db->update('admin', $input);
	}
	function ambil_data($id_admin)
	{
		$this->db->where('admin', $id_admin);
		$ambil = $this->db->get('admin');
		$data = $ambil->row_array();
		return $data;
	}
}
?>