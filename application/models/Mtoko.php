<?php

class Mtoko extends CI_Model
{
	
	function tampil()
	{
		$this->db->order_by('id_pengaturan', 'desc');
		$ambil = $this->db->get("pengaturan");
		$data = $ambil->result_array();
		return $data;
	}
	function tambah($input)
	{
		$this->db->insert('pengaturan', $input);
	}
	function ambil_data($id_pengaturan)
	{
		$this->db->where('id_pengaturan', $id_pengaturan);
		$data = $this->db->get('pengaturan');
		$ambil = $data->row_array();
		return $ambil;
	}
	function hapus($id_pengaturan)
	{
		$this->db->where('id_pengaturan', $id_pengaturan);
		$this->db->delete('pengaturan');
	}
	function ubah($id_pengaturan, $input)
	{
		$this->db->where('id_pengaturan', $id_pengaturan);
		$this->db->update('pengaturan', $input);
	}
}
?>