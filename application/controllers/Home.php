<?php

class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library("pagination");
		$this->load->model("Mkategori");
		$this->load->model("Mproduk");
		$this->load->model("Mbank");
		$this->load->model("Mslider");
		$this->load->model("Mbrand");
		$this->load->model("Msubkategori");
		$this->load->model("Martikel");
		$this->load->model("Mtransaksi");
		$this->load->model("Mmember");
		$this->load->model("Mpembayaran");
		$this->load->model("Mtoko");
		$this->load->model("Minformasi");
	}
	function index()
	{		
		$data['slider'] = $this->Mslider->tampil();
		$data['kategori'] = $this->Mkategori->kategori_sidenav();
		$data['subkategori'] = $this->Msubkategori->tampil();
		$data['produk_terbaru'] = $this->Mproduk->terbaru();
		$brand['brand'] = $this->Mbrand->brand();
		$data['gadget'] = $this->Mkategori->ambil_data(9);
		$data['produk_kategori'] = $this->Mproduk->produk_kategori($data['gadget']['id_kategori']);
		$head['member'] = $this->session->userdata('member');
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);

		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$this->load->view("toko/header", $head);
		$this->load->view("toko/body", $data);
		$this->load->view("toko/brand", $brand);
		$this->load->view("toko/footer", $footer);
	}
	function bykategori($url_kategori)
	{
		$head['member'] = $this->session->userdata('member');
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$kategori = $this->Mproduk->bykategori($url_kategori);
		$data['judul'] = $this->Mkategori->ambil_judul($url_kategori);
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);

		// Pagination
		$config['base_url'] = base_url("home/bykategori/".$url_kategori);
		$config['total_rows'] = count($kategori);
		$config['per_page'] = 5;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$from = $this->uri->segment(4);

		$data['kategori'] = $this->Mproduk->kategori_produk_pagination($url_kategori, $config['per_page'], $from);
		$data['mpaging'] = $this->pagination->create_links();

		// footer
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		$this->load->view("toko/header", $head);
		$this->load->view("toko/bykategori", $data);
		$this->load->view("toko/footer", $footer);
	}
	function bysubkategori($url_kategori, $url_subkategori)
	{
		$head['member'] = $this->session->userdata('member');
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$subkategori = $this->Mproduk->bysubkategori($url_subkategori);
		$data['sub'] = $this->Msubkategori->ambil_judul($url_subkategori);
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		// Pagination
		$config['base_url'] = base_url("home/bysubkategori/".$url_kategori."/".$url_subkategori);
		$config['total_rows'] = count($subkategori);
		$config['per_page'] = 5;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$from = $this->uri->segment(5);
		$data['subkategori'] = $this->Mproduk->subkategori_produk_pagination($url_subkategori, $config['per_page'], $from);
		$data['mpaging'] = $this->pagination->create_links();

		// footer
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$this->load->view("toko/header", $head);
		$this->load->view("toko/bysubkategori", $data);
		$this->load->view("toko/footer", $footer);
	}
	function detail($url_produk)
	{
		$head['member'] = $this->session->userdata('member');
		$data['dp'] = $this->Mproduk->produk_url($url_produk);
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);

		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		foreach ($data as $key => $value) {
			$data['produk_terbaru'] = $this->Mproduk->produk_related($value['id_subkategori']);
		}
		if(count($_SESSION['perbandingan'])>2) {
			echo "";
		}
		else
		{
			if(isset($_SESSION['perbandingan'][$data['dp']['id_produk']]))
			{
					// kalo session id produk sudah ada, maka ditambahkan
				$_SESSION['perbandingan'][$data['dp']['id_produk']]=$data['dp']['id_produk'];
			}
			else
			{
					// pertama kali dieksekusi jika session masih kosong, baru dia nyimpen
				$_SESSION['perbandingan'][$data['dp']['id_produk']]=$data['dp']['id_produk'];
			}
		}

		$this->load->view("toko/header", $head);
		$this->load->view("toko/detailproduk", $data);
		$this->load->view("toko/footer", $footer);
	}
	function register()
	{
		$head['member'] = $this->session->userdata('member');
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);

		$this->form_validation->set_message("is_unique", "Upss, %s yang sama sudah digunakan !");
		$this->form_validation->set_message("required", "%s harus diisi !");
		$this->form_validation->set_message("matches", "%s tidak sama !");
		$this->form_validation->set_rules('no_hp', 'Nomor Handphone', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
		$this->form_validation->set_rules('email_member', 'Email', 'is_unique[member.email_member]');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view("toko/header", $head);
			$this->load->view("toko/register");
			$this->load->view("toko/footer", $footer);
		}
		else
		{
			$input = $this->input->post();
			if ($this->input->post()) {
				$this->Mmember->simpan($input);
			echo "<script>alert('Anda berhasil register. Silahkan login.');</script>";
			redirect("login", "refresh");
			}
		}
	}
	// untuk form login member
	function cekemail()
	{
		$email = $this->input->post('email_member');
		$hasil = $this->Mmember->ambil_email($email);
		if(empty($hasil))
		{
			echo "gagal";
		}
		else
		{
			echo "berhasil";
		}
	}
	function login()
	{
		$keranjang = $this->session->userdata('keranjang');
		$head['member'] = $this->session->userdata('member');
		$head['kategori_header'] = $this->Mkategori->tampil();
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		$this->form_validation->set_message("required", "%s harus diisi !");
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view("toko/header", $head);
			$this->load->view("toko/loginmember");
			$this->load->view("toko/footer", $footer);
		}
		else
		{
			$input = $this->input->post();
			if ($input) {
				$cek = $this->Mmember->login($input);

				if ($cek=='berhasil') {
					echo "<script>alert('Terima Kasih, Anda berhasil login');</script>";
					redirect(base_url("member"), "refresh");
				}
				else
				{
					echo "<script>alert('Upss, Email atau Password Anda salah');</script>";
					redirect(base_url("login"), "refresh");
				}
			}
		}
	}
	function member()
	{
		if(!$this->session->userdata('member'))
		{
			$log = base_url("login");
			echo "<script>alert('Anda tidak bisa mengakses halaman ini. Anda harus Login dahulu'); location='$log';</script>";
		}
		else
		{
			$keranjang = $this->session->userdata('keranjang');
			$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
			$head['telepon'] = $this->Mtoko->ambil_data(2);
			$head['wa'] = $this->Mtoko->ambil_data(3);
			$head['kategori_header'] = $this->Mkategori->tampil();
			$head['member'] = $this->session->userdata('member');
			$data['kategori'] = $this->Mkategori->kategori_nav();
			$data['subkategori'] = $this->Msubkategori->tampil();
			$id_member = $head['member']['id_member'];
			$data['riwayat'] = $this->Mtransaksi->ambil_riwayat($id_member);

			$input = $this->input->post();
			if ($input) {
				$this->session->set_userdata('member', $input);
				$this->Mmember->ubah($id_member, $input);
				echo "<script>alert('Profil Anda berhasil diubah');</script>";
				redirect("member", "refresh");
			}
			$footer['tagline'] = $this->Mtoko->ambil_data(5);
			$footer['alamat'] = $this->Mtoko->ambil_data(1);
			$footer['email'] = $this->Mtoko->ambil_data(7);
			$footer['telepon'] = $this->Mtoko->ambil_data(2);
			$footer['wa'] = $this->Mtoko->ambil_data(3);
			$footer['facebook'] = $this->Mtoko->ambil_data(8);
			$footer['instagram'] = $this->Mtoko->ambil_data(9);
			$this->load->view("toko/header", $head);
			$this->load->view("toko/member", $data);
			$this->load->view("toko/footer", $footer);
		}
	}
	function logout()
	{
		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		$this->session->sess_destroy();
		echo "<script>alert('Anda berhasil logout, Good Bye !');location='".base_url()."'</script>";
	}

	function blog()
	{
		$head['member'] = $this->session->userdata('member');
		$data['kategori'] = $this->Mkategori->kategori_nav();
		$data['subkategori'] = $this->Msubkategori->tampil();
		$blog = $this->Martikel->tampil();

		// Pagination
		$config['base_url'] = base_url("home/blog");
		$config['total_rows'] = count($blog);
		$config['per_page'] = 2;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$from = $this->uri->segment(3);

		$data['blog'] = $this->Martikel->tampil_blog_pagination($config['per_page'], $from);
		$data['mpaging'] = $this->pagination->create_links();

		// footer
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);

		$this->load->view("toko/header", $head);
		$this->load->view("toko/blog", $data);
		$this->load->view("toko/footer", $footer);
	}
	function detailblog($url_artikel)
	{
		$head['member'] = $this->session->userdata('member');
		$data['kategori'] = $this->Mkategori->kategori_nav();
		$data['subkategori'] = $this->Msubkategori->tampil();
		$data['dtblog'] = $this->Martikel->ambil_url($url_artikel);
		$data['artikel_terkait'] = $this->Martikel->artikel_terkait();

		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		$this->load->view("toko/header", $head);
		$this->load->view("toko/detailblog", $data);
		$this->load->view("toko/footer", $footer);
	}
	function shoppingcart()
	{
		if ($this->input->post('jumlah_beli')) {
			$input = $this->input->post('jumlah_beli');
			foreach ($input as $id_produk => $jumlah_beli) {
				$_SESSION['keranjang'][$id_produk] = $jumlah_beli;
			}
			redirect("shoppingcart");
			exit();
		}

		$head['member'] = $this->session->userdata('member');
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$data['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);

		$head['kategori_header'] = $this->Mkategori->tampil();
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		$this->load->view("toko/header", $head);
		$this->load->view("toko/cart", $data);
		$this->load->view("toko/footer", $footer);
	}
	function cart($id_produk)
	{
		//jika ada session keranjang dengan id_produk yang sama maka akan menambah jumlah_beli/ belanja
		if(isset($_SESSION['keranjang']['$id_produk']))
		{
			$_SESSION['keranjang'][$id_produk]+=1;
		}
		// selain itu (tidak ada session keranjang dengan id_produk yang sama) maka : menambahkan session keranjang dengan id_produk beserta jumlah belinya 1
		else
		{
			$_SESSION['keranjang'][$id_produk]=1;
		}

		redirect("shoppingcart");
		exit();
	}
	function deletecart($id_produk)
	{
		unset($_SESSION['keranjang'][$id_produk]);
		redirect("shoppingcart");
		exit();
	}
	function checkout()
	{
		$head['member'] = $this->session->userdata('member');
		$keranjang = $this->session->userdata('keranjang');
		$data['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);

		$input = $this->input->post();
		if ($input) {
			if (!$this->session->userdata('member'))
			{
				echo "<script>alert('Anda harus login terlebih dahulu');location='".base_url("login")."'</script>";
			}
			else
			{
				if (isset($keranjang)) {
					$id['id_transaksi'] = $this->Mtransaksi->simpan_transaksi($input,$keranjang);
					$this->session->unset_userdata('keranjang');
					$this->session->set_userdata('transaksi', $id['id_transaksi']);
					echo "<script>alert('Terima kasih atas pembelian anda');location='".base_url("invoice")."'</script>";
				}
				else
				{
					echo "<script>alert('Keranjang anda KOSONG, Anda tidak bisa melanjutkan');location='".base_url("shoppingcart")."'</script>";
				}
			}
		}
		$head['kategori_header'] = $this->Mkategori->tampil();
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		$this->load->view("toko/header", $head);
		$this->load->view("toko/checkout", $data);
		$this->load->view("toko/footer", $footer);
	}
	function invoice()
	{
		if(!$this->session->userdata('member'))
		{
			echo "<script>alert('Untuk mengakses halaman ini, Anda diharuskan login.');</script>";
			redirect(base_url(), "refresh");
		}
		else
		{
			$id_transaksi = $this->session->userdata('transaksi');
			$head['member'] = $this->session->userdata('member');
			$data['invoice'] = $this->Mtransaksi->ambil_invoice($id_transaksi);
			$data['detail'] = $this->Mtransaksi->detail($id_transaksi);
			$data['bank'] = $this->Mbank->tampil();
			$data['nama_toko'] = $this->Mtoko->ambil_data(6);
			$data['alamat'] = $this->Mtoko->ambil_data(1);
			$data['telepon'] = $this->Mtoko->ambil_data(2);
			$data['email'] = $this->Mtoko->ambil_data(7);
			$data['wa'] = $this->Mtoko->ambil_data(3);

			$head['kategori_header'] = $this->Mkategori->tampil();
			$head['telepon'] = $this->Mtoko->ambil_data(2);
			$head['wa'] = $this->Mtoko->ambil_data(3);
			$footer['tagline'] = $this->Mtoko->ambil_data(5);
			$footer['alamat'] = $this->Mtoko->ambil_data(1);
			$footer['email'] = $this->Mtoko->ambil_data(7);
			$footer['telepon'] = $this->Mtoko->ambil_data(2);
			$footer['wa'] = $this->Mtoko->ambil_data(3);
			$footer['facebook'] = $this->Mtoko->ambil_data(8);
			$footer['instagram'] = $this->Mtoko->ambil_data(9);

			$this->load->view("toko/header", $head);
			$this->load->view("toko/payment", $data);
			$this->load->view("toko/footer", $footer);
		}
	}
	// function riwayat_invoice($id_transaksi)
	// {
	// 	$this->Mtransaksi->
	// }
	function perbandingan($id=null)
	{
		if ($id!==null) {
			unset($_SESSION['perbandingan'][$id]);
			redirect("home/perbandingan");
			exit();
		}

		$head['member'] = $this->session->userdata('member');
		$perbandingan = $this->session->userdata('perbandingan');
		$data['perbandingan'] = $this->Mproduk->ambil_produk_perbandingan($perbandingan);

		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		$this->load->view("toko/header", $head);
		$this->load->view("toko/perbandingan", $data);
		$this->load->view("toko/footer", $footer);
	}
	function konfirmasi()
	{
		$head['member'] = $this->session->userdata('member');
		$data['bank'] = $this->Mbank->tampil();
		$data['cek'] = "";
		if ($this->input->post()) {
			$input = $this->input->post();
			$this->Mpembayaran->konfirmasi($input);
			$data['cek'] = "berhasil";
		}
		$head['kategori_header'] = $this->Mkategori->tampil();
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);

		$this->load->view("toko/header", $head);
		$this->load->view("toko/pembayaran", $data);
		$this->load->view("toko/footer", $footer);
	}
	// untuk form konfirmasi
	function cekinvoice()
	{
		$id_transaksi = $this->input->post('id_transaksi');
		$hasil = $this->Mtransaksi->detail($id_transaksi);
		if(empty($hasil))
		{
			echo "gagal";
		}
		else
		{
			echo "berhasil";
		}
	}
	function cari_produk()
	{
		$data['produk']=array();
		if($this->input->post('nama_produk'))
		{
			$input['nama_produk'] = $this->input->post('nama_produk');
			$data['produk'] = $this->Mproduk->cari_produk($input['nama_produk']);
		}
		elseif ($this->input->post('kategori'))
		{
			$input = $this->input->post();
			$id_kategori = $input['kategori'];
			$data['produk'] = $this->Mproduk->cari_produk_by_kategori($id_kategori);
		}
		
		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$head['kategori_header'] = $this->Mkategori->tampil();
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$this->load->view("toko/header", $head);
		$this->load->view("toko/cariproduk", $data);
		$this->load->view("toko/footer", $footer);
	}
	function cara_belanja()
	{
		$head['member'] = $this->session->userdata('member');
		$data['kategori'] = $this->Mkategori->kategori_nav();
		$head['kategori_header'] = $this->Mkategori->tampil();
		$data['subkategori'] = $this->Msubkategori->tampil();
		$query = 'cara-belanja';
		$data['carabelanja'] = $this->Minformasi->ambil_info($query);

		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$this->load->view("toko/header", $head);
		$this->load->view("toko/cara-belanja", $data);
		$this->load->view("toko/footer", $footer);
	}
	function metode_pembayaran()
	{
		$head['member'] = $this->session->userdata('member');
		$data['kategori'] = $this->Mkategori->kategori_nav();
		$head['kategori_header'] = $this->Mkategori->tampil();
		$data['subkategori'] = $this->Msubkategori->tampil();
		$query = 'metode-pembayaran';
		$data['mpembayaran'] = $this->Minformasi->ambil_info($query);
		$data['bank'] = $this->Mbank->tampil();

		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$this->load->view("toko/header", $head);
		$this->load->view("toko/metode-pembayaran", $data);
		$this->load->view("toko/footer", $footer);
	}
	function profil_perusahaan()
	{
		$head['member'] = $this->session->userdata('member');
		$data['kategori'] = $this->Mkategori->kategori_nav();
		$head['kategori_header'] = $this->Mkategori->tampil();
		$data['subkategori'] = $this->Msubkategori->tampil();
		$query = 'profil-perusahaan';
		$data['profil'] = $this->Minformasi->ambil_info($query);

		$keranjang = $this->session->userdata('keranjang');
		$head['keranjang'] = $this->Mproduk->tampil_keranjang($keranjang);
		$head['telepon'] = $this->Mtoko->ambil_data(2);
		$head['wa'] = $this->Mtoko->ambil_data(3);
		$footer['tagline'] = $this->Mtoko->ambil_data(5);
		$footer['alamat'] = $this->Mtoko->ambil_data(1);
		$footer['email'] = $this->Mtoko->ambil_data(7);
		$footer['telepon'] = $this->Mtoko->ambil_data(2);
		$footer['wa'] = $this->Mtoko->ambil_data(3);
		$footer['facebook'] = $this->Mtoko->ambil_data(8);
		$footer['instagram'] = $this->Mtoko->ambil_data(9);
		$this->load->view("toko/header", $head);
		$this->load->view("toko/profil-perusahaan", $data);
		$this->load->view("toko/footer", $footer);
	}




}
?>