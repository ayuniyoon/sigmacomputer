<?php

class Api extends CI_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mapi");
	}

	function provinsi()
	{
		$provinsi = $this->Mapi->provinsi();
		if(empty($provinsi))
		{
			echo '<option value="">Provinsi tidak di temukan</option>';
		}
		else
		{
			echo '<option value="">- Pilih Provinsi -</option>';
			foreach ($provinsi as $key => $value)
			{
				echo '<option value="'.$value['province_id'].'">'.$value['province'].'</option>';
			}	
		}
	}

	function kota()
	{
		$provinsi = $this->input->post('idprovinsi');
		$kota = $this->Mapi->kota($provinsi);
		if(empty($kota))
		{
			echo '<option value="">kota tidak di temukan</option>';
		}
		else
		{
			echo '<option value="">- Pilih kota -</option>';
			foreach ($kota as $key => $value)
			{
				echo '<option value="'.$value['city_id'].'" kota="'.$value['city_name'].'" tipe-kota="'.$value['type'].'">'.$value['city_name'].'</option>';
			}	
		}
	}

	function paketkurir()
	{
		$kurir = $this->input->post('kurir');
		$berat = $this->input->post('berat');
		$kota_tujuan = $this->input->post('kota_tujuan');

		$datapaket = $this->Mapi->paketkurir($kurir,$berat,$kota_tujuan);
		if (empty($datapaket))
		{
			echo '<option value="">Paket tidak di temukan</option>';
		}
		else
		{
			echo '<option value="">- Pilih Paket Ekspedisi -</option>';
			foreach ($datapaket as $key => $value)
			{
				echo '<option value="'.$value['service'].'" hari="'.$value['cost'][0]['etd'].'" harga="'.$value['cost'][0]['value'].'">'.$value['service'].' (Rp. '.number_format($value['cost'][0]['value']).')</option>';
			}
		}
	}

}
?>