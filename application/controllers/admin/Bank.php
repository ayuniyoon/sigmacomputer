<?php

class Bank extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mbank");
		if (!$this->session->userdata('ambil')) {
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'bank';
		$data['judul'] = 'Rekening Bank Toko';
		$data['bank'] = $this->Mbank->tampil();

		$input = $this->input->post();
		if ($input)
		{
			$this->Mbank->tambah($input);
			redirect('admin/bank', 'refresh');
		}

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav',$admin);
		$this->load->view('admin/bank/tampil', $data);
		$this->load->view('admin/footer');
	}
	function ubah($id_bank)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'bank';
		$input = $this->input->post();
		if ($input) {
			$this->Mbank->ubah_data($input, $id_bank);
			redirect("admin/bank", "refresh");
		}
		$data['ubah'] = $this->Mbank->ambil_data($id_bank);

		// formulir ubah data
		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/bank/ubah", $data);
		$this->load->view("admin/footer");
	}
	function hapus($id_bank)
	{
		$this->Mbank->hapus_data($id_bank);
		redirect("admin/bank", "refresh");
	}



}
?>