<?php

class Admin extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Madmin");
	}
	function login()
	{
		$data['hasil'] ="";
		$data['title'] = 'Sigma Computer';
		if ($this->input->post()) {
			$cek = $this->Madmin->login($this->input->post());

			if ($cek=='berhasil') {
				$data['hasil'] = "berhasil";
			}
			else
			{
				$data['hasil'] = "gagal";
			}
		}

		$this->load->view('admin/login', $data);
	}
	function logout()
	{
	    $user_data = $this->session->all_userdata();
	        foreach ($user_data as $key => $value) {
	            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
	                $this->session->unset_userdata($key);
	            }
	        }
	    $this->session->sess_destroy();
	    echo "<script>alert('Anda berhasil logout, Good Bye !');location='".base_url("admin")."'</script>";
	}
}
?>