<?php
/**
* 
*/
class Transaksi extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
		$this->load->model("Mtransaksi");
		$this->load->model("Mbank");
		$this->load->model("Mtoko");
		$this->load->model("Mpembayaran");
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'transaksi';
		$data['judul'] = 'Daftar Transaksi Penjualan';
		$data['penjualan'] = $this->Mtransaksi->tampil();

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/penjualan/tampil', $data);
		$this->load->view('admin/footer');
	}
	function detail($id_transaksi)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'transaksi';
		$data['judul'] = 'Detail Transaksi';
		$data['ambil'] = $this->Mtransaksi->ambil($id_transaksi);
		$data['detail'] = $this->Mtransaksi->detail($id_transaksi);
		$data['bank'] = $this->Mbank->tampil();
		
		$data['nama_toko'] = $this->Mtoko->ambil_data(6);
		$data['alamat'] = $this->Mtoko->ambil_data(1);
		$data['telepon'] = $this->Mtoko->ambil_data(2);
		$data['email'] = $this->Mtoko->ambil_data(7);
		$data['wa'] = $this->Mtoko->ambil_data(3);

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/penjualan/detail', $data);
		$this->load->view('admin/footer');
	}
	function kirim()
	{
		$id_transaksi = $this->input->post('id_transaksi');
		$data['no_resi'] = $this->input->post('no_resi');
		$data['status_pengiriman'] = 1;
		$data['tgl_pengiriman'] = date("Y-m-d");
		$cek = $this->Mtransaksi->kirim_barang($data, $id_transaksi);
		echo $cek;
		
	}
	function pembayaran($id_transaksi)
	{
		if($this->input->post())
		{
			$status['status_pembelian'] = '1';
			$this->Mtransaksi->ubah_status($id_transaksi,$status);
			echo "<script>alert('Status telah diubah');</script>";
			redirect("admin/transaksi", "refresh");

		}
		$data['pembayaran'] = $this->Mpembayaran->ambil_data($id_transaksi);

		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'transaksi';
		$data['judul'] = 'Detail Pembayaran';

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/pembayaran/detail', $data);
		$this->load->view('admin/footer');
	}
}
?>