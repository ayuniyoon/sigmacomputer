<?php
/**
* 
*/
class Pembayaran extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
		$this->load->model("Mpembayaran");
		$this->load->model("Mtransaksi");
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'pembayaran';
		$data['judul'] = 'Konfirmasi Pembayaran';
		$data['bayar'] = $this->Mpembayaran->tampil();
		
		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/pembayaran/tampil', $data);
		$this->load->view('admin/footer');
	}
	// function detail($id_pembayaran)
	// {
	// 	$admin['admin'] = $this->session->userdata('ambil');
	// 	$data['halaman'] = 'pembayaran';
	// 	$data['judul'] = 'Detail Konfirmasi Pembayaran';
	// 	$data['detail'] = $this->Mpembayaran->ambil_data($id_pembayaran);

	// 	$this->load->view('admin/header');
	// 	$this->load->view('admin/sidenav', $data);
	// 	$this->load->view('admin/topnav', $admin);
	// 	$this->load->view('admin/pembayaran/detail', $data);
	// 	$this->load->view('admin/footer');
	// }

	
	
}
?>