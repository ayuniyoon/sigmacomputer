<?php

class Sms extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Msms");
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['judul'] = 'Pengaturan Template SMS Notifikasi';
		$data['halaman'] = 'sms';
		$temp['temp'] = $this->Msms->tampil();

		$input = $this->input->post();
		if ($input) {
			$id = $input['id'];
			$this->Msms->ubah($id,$input);
			echo "<script>alert('Berhasil diupdate');</script>";
			redirect("admin/sms", "refresh");
		}

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/sms/tampil", $temp);
		$this->load->view("admin/footer");
	}
}
?>