<?php
/**
* 
*/
class Member extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mmember");
		$this->load->model("Mtransaksi");
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'member';
		$data['judul'] = 'Member Yang Terdaftar';
		$data['member'] = $this->Mmember->tampil();

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/member/tampil', $data);
		$this->load->view('admin/footer');
	}
	function detail($id_member)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'member';
		$data['judul'] = 'Detail Member Terdaftar';
		$data['detail'] = $this->Mmember->ambil_data($id_member);
		$data['riwayat'] = $this->Mtransaksi->ambil_riwayat($id_member);

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/member/detail', $data);
		$this->load->view('admin/footer');
	}
	function hapus($id_member)
	{
		$this->Mmember->hapus_data($id_member);
		redirect("admin/member", "refresh");
	}
}
?>