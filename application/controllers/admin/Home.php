<?php

class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mtransaksi');
		$this->load->model('Mpembayaran');
		$this->load->model('Madmin');
		$this->load->model('Mmember');
		if (!$this->session->userdata('ambil')) {
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}
	function index()
	{
		$dt['halaman'] = 'home';
		$admin['admin'] = $this->session->userdata('ambil');

		$data['penjualan'] = $this->Mtransaksi->total_penjualan();
		$data['sales'] = $this->Mtransaksi->total_sales();
		// hitungan masuk per bulan
		$data['penjualan_array'] = $this->Mtransaksi->jumlah_penjualan();
		$data['jml_penjualan'] = count($data['penjualan_array']);
		$data['konfirmasi'] = $this->Mpembayaran->jumlah_konfirmasi();
		$data['konfirmasi_bln'] = count($data['konfirmasi']);
		$data['member'] = $this->Mmember->jumlah_mendaftar();
		$data['member_bln'] = count($data['member']);

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $dt);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/index', $data);
		$this->load->view('admin/footer');
	}

	function profile()
	{
		$login = $this->session->userdata('ambil');
		$admin['admin'] = $this->Madmin->ambil_data($login['admin']);
		$dt['halaman'] = 'home';
		$data['judul'] = 'Profile Administrator';

		$input = $this->input->post();
		if ($input) {
			$id = $input['admin'];
			$this->Madmin->ubah($id, $input);
			echo "<script>alert('Berhasil diupdate');</script>";
			redirect("admin/home/profile", "refresh");
		}
		
		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $dt);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/admin/profile", $data, $admin);
		$this->load->view("admin/footer");
	}
}
?>