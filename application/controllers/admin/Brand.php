<?php

class Brand extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mbrand');
		$this->load->library('form_validation');
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";		
		}		
	}
	function index()
	{ 
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'brand';
		$data['judul'] = 'Semua Data Brand';
		$data['brand'] = $this->Mbrand->tampil();

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav',$data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/brand/tampil', $data);
		$this->load->view('admin/footer');
	}
	function tambah()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'brand';

		$this->form_validation->set_message("is_unique", "Upss, %s yang sama sudah ada !");
		$this->form_validation->set_rules('nama_brand', 'Brand', 'is_unique[brand.nama_brand]');

		if ($this->form_validation->run() == TRUE)
		{
			$input = $this->input->post();
			if ($input) {
				$this->Mbrand->tambah($input);
				redirect('admin/brand', 'refresh');
			}
		}

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/brand/tambah');
		$this->load->view('admin/footer');
	}
	function hapus($id_brand)
	{
		$this->Mbrand->hapus_data($id_brand);
		redirect("admin/brand", "refresh");
	}
	function ubah($id_brand)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'brand';
		$data['ubah'] = $this->Mbrand->ambil_data($id_brand);
		$data['judul'] = 'Edit Brand Lama';

		$input = $this->input->post();
		
		if ($input) {
			$this->Mbrand->ubah_data($input, $id_brand);
			redirect('admin/brand', 'refresh');
		}
		//lempar ke view formulir ubah data
		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/brand/ubah", $data);
		$this->load->view("admin/footer");
	}
}
?>