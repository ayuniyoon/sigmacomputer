<?php

class Subkategori extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Msubkategori');
		$this->load->model('Mkategori');
		$this->load->library('form_validation');
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'subkategori';
		$data['judul'] = 'Semua Data Sub Kategori';
		$data['subkategori'] = $this->Msubkategori->tampil();
		$data['kategori'] = $this->Mkategori->tampil();

		$this->form_validation->set_message("is_unique", "Upss, %s yang sama sudah ada !");
		$this->form_validation->set_rules('nama_subkategori', 'Nama Sub Kategori', 'is_unique[subkategori.nama_subkategori]');
		$this->form_validation->set_message("required", "%s harus diisi !");
		$this->form_validation->set_rules('id_kategori', 'Kategori', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$input = $this->input->post();
			if ($input) {
				$this->Msubkategori->tambah($input);
				redirect("admin/subkategori");
			}
		}

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/subkategori/tampil", $data);
		$this->load->view("admin/footer");
	}

	function ubah($id_subkategori)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'subkategori';
		$data['kategori'] = $this->Mkategori->tampil();
		
		$input = $this->input->post();
		if ($input) {
			$this->Msubkategori->ubah_data($input, $id_subkategori);
			redirect("admin/subkategori", "refresh");
		}
		$data['ubah'] = $this->Msubkategori->ambil_data($id_subkategori);

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/subkategori/ubah", $data);
		$this->load->view("admin/footer");
	}
	function hapus($id_subkategori)
	{
		$this->Msubkategori->hapus_data($id_subkategori);
		redirect("admin/subkategori", "refresh");
	}
}
?>