<?php

class Artikel extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('Martikel');
		$this->load->library('form_validation');
		$this->load->library('session');
		if(!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}

	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'artikel';
		$data['judul'] = 'Semua Artikel';
		$data['artikel'] = $this->Martikel->tampil();

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/artikel/tampil', $data);
		$this->load->view('admin/footer');
	}
	function tambah()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'artikel';
		$this->form_validation->set_rules("judul_artikel", "Judul Artikel", "required");
		$this->form_validation->set_rules("isi_artikel", "Isi Artikel", "required");
		$this->form_validation->set_rules("status_artikel", "Status Publikasi", "required");
		$this->form_validation->set_message("required", "%s harus diisi");

		$input = $this->input->post();
		if ($input)
		{
			$input['tgl_posting'] = date("Y-m-d");
			$this->Martikel->tambah($input);
			redirect('admin/artikel', 'refresh');
		}

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/artikel/tambah');
		$this->load->view('admin/footer');
	}
	function detail($id_artikel)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'artikel';
		$data['detail'] = $this->Martikel->ambil_data($id_artikel);

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/artikel/detail', $data);
		$this->load->view('admin/footer');
	}
	function hapus($id_artikel)
	{
		$this->Martikel->hapus_data($id_artikel);
		redirect("admin/artikel", "refresh");
	}
	function ubah($id_artikel)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'artikel';
		$input = $this->input->post();

		if ($input) {
			$this->Martikel->ubah_data($input, $id_artikel);
			redirect("admin/artikel", "refresh");
		}

		$data['ubah'] = $this->Martikel->ambil_data($id_artikel);
		$data['judul'] = 'Edit Artikel Lama';

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/artikel/ubah", $data);
		$this->load->view("admin/footer");
	}
}

?>