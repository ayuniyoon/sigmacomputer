<?php

class Slider extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
		$this->load->model("Mslider");
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'slider';
		$data['judul'] = 'Pengaturan Slider';
		$data['slider'] = $this->Mslider->tampil();

		if ($this->input->post())
		{
			$input = $this->input->post();
			$this->Mslider->tambah($input);
			redirect("admin/slider", "refresh");
		}	

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/slider/tampil", $data);
		$this->load->view("admin/footer");
	}
	function hapus($id_slider)
	{
		$this->Mslider->hapus($id_slider);
		redirect("admin/slider", "refresh");
	}
}
?>