<?php

class Kategori extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mkategori");
		$this->load->library('form_validation');
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}

	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'kategori';
		$data['kategori'] = $this->Mkategori->tampil();
		$data['judul'] = 'Semua Data Kategori';
		$data['form'] = 'Tambah Kategori Baru';

		$this->form_validation->set_message("is_unique", "Upss, %s yang sama sudah ada !");
		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'is_unique[kategori.nama_kategori]');

		if ($this->form_validation->run() == TRUE)
		{
			$input = $this->input->post();
			if ($input) {
				$this->Mkategori->tambah($input);
				redirect("admin/kategori");
			}
		}

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/kategori/tampil', $data);
		$this->load->view('admin/footer');
	}
	function hapus($id_kategori)
	{
		$this->Mkategori->hapus_data($id_kategori);
		redirect("admin/kategori", "refresh");
	}
	function ubah($id_kategori)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'kategori';
		$input = $this->input->post();
		if ($input) {
			$this->Mkategori->ubah_data($input, $id_kategori);
			redirect("admin/kategori", "refresh");
		}
		$data['ubah'] = $this->Mkategori->ambil_data($id_kategori);

		// formulir ubah data
		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/kategori/ubah", $data);
		$this->load->view("admin/footer");
	}
}
?>