<?php
/**
* 
*/
class Produk extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mproduk");
		$this->load->model("Mbrand");
		$this->load->model("Mkategori");
		$this->load->model("Msubkategori");

		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'produk';
		$data['judul'] = 'Semua Produk';
		$data['produk'] = $this->Mproduk->tampil();
		
		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/produk/tampil', $data);
		$this->load->view('admin/footer');
	}
	function tambah()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'produk';
		$data['cek']="";
		$data['kategori'] = $this->Mkategori->tampil();
		$data['brand'] = $this->Mbrand->tampil();

		if ($this->input->post('id_kategori'))
		{
			$data['id_kategori'] = $this->input->post('id_kategori');
			$data['subkategori'] = $this->Mproduk->ambil_subkategori($data['id_kategori']);
		}
		else
		{
			$data['id_kategori'] = "";
			$data['subkategori'] = "";
		}

		if ($this->input->post('simpan')) {
			$input = $this->input->post();
			$this->Mproduk->tambah($input);
			$data['cek'] = "berhasil";
		}

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/produk/tambah', $data);
		$this->load->view('admin/footer');
	}
	function detail($id_produk)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'produk';
		$data['detail'] = $this->Mproduk->ambil_data($id_produk);
		$data['ecom'] = 'Detail Barang';

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/produk/detail', $data);
		$this->load->view('admin/footer');
	}
	function ubah($id_produk)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'produk';
		$data['cek']="";
		$data['produk'] = $this->Mproduk->ambil_data($id_produk);
		$data['kategori'] = $this->Mkategori->tampil();
		$data['brand'] = $this->Mbrand->tampil();
		$data['subkategori'] = $this->Msubkategori->tampil();

		if ($this->input->post('id_kategori'))
		{
			$data['id_kategori'] = $this->input->post('id_kategori');
			$data['subkategori'] = $this->Mproduk->ambil_subkategori($data['id_kategori']);
		}
		else
		{
			$data['subkategori'] = $this->Mproduk->ambil_subkategori($data['produk']['id_kategori']);
			$data['id_kategori'] = $data['produk']['id_kategori'];
		}

		if ($this->input->post('simpan')) {
			$input =$this->input->post();
			$this->Mproduk->ubah($input, $id_produk);
			$data['cek'] = "berhasil";
		}

		$this->load->view('admin/header');
		$this->load->view('admin/sidenav', $data);
		$this->load->view('admin/topnav', $admin);
		$this->load->view('admin/produk/ubah', $data);
		$this->load->view('admin/footer');
	}
	function hapus($id_produk)
	{
		$this->Mproduk->hapus($id_produk);
		redirect("admin/produk", "refresh");
	}




}
?>