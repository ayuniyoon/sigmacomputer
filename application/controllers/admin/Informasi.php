<?php

class Informasi extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Minformasi");
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
	}
	function index(){
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'informasi';
		$data['judul'] = 'Tutorial Untuk Pengunjung';
		$data['info'] = $this->Minformasi->tampil();

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/informasi/tampil", $data);
		$this->load->view("admin/footer");
	}
	function tambah()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'informasi';
		$input = $this->input->post();
		if ($input) {
			$input['tgl_posting'] = date("Y-m-d");
			$this->Minformasi->tambah($input);
			redirect('admin/informasi', 'refresh');
		}

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/informasi/tambah");
		$this->load->view("admin/footer");
	}
	function ubah($id_informasi)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'informasi';
		$data['judul'] = 'Edit Informasi Pengunjung';
		$data['ubah'] = $this->Minformasi->ambil_data($id_informasi);

		$input = $this->input->post();
		if ($input) {
			$this->Minformasi->ubah($input, $id_informasi);
		}

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/informasi/ubah", $data);
		$this->load->view("admin/footer");
	}
	function detail($id_informasi)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'informasi';
		$data['judul'] = 'Detail Informasi Toko';
		$data['detail'] = $this->Minformasi->ambil_data($id_informasi);

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/informasi/detail", $data);
		$this->load->view("admin/footer");
	}
	function hapus($id_informasi)
	{
		$this->Minformasi->hapus_data($id_informasi);
		redirect("admin/informasi", "refresh");
	}
}
?>