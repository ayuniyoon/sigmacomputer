<?php

class Toko extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('ambil'))
		{
			$log = base_url("admin");
			echo "<script>alert('Anda Harus Login Dahulu'); location='$log';</script>";
		}
		$this->load->model("Mtoko");
	}
	function index()
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'pengaturan';
		$data['judul'] = 'Pengaturan Toko';
		$data['pengaturan'] = $this->Mtoko->tampil();

		if ($this->input->post()) {
			$input = $this->input->post();
			$this->Mtoko->tambah($input);
			redirect("admin/toko", "refresh");
		}

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/toko/tampil", $data);
		$this->load->view("admin/footer");
	}
	function hapus($id_pengaturan)
	{
		$this->Mtoko->hapus($id_pengaturan);
		redirect("admin/toko", "refresh");
	}
	function ubah($id_pengaturan)
	{
		$admin['admin'] = $this->session->userdata('ambil');
		$data['halaman'] = 'pengaturan';
		$data['judul'] = 'Edit Pengaturan';

		$input = $this->input->post();
		if($input)
		{
			$this->Mtoko->ubah($id_pengaturan, $input);
			redirect("admin/toko", "refresh");
		}

		$data['ubah'] = $this->Mtoko->ambil_data($id_pengaturan);

		$this->load->view("admin/header");
		$this->load->view("admin/sidenav", $data);
		$this->load->view("admin/topnav", $admin);
		$this->load->view("admin/toko/ubah", $data);
		$this->load->view("admin/footer");
	}
	
}
?>