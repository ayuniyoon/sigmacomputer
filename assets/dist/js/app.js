$(document).ready(function()
{
	$("#homeslider").owlCarousel({
		items:1,
		loop:true,
		autoHeightClass:'owl-height',
		autoplay:true,
		autoplayTimeout:4000,
		autoplayHoverPause:false
	});
	// produk terbaru
	$("#terbaru").owlCarousel({
		margin:10,
		loop:true,
		dots:false,
		navContainer:'#produkterbaru',
		navText:["<a class='btn btn-default btn-xs'><i class='fa fa-chevron-left'></i></a>","<a class='btn btn-default btn-xs'><i class='fa fa-chevron-right'></i></a>"],
		responsiveClass:true,
		responsive:{
			300:{
				items:1,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:true
			}
		}
	});
	// laptop
	$("#produkbykategori").owlCarousel({
		margin:10,
		loop:true,
		dots:true,
		navContainer:'#bykategori',
		navText:["<a class='btn btn-default btn-xs'><i class='fa fa-chevron-left'></i></a>","<a class='btn btn-default btn-xs'><i class='fa fa-chevron-right'></i></a>"],
		responsiveClass:true,
		responsive:{
			300:{
				items:1,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:true
			}
		}
	});
	// slider detail
	$("#detailterbaru").owlCarousel({
		margin:10,
		loop:true,
		dots:false,
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		navContainer:'#bykategori',
		navText:["<a class='btn btn-default btn-xs'><i class='fa fa-chevron-left'></i></a>","<a class='btn btn-default btn-xs'><i class='fa fa-chevron-right'></i></a>"],
		responsiveClass:true,
		responsive:{
			300:{
				items:1,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:true
			}
		}
	});
	$("#brand").owlCarousel({
		items:9,
		loop:true,
		autoplay:true,
		autoplayTimeout:2000,
		responsiveClass:true,
		responsive:{
			300:{
				items:4,
			},
			600:{
				items:6,
			},
			1000:{
				items:9,
			}
		}
	});
	// $(".semualaptop").owlCarousel({
	// 	margin:10,
	// 	loop:true,
	// 	dots:true,
	// 	navContainer:'#navsemualaptop',
	// 	navText:["<a class='btn btn-default btn-xs'><i class='fa fa-chevron-left'></i></a>","<a class='btn btn-default btn-xs'><i class='fa fa-chevron-right'></i></a>"],
	// 	responsiveClass:true,
	// 	responsive:{
	// 		300:{
	// 			items:1,
	// 			nav:true
	// 		},
	// 		600:{
	// 			items:3,
	// 			nav:false
	// 		},
	// 		1000:{
	// 			items:5,
	// 			nav:true
	// 		}
	// 	}
	// });
});